USE [SIG]
GO
/****** Object:  Table [dbo].[AcuerdoConcepto]    Script Date: 09/04/2015 00:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcuerdoConcepto](
	[idConcepto] [int] IDENTITY(1,1) NOT NULL,
	[Concepto] [varchar](150) NULL,
	[ConEsp] [varchar](1) NULL,
	[Orden] [int] NULL,
 CONSTRAINT [PK_AcuerdoConcepto] PRIMARY KEY CLUSTERED 
(
	[idConcepto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AcuerdoConcepto] ON
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (1, N'Hs Trabajadas', N'P', 1)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (2, N'Hs Espera', N'P', 2)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (3, N'Valor', N'P', 3)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (4, N'Diario', N'E', 1)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (5, N'Quincenal', N'E', 2)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (6, N'CantMensual', N'E', 3)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (7, N'Mensual', N'E', 4)
INSERT [dbo].[AcuerdoConcepto] ([idConcepto], [Concepto], [ConEsp], [Orden]) VALUES (8, N'%', N'M', 1)
SET IDENTITY_INSERT [dbo].[AcuerdoConcepto] OFF
