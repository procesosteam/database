USE [SIG]
GO

/****** Object:  Table [dbo].[Acuerdo]    Script Date: 09/08/2015 09:48:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Acuerdo](
	[idAcuerdo] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [varchar](250) NULL,
	[nroAcuerdo] [varchar](50) NOT NULL,
	[idCliente] [int] NOT NULL,
	[fechaInicio] [datetime] NULL,
	[fechaFin] [datetime] NULL,
	[baja] [bit] NOT NULL,
	[fechaEmision] [datetime] NULL,
	[nroRevision] [int] NULL,
	[tipoMoneda] [varchar](50) NULL,
 CONSTRAINT [PK_Acuerdo] PRIMARY KEY CLUSTERED 
(
	[idAcuerdo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Acuerdo] ADD  CONSTRAINT [DF_Acuerdo_baja]  DEFAULT ((0)) FOR [baja]
GO


