/*
   viernes, 04 de septiembre de 201510:59:09 a.m.
   Usuario: 
   Servidor: localhost
   Base de datos: SIG
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AcuerdoServicio ADD
	Requiere varchar(1) NULL
GO
ALTER TABLE dbo.AcuerdoServicio SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
