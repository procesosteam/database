/*
   martes, 01 de septiembre de 201512:01:03 p.m.
   Usuario: 
   Servidor: localhost
   Base de datos: SIG
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Equipos ADD
	idEmpresa int NULL
GO
ALTER TABLE dbo.Equipos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
