USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionAdd]    Script Date: 09/13/2015 16:26:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionAdd]
(
	@Descripcion varchar(100) = NULL,
	@idPersonalACargo int = NULL,
	@idPersonal int = NULL,
	@Fecha smalldatetime = NULL,
	@FechaOriginal smalldatetime = NULL
)
AS
BEGIN


	INSERT
	INTO [PermisoConduccion]
	(
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	)
	VALUES
	(
		@Descripcion,
		@idPersonalACargo,
		@idPersonal,
		@Fecha,
		@FechaOriginal
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
