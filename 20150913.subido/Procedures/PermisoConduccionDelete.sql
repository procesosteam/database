USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionDelete]    Script Date: 09/13/2015 18:41:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionDelete]
(
	@idPermiso int
)
AS
BEGIN

	DELETE
	FROM [PermisoConduccion]
	WHERE
		[idPermiso] = @idPermiso
END
