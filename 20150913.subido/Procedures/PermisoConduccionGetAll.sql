USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetAll]    Script Date: 09/13/2015 16:26:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionGetAll]
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	FROM [PermisoConduccion]
END
