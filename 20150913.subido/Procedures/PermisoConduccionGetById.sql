USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetById]    Script Date: 09/13/2015 16:26:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionGetById]
(
	@idPermiso int
)
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	FROM [PermisoConduccion]
	WHERE
		([idPermiso] = @idPermiso)

END
