USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetByLegajo]    Script Date: 09/13/2015 16:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionGetByLegajo]
(
	@idLegajo int
)
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
		
	FROM [PermisoConduccion]
	WHERE
		[idPersonal] = @idLegajo
END
