USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionUpdate]    Script Date: 09/13/2015 16:27:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PermisoConduccionUpdate]
(
	@idPermiso int,
	@Descripcion varchar(100),
	@idPersonalACargo int,
	@idPersonal int,
	@Fecha smalldatetime,
	@FechaOriginal smalldatetime
)
AS
BEGIN

	UPDATE [PermisoConduccion]
	SET
		[Descripcion] = @Descripcion,
		[idPersonalACargo] = @idPersonalACargo,
		[idPersonal] = @idPersonal,
		[Fecha] = @Fecha,
		[FechaOriginal] = @FechaOriginal
	WHERE
		[idPermiso] = @idPermiso




END
