USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaAdd]    Script Date: 09/17/2015 16:15:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EspecificacionTecnicaAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@RequiereCuadrilla bit = NULL,
	@RequiereEquipo bit = NULL,
	@EquipoPropio bit = NULL,
    @RequierePersonal bit,
    @RequiereTraslado bit,
    @RequiereMateriales bit
)
AS
BEGIN


	INSERT
	INTO [EspecificacionTecnica]
	(
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	)
	VALUES
	(
		@Descripcion,
		@RequiereCuadrilla,
		@RequiereEquipo,
		@EquipoPropio,
		@RequierePersonal,
		@RequiereTraslado,
		@RequiereMateriales
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
