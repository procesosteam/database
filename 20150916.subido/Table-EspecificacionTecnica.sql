/*
   miércoles, 16 de septiembre de 201502:46:15 p.m.
   Usuario: userSIG
   Servidor: localhost
   Base de datos: SIG
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EspecificacionTecnica ADD
	RequiereTraslado bit NULL,
	RequierePersonal bit NULL,
	RequiereMaterial bit NULL
GO
ALTER TABLE dbo.EspecificacionTecnica SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
