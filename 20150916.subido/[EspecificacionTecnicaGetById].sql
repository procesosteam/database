USE [SIG]
GO

/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetById]    Script Date: 09/17/2015 17:36:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EspecificacionTecnicaGetById]
(
	@idEspecificaciones int
)
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica]
	WHERE
		([idEspecificaciones] = @idEspecificaciones)

END

GO

/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdSolicitud]    Script Date: 09/17/2015 17:36:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EspecificacionTecnicaGetByIdSolicitud]
(
	@idSolicitud int
)
AS
BEGIN

	SELECT
		et.[idEspecificaciones],
		et.[Descripcion],
		et.[RequiereCuadrilla],
		et.[RequiereEquipo],
		et.[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica] ET
	INNER JOIN SolicitudClienteEspecificacion TS ON TS.idEspecificacion = et.idEspecificaciones
	WHERE
		(TS.idSolicitudCliente = @idSolicitud)

END

GO

/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdTipoServicio]    Script Date: 09/17/2015 17:36:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EspecificacionTecnicaGetByIdTipoServicio]
(
	@idTipoServicio int
)
AS
BEGIN

	SELECT
		et.[idEspecificaciones],
		et.[Descripcion],
		et.[RequiereCuadrilla],
		et.[RequiereEquipo],
		et.[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica] ET
	INNER JOIN TipoServicioEspecificacion TS ON TS.idEspecificacion = et.idEspecificaciones
	WHERE
		(TS.idTipoServicio = @idTipoServicio)

END

GO


