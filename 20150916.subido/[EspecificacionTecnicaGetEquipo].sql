USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetEquipo]    Script Date: 09/17/2015 16:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EspecificacionTecnicaGetEquipo]
(
	@equipo bit,
	@cuadrilla bit,
	@personal bit,
	@traslado bit,
	@material bit
)
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
		RequierePersonal,
		RequiereTraslado,
		RequiereMaterial
	FROM [EspecificacionTecnica]
	where (isnull(RequiereEquipo,0) = @equipo or @equipo is null)
	and (isnull(RequiereCuadrilla,0) = @cuadrilla or @cuadrilla is null)
	and (isnull(RequierePersonal,0) = @personal or @personal is null)
	and (isnull(RequiereTraslado,0) = @traslado or @traslado is null)
	and (isnull(RequiereMaterial,0) = @material or @material is null)
	order by Descripcion
END
