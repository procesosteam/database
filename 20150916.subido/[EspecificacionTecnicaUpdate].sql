USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaUpdate]    Script Date: 09/17/2015 16:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EspecificacionTecnicaUpdate]
(
	@idEspecificaciones int,
	@Descripcion nvarchar(150),
	@RequiereCuadrilla bit,
	@RequiereEquipo bit,
	@EquipoPropio bit,
    @RequierePersonal bit,
    @RequiereTraslado bit,
    @RequiereMaterial bit
)
AS
BEGIN

	UPDATE [EspecificacionTecnica]
	SET
		[Descripcion] = @Descripcion,
		[RequiereCuadrilla] = @RequiereCuadrilla,
		[RequiereEquipo] = @RequiereEquipo,
		[EquipoPropio] = @EquipoPropio,
		[RequierePersonal] = @RequierePersonal,
		[RequiereTraslado] = @RequiereTraslado,
		[RequiereMaterial] = @RequiereMaterial
		
	WHERE
		[idEspecificaciones] = @idEspecificaciones




END
