USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]    Script Date: 09/24/2015 08:34:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]
(
	@idAcuerdo int
)
AS
BEGIN
SELECT DISTINCT  
				acc.idAcuerdoConcepto, 
				acc.Concepto,
				acc.Costo, 
				max(cast(replace(DBO.GETDIFF(ACCANT.COSTO,acc.Costo),' ', '')as float)) AS COSTODIF, 
				accant.Costo as costoAnt,
				acc.idEspecificacion,
				case when (accant.Costo <> acc.costo) then
				999999 
				else
				isnull(ACCANT.IDESPECIFICACION, 999999) end AS ESPDIF, 
				ACCANT.IDESPECIFICACION as ESPAnt,
				acc.idAcuerdoServicio, ACC. idAcuerdoServicio AS SERVDIF, 
				ET.Descripcion /*+  case when (accant.Costo <> acc.costo Or ACCANT.Costo IS null ) then
									'[' +convert(varchar,999999) +']' else '' end */
							  AS Especificacion,ac.orden
FROM            AcuerdoConceptoCosto  acc 
INNER JOIN		AcuerdoServicio ACS ON acc.idAcuerdoServicio = ACS.idAcuerdoServicio 
LEFT  JOIN EspecificacionTecnica ET ON acc.idEspecificacion = ET.idEspecificaciones

INNER JOIN ACUERDO A ON A.IDACUERDO = ACS.IDACUERDO /*ACUERDO ACTUAL*/
LEFT  JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = a.idCliente/*ACUERDO ANTERIOR*/
LEFT  JOIN ACUERDOSERVICIO ATANT ON ATANT.IDACUERDO = AANT.IDACUERDO AND ATANT.IDTIPOSERVICIO = ACS.IDTIPOSERVICIO and ACS.Requiere = ATANT.Requiere/*SERVICIOS ANTERIOR*/
LEFT  JOIN AcuerdoConceptoCosto ACCANT ON ACCANT.idAcuerdoServicio = ATANT.idAcuerdoServicio AND ACCANT.IDESPECIFICACION = ACC.IDESPECIFICACION /*ESPECIFICACIONES ANTERIOR*/
										and accant.costo = acc.costo
LEFT JOIN AcuerdoConcepto AC on ac.Concepto = acc.Concepto
WHERE        (ACS.idAcuerdo = @idAcuerdo)
GROUP BY acc.idAcuerdoConcepto, 
			acc.Concepto,
			acc.Costo, 
			acc.idEspecificacion,
			ACCANT.IDESPECIFICACION, 			
			acc.idAcuerdoServicio, 
			ACC.idAcuerdoServicio, 
			ET.Descripcion,accant.Costo,a.nroRevision
			,ac.orden
ORDER BY ac.orden
/*
SELECT * FROM AcuerdoConceptoCosto acc 
INNER JOIN		AcuerdoServicio ACS ON acc.idAcuerdoServicio = ACS.idAcuerdoServicio 
WHERE ACS.IDACUERDO IN(SELECT IDACUERDO FROM ACUERDO WHERE NROACUERDO LIKE 'AC%' AND BAJA = 0)
*/

END
