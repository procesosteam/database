USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]    Script Date: 10/01/2015 09:57:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]
(
@IDACUERDO INT
)
AS
--declare @IDCLIETE INT= 40
BEGIN

	SELECT DISTINCT
		AT.[idAcuerdoConceptoTrailer],
		AT.[idAcuerdoConcepto],
		AT.[idTrailer],
		vi.NroCodificacion + ' ' + c.Codigo AS Trailer,
		v.idVehiculo,
		TS.Descripcion AS TIPOSERVICIO,
		TS.idTipoServicio,
		ET.Descripcion AS ESPECIFICACION
		
	FROM [AcuerdoConceptoTrailer] AT
	INNER JOIN AcuerdoConceptoCosto ACC ON ACC.idAcuerdoConcepto = AT.idAcuerdoConcepto
	INNER JOIN AcuerdoServicio ASE ON ASE.idAcuerdoServicio = ACC.idAcuerdoServicio
	INNER JOIN Acuerdo A ON A.idAcuerdo = ASE.idAcuerdo
	INNER JOIN Vehiculo v ON v.idVehiculo = AT.idTrailer
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion =vi.idCodificacion
	INNER JOIN TipoServicio TS ON TS.idTipoServicio = ASE.idTipoServicio
	INNER JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = ACC.idEspecificacion
	WHERE A.idAcuerdo = @IDACUERDO
	AND AT.idTrailer not in(select idTrailer from AcuerdoConceptoTrailer AT2
							INNER JOIN AcuerdoConceptoCosto ACC2 ON ACC2.idAcuerdoConcepto = AT2.idAcuerdoConcepto
							INNER JOIN AcuerdoServicio ASE2 ON ASE2.idAcuerdoServicio = ACC2.idAcuerdoServicio
							INNER JOIN Acuerdo A2 ON A2.idAcuerdo = ASE2.idAcuerdo
							where A2.idCliente <> A.idCliente and A2.baja = 0)
							

END
