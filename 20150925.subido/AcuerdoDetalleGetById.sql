USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDetalleGetById]    Script Date: 09/30/2015 16:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoDetalleGetById]
(
	@idAcuerdo int
)
AS

BEGIN

	
	select distinct ac.*,
			acc.idAcuerdoConcepto,
			acc.Concepto,
			acc.Costo,
			acc2.Costo as cant,
			ts.Descripcion as tipoServicio, 
			acc.idEspecificacion,
			et.Descripcion as especificacion,
			v.idVehiculo,
			vi.NroCodificacion + ' ' + c.Codigo as codificacion
	from AcuerdoServicio ac
	inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
	inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = ac.idAcuerdoServicio and acc.Concepto not like '%Cant%'
	inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like '%Cant%' 
	inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and acc2.idEspecificacion = et.idEspecificaciones
	inner join EspecificacionDetalle ed on ed.idEspecificacion = acc.idEspecificacion and acc2.idEspecificacion = et.idEspecificaciones
	inner join Vehiculo v on v.idVehiculo = ed.idEquipoPropio
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	inner join Acuerdo a on a.idAcuerdo = ac.idAcuerdo
	where ac.idAcuerdo = @idAcuerdo and a.baja = 0
	and acc.Concepto like '%Mensual%'
	and acc2.Concepto like '%Mensual%'
	and v.idVehiculo not in(select AT2.idTrailer from AcuerdoConceptoTrailer AT2
							INNER JOIN AcuerdoConceptoCosto ACC2 ON ACC2.idAcuerdoConcepto = AT2.idAcuerdoConcepto
							INNER JOIN AcuerdoServicio ASE2 ON ASE2.idAcuerdoServicio = ACC2.idAcuerdoServicio
							INNER JOIN Acuerdo A2 ON A2.idAcuerdo = ASE2.idAcuerdo
							where A2.idCliente <> a.idCliente and A2.baja = 0)


END
