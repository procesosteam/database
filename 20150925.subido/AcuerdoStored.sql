USE [SIG]
GO

/****** Object:  StoredProcedure [dbo].[AcuerdoAdd]    Script Date: 10/02/2015 19:39:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AcuerdoAdd]
(
	@nroAcuerdo varchar(50),
	@idCliente int,
	@fechaInicio datetime = NULL,
	@fechaFin datetime = NULL,
	@fechaEmision datetime = NULL,
	@nroRevision int = NULL,
	@titulo varchar(250) = NULL,
	@tipoMoneda varchar(50) = NULL,
	@idSector int
)
AS
BEGIN


	INSERT
	INTO [Acuerdo]
	(
		[titulo],
		[nroAcuerdo],
		[idCliente],
		[fechaInicio],
		[fechaFin],
		[fechaEmision],
		[nroRevision],
		[tipoMoneda],
		[idSector]	
	
	)
	VALUES
	(
		@titulo,
		@nroAcuerdo,
		@idCliente,
		@fechaInicio,
		@fechaFin,
		@fechaEmision,
		@nroRevision,
		@tipoMoneda,
		@idSector
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

GO

/****** Object:  StoredProcedure [dbo].[AcuerdoGetAll]    Script Date: 10/02/2015 19:39:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AcuerdoGetAll]
AS
BEGIN

	SELECT
		[idAcuerdo],
		[nroAcuerdo],
		[titulo],
		[idCliente],
		[fechaInicio],
		[fechaFin],
		[fechaEmision],
		[nroRevision],
		[tipoMoneda],
		[idSector]
	FROM [Acuerdo]
	WHERE BAJA = 0
	ORDER BY NROACUERDO, NROREVISION
END

GO

/****** Object:  StoredProcedure [dbo].[AcuerdoGetById]    Script Date: 10/02/2015 19:39:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AcuerdoGetById]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT DISTINCT
		A.[idAcuerdo],
		A.[nroAcuerdo],
		A.[titulo],
		DBO.GETDIFF(A.[titulo],AANT.[titulo]) AS TITULODIF,
		A.[idCliente],
		E.RAZONSOCIAL,
		A.[fechaInicio],
		A.[fechaFin],
		A.[fechaEmision],
		A.[nroRevision],
		A.[tipoMoneda],
		AT.detalle as header,
		A.idSector
	FROM [Acuerdo] A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = E.idEmpresa
	LEFT JOIN AcuerdoTitulo AT on AT.idAcuerdo = A.idAcuerdo and AT.titulo = 'header'
	WHERE
		(A.[idAcuerdo] = @idAcuerdo)
		AND A.baja = 0

END


GO


/****** Object:  StoredProcedure [dbo].[AcuerdoUpdate]    Script Date: 10/02/2015 19:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoUpdate]
(
	@idAcuerdo int,
	@nroAcuerdo varchar(50),
	@idCliente int,
	@fechaInicio datetime,
	@fechaFin datetime,
	@baja bit,
	@fechaEmision datetime = NULL,
	@nroRevision int = NULL,
	@titulo varchar(250) = NULL,
	@tipoMoneda varchar(50) = null,
	@idSector int
)
AS
BEGIN

	UPDATE [Acuerdo]
	SET
		[titulo] = @titulo,
		[nroAcuerdo] = @nroAcuerdo,
		[idCliente] = @idCliente,
		[fechaInicio] = @fechaInicio,
		[fechaFin] = @fechaFin,
		[baja] = @baja,
		[fechaEmision] = @fechaEmision,
		[nroRevision] = @nroRevision,
		[tipoMoneda] = @tipoMoneda,
		[idSector] = @idSector
	WHERE
		[idAcuerdo] = @idAcuerdo




END
