USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetAll]    Script Date: 09/30/2015 14:57:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EquiposGetAll]
AS
BEGIN

	SELECT
		[idEquipo],
		[Descripcion],
		idEmpresa
	FROM [Equipos]
	ORDER BY Descripcion
END
