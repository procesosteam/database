/*PANTALLA GESTION CUADRILLA
- NO SE PUEDE MODIFICAR
- SOLO DE INFORMACION
- QUE SE PUEDA EXPORTAR A EXCEL
*/

select c.Nombre as Cuadrilla
		,p.Apellido + ', ' + p.Nombres as Persona
		, s.Descripcion as Sector
		,cp.FechaActivo as FechaInicio
		,cp.FechaBaja as FechaDesafectacion
from Cuadrilla c
inner join CuadrillaPersonal cp on cp.idCuadrilla = c.idCuadrilla
inner join Personal p on p.idPersonal= cp.idPersonal
inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
inner join Sector s on s.idSector = pl.idSector
inner join Empresa e on e.idEmpresa = pl.idEmpresa

order by Persona