/*
   viernes, 02 de octubre de 201507:36:46 p.m.
   Usuario: userSIG
   Servidor: localhost
   Base de datos: SIG
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acuerdo ADD
	idSector int NULL
GO
ALTER TABLE dbo.Acuerdo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
