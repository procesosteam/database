USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetById]    Script Date: 10/02/2015 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AcuerdoGetById]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT DISTINCT
		A.[idAcuerdo],
		A.[nroAcuerdo],
		A.[titulo],
		DBO.GETDIFF(A.[titulo],AANT.[titulo]) AS TITULODIF,
		A.[idCliente],
		E.RAZONSOCIAL,
		A.[fechaInicio],
		A.[fechaFin],
		A.[fechaEmision],
		A.[nroRevision],
		A.[tipoMoneda],
		AT.detalle as header,
		A.idSector,
		s.Descripcion as Sector
	FROM [Acuerdo] A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = E.idEmpresa
	LEFT JOIN AcuerdoTitulo AT on AT.idAcuerdo = A.idAcuerdo and AT.titulo = 'header'
	left join Sector s on s.idSector = a.idSector
	WHERE
		(A.[idAcuerdo] = @idAcuerdo)
		AND A.baja = 0

END


