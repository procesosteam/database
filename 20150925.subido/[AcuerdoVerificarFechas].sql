USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoVerificarFechas]    Script Date: 10/02/2015 21:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoVerificarFechas]
	(		
		@fechaDesde datetime,
		@fechaHasta datetime,
		@idCliente int,
		@idSector int
	)
	
AS
BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		AMAX.NROREVISION,
		A.TITULO
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN (SELECT MAX(IDACUERDO)AS IDACUERDO, MAX(ISNULL(NROREVISION, 1)) AS NROREVISION, NROACUERDO
				FROM ACUERDO
				GROUP BY NROACUERDO) AMAX ON  AMAX.IDACUERDO = A.IDACUERDO
	WHERE A.BAJA = 0	
	AND (A.IDCLIENTE = @IDCLIENTE OR @IDCLIENTE IS NULL) 
	AND (A.idSector = @idSector or @idSector is null) and (
		(fechainicio between @fechaDesde and @fechaHasta) or
		(fechafin between @fechaDesde and @fechaHasta ) or 
		(fechainicio <= @fechaDesde and fechafin > @fechaHasta ) or 
		(fechainicio >= @fechaDesde and fechafin < @fechaHasta )
    )
	

END
