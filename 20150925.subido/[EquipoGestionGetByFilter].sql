USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EquipoGestionGetByFilter]    Script Date: 09/25/2015 17:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EquipoGestionGetByFilter]
	(
		@patente varchar(250),
		@ruta varchar(250),
		@vCedula int,
		@vPatente int,
		@vtv varchar(50),
		@idCodificacion int,
		@idEmpresa int
		
	)
	
AS

BEGIN

	SELECT V.idVehiculo,
		V.Patente,		
		E.RazonSocial AS EMPRESA,
		M.Descripcion AS MARCA,
		SE.Descripcion AS Sector,
		(C.Codigo) + ' '+ CONVERT(VARCHAR(10),vi.NroCodificacion) AS Codificacion ,
		CONVERT(VARCHAR(10),VV.vencimientoCedula, 103) AS vencimientoCedula,
		CONVERT(VARCHAR(10),VV.vencimientoPatente, 103) AS vencimientoPatente,
		CONVERT(VARCHAR(10),VV.vencimientoLeasing, 103) AS vencimientoLeasing,
		CONVERT(VARCHAR(10),VV.vencimientoVTV, 103) AS vencimientoVTV,
		CONVERT(VARCHAR(10),VV.vencimiento, 103) AS vencimiento,
		CONVERT(VARCHAR(10),VS.vigenciaHasta, 103) AS VencimientoSeguro
		
	FROM Vehiculo V
	left JOIN VehiculoIdentificacion vi on vi.idVehiculo  = v.idVehiculo
	left JOIN VehiculoVencimiento VV ON V.idVehiculo = VV.idVehiculo 
	left JOIN Codificacion C ON C.idCodificacion = vi.idCodificacion
	left JOIN EMPRESA E ON E.idEmpresa = V.idEmpresa
	left JOIN Sector SE ON SE.idSector = V.idSector
													
	left JOIN Marca M ON M.idMarca = V.idMarca
	left JOIN VehiculoSeguro VS ON VS.idVehiculo = V.idVehiculo
				
	WHERE
	(V.esEquipo = 1)
	AND (V.baja = 0)
	AND (C.idCodificacion = @idCodificacion OR @idCodificacion IS NULL)
	AND (V.Patente LIKE '%' + @patente + '%' OR (@patente IS NULL) )
	AND (V.idEmpresa = @idEmpresa or @idEmpresa is NULL)	
	AND (VV.vencimientoCedula >= @vCedula OR @vCedula IS NULL)
	AND (VV.vencimientoPatente >= @vPatente OR @vPatente IS NULL)
	AND (VV.NroVTV = @vtv OR @vtv IS NULL)
	AND (VV.RUTA = @ruta OR @ruta IS NULL)
	
		
	ORDER BY VV.vencimientoPatente,VS.vigenciaHasta, VV.vencimiento, VV.vencimientoVTV, VV.vencimientoCedula, VV.vencimientoLeasing,V.Patente desc

END
