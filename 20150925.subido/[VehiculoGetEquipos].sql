USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetEquipos]    Script Date: 09/30/2015 10:03:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VehiculoGetEquipos]
AS
BEGIN

	SELECT
		v.[idVehiculo],
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		v.[alta],
		v.[baja] ,
		esEquipo,
		c.Codigo + ' ' + convert(varchar,vi.NroCodificacion) as codificacion
	FROM [Vehiculo] v
	inner join VehiculoVencimiento vv on vv.idVehiculo = v.idVehiculo
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	WHERE
		[esEquipo] = 1
		and v.baja = 0
		and vv.vencimiento > GETDATE()
		and vv.vencimientoPatente > GETDATE()
	ORDER BY C.Descripcion, vi.NroCodificacion
END
