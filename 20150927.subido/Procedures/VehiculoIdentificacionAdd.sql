USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionAdd]    Script Date: 09/27/2015 01:49:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VehiculoIdentificacionAdd]
(
	@idVehiculo int,
	@idCodificacion int,
	@NroTacografo varchar(50) = NULL,
	@Alta bit = NULL,
	@Baja bit = NULL,
	@NroCodif varchar(50)
)
AS
BEGIN


	INSERT
	INTO [VehiculoIdentificacion]
	(
		[idVehiculo],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion]
	)
	VALUES
	(
		@idVehiculo,
		@idCodificacion,
		@NroTacografo,
		@Alta,
		@Baja,
		@NroCodif
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
