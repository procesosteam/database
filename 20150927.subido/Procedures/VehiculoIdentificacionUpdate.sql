USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionUpdate]    Script Date: 09/27/2015 01:48:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VehiculoIdentificacionUpdate]
(
	@idIdentificacion int,
	@idVehiculo int,
	@idCodificacion int,
	@NroTacografo varchar(50),
	@Alta bit,
	@Baja bit,
	@NroCodif varchar(50)
)
AS
BEGIN

	UPDATE [VehiculoIdentificacion]
	SET
		[idVehiculo] = @idVehiculo,
		[idCodificacion] = @idCodificacion,
		[NroTacografo] = @NroTacografo,
		[Alta] = @Alta,
		[Baja] = @Baja,
		[NroCodificacion] = @NroCodif
	WHERE
		[idIdentificacion] = @idIdentificacion




END
