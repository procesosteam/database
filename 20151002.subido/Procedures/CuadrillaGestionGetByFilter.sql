USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGestionGetByFilter]    Script Date: 10/02/2015 18:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*PANTALLA GESTION CUADRILLA
- NO SE PUEDE MODIFICAR
- SOLO DE INFORMACION
- QUE SE PUEDA EXPORTAR A EXCEL
*/
ALTER PROCEDURE [dbo].[CuadrillaGestionGetByFilter]
(
		@desde datetime,
		@hasta datetime,
		@idPersona int,
		@idCuadrilla int,
		@idSector int,
		@idEmpresa int
		
	)
	AS
	BEGIN
		SELECT  c.idCuadrilla
				,c.Nombre as Cuadrilla
				,p.Apellido + ', ' + p.Nombres as Persona
				, s.Descripcion as Sector
				,cp.FechaActivo as FechaInicio
				,cp.FechaBaja as FechaDesafectacion
		FROM Cuadrilla c
		inner join CuadrillaPersonal cp on cp.idCuadrilla = c.idCuadrilla 
		inner join Personal p on p.idPersonal= cp.idPersonal
		inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
		inner join Sector s on s.idSector = pl.idSector
		inner join Empresa e on e.idEmpresa = pl.idEmpresa
		WHERE
			(e.idEmpresa = @idEmpresa or @idEmpresa IS NULL)
		AND (s.idSector = @idSector OR @idSector IS NULL)
		AND (p.idPersonal = @idPersona OR @idPersona IS NULL)
		AND (cp.idCuadrilla = @idCuadrilla OR @idCuadrilla IS NULL) 

		order by Persona
end