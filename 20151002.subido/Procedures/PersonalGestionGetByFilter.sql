USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGestionGetByFilter]    Script Date: 10/02/2015 13:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PersonalGestionGetByFilter]
	(
		@ApNom varchar(250),
		@DNI int,
		@nroLegajo varchar(50),
		@idEmpresa int,
		@idSector int,
		@vtoCarnet datetime,
		@vtoPsico datetime,
		@vtoGrales datetime,
		@vtoPeligrosa datetime,
		@vtoMDef datetime
	)
	
AS

BEGIN

	SELECT P.Apellido,
		P.Nombres,		
		CONVERT(VARCHAR(10),P.FechaNacimiento, 103) AS FECHANAC,
		E.RazonSocial AS EMPRESA,
		P.DNI,
		S.Descripcion AS Sexo,
		SE.Descripcion AS Sector,
		P.NroLegajo,
		P.idPersonal,
		CONVERT(VARCHAR(10),PC.VtoCargasGenerales, 103) AS VtoCargasGenerales,
		CONVERT(VARCHAR(10),PC.VtoCargasPeligrosas, 103) AS VtoCargasPeligrosas,
		CONVERT(VARCHAR(10),PC.VtoCarnet, 103) AS VtoCarnet,
		CONVERT(VARCHAR(10),PC.VtoPsicofisico, 103) AS VtoPsicofisico,
		CONVERT(VARCHAR(10),PC.VtoManejoDefensivo, 103) AS VtoManejoDefensivo
		
		
		
		
	FROM Personal P
	INNER JOIN Sexo S ON P.idSexo = S.idSexo
	left JOIN PersonalDatosLaborales L ON P.idPersonalDatosLaborales = L.idPersonalDatosLaborales
	left JOIN EMPRESA E ON E.IDEMPRESA = L.idEmpresa AND (E.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
	left JOIN PersonalConduccion PC on P.idPersonalConduccion = PC.idPersonalConduccion 
													AND ( (PC.VtoCarnet >= @vtoCarnet OR @vtoCarnet IS NULL)
													AND (PC.VtoPsicofisico >= @vtoPsico OR @vtoPsico IS NULL)
													AND (PC.VtoCargasGenerales >= @vtoGrales OR @vtoGrales IS NULL)
													AND (PC.VtoCargasPeligrosas >= @vtoPeligrosa OR @vtoPeligrosa IS NULL)
													AND (PC.VtoManejoDefensivo >= @vtoMDef OR @vtoMDef IS NULL) )
	left JOIN PersonalInformacionMedica PM on P.idPersonalInformacionMedica = PM.idPersonalInformacionMedica
	left JOIN Sector SE on SE.idSector = L.idSector	AND (SE.idSector = @idSector OR @idSector IS NULL)
				
				
	WHERE
	(P.BAJA = 0)
	AND ( P.Apellido + P.Nombres LIKE '%' + @ApNom + '%' OR (@ApNom IS NULL) )
	AND (P.DNI = @DNI OR @DNI IS NULL)
	AND (P.NroLegajo =@nroLegajo OR @nroLegajo IS NULL)
	AND ((SE.idSector = @idSector) OR (@idSector IS NULL) )
	AND ((L.idEmpresa = @idEmpresa) OR (@idEmpresa IS NULL))
	
	
	
	
	ORDER BY P.Apellido , P.Nombres desc

END
