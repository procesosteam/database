USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetAll]    Script Date: 10/02/2015 18:52:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PersonalGetAll]
AS
BEGIN

	SELECT
		[idPersonal],
		[NroLegajo],
		[Apellido],
		[Nombres],
		[FechaNacimiento],
		[idSexo],
		[DNI],
		[CUIL],
		[idEstadoCivil],
		[idEstudios],
		[idNacionalidad],
		[idLocalidad],
		[Domicilio],
		[Telefono],
		[Mail],
		[idUsuarioAcceso],
		[idPersonalContacto],
		[idPersonalDatosLaborales],
		[idPersonalInformacionMedica],
		[idPersonalConduccion]
	FROM [Personal]
	ORDER BY [Personal].Apellido
END
