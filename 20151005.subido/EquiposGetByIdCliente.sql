USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetById]    Script Date: 10/08/2015 18:49:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].EquiposGetByIdCliente
(
	@idCliente int
)
AS
BEGIN

	SELECT
		[idEquipo],
		[Descripcion],
		idEmpresa
	FROM [Equipos]
	WHERE
		([idEmpresa] = @idCliente)

END
