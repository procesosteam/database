USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetByFilter]    Script Date: 10/06/2015 13:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[OrdenTrabajoGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer inT = null	
)
AS
BEGIN
DECLARE 
	
	@NroSolicitudPARAM int,
	@idEmpresaPARAM int,
	@fechaDesdePARAM datetime,
	@fechaHastaPARAM datetime,
	@nroPartePARAM int,
	@idTRailerPARAM int 
	
SET @NroSolicitudPARAM = @NroSolicitud
SET @idEmpresaPARAM = @idEmpresa
SET @fechaDesdePARAM  = @fechaDesde
SET @fechaHastaPARAM = @fechaHasta 
SET @nroPartePARAM = @nroParte 
SET @idTRailerPARAM = @idTRailer
	
SELECT	OT.idOrden,
		OT.idTrailer, 
		ISNULL(SC.NroPedidoCliente, str(SC.NroSolicitud) +'-'+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud))as NroSolicitud,		
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		e.Descripcion AS EMPRESA,
		sec.Descripcion as SOLICITANTE,
		ts.Descripcion as TipoServicio,
		c.Codigo +' ' + vi.NroCodificacion as Trailer,
		SC.idSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		isnull(SC.Entrega, 1) as entrega,
		PE.NroParte
FROM  OrdenTrabajo OT
LEFT JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
left join Vehiculo v on v.idVehiculo = OT.idTrailer
left join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
left join Codificacion c on c.idCodificacion = vi.idCodificacion
	
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
left join Solicitante s on (s.idSolicitante = SC.idSolicitante and s.idSector = SC.idSector)	
left join Sector sec on sec.idSector = SC.idSector
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0
		AND (SC.NroSolicitud = @NroSolicitudPARAM OR @NroSolicitudPARAM IS NULL )
		AND (SC.Fecha >= @fechaDesdePARAM OR @fechaDesdePARAM IS NULL)
		AND (SC.Fecha <= @fechaHastaPARAM OR @fechaHastaPARAM IS NULL)
		AND (e.IDEMPRESA = @idEmpresaPARAM OR @idEmpresaPARAM IS NULL)		
		AND (PE.NroParte = @nroPartePARAM or @nroPartePARAM is null)
		AND (v.idVehiculo = @idTRailerPARAM or @idTRailerPARAM is null)
AND SC.Baja = 0
-- and isnull(pe.nroremito,0)=0
ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
END
