USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoRegresoGetByFilter]    Script Date: 10/07/2015 09:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[OrdenTrabajoRegresoGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime
)
AS
BEGIN

SELECT	OT.idOrden,
		OT.idTrailer, 
		c.Codigo + ' ' + vi.NroCodificacion,
		SC.NroSolicitud,
		SC.TIPOSERVICIO,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		SC.EMPRESA,
		SC.SOLICITANTE,
		SC.idSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega
FROM  OrdenTrabajo OT
INNER JOIN v_SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
LEFT JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 

WHERE OT.Baja = 0
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (sc.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)
		AND SC.EntregaSolicitud = 0
		--AND SC.NroSolicitud not in (SELECT NroSolicitud 
		--							FROM SolicitudCliente 
		--							WHERE ENTREGA = 1
		--							and Baja = 0)

END
