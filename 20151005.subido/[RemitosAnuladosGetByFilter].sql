USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[RemitosAnuladosGetByFilter]    Script Date: 10/06/2015 13:55:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[RemitosAnuladosGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer int = null,
	@idSector int,
	@idTipoServicio int,
	@idCondicion int
		
)
AS
BEGIN

SELECT	
		SC.idSolicitud,
		str(SC.NroSolicitud) +' '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud) as NroSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		ISNULL(PE.NroParte,0) as NroParte,
		ISNULL(R.nroRemito,0) as nroRemito,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		ISNULL(OT.idOrden,0) as idOrden,
		ISNULL(OT.idTrailer,0) as idTrailer, 
		e.Descripcion AS EMPRESA,
		sec.Descripcion as Sector,
		ts.Descripcion as TipoServicio,
		c.Codigo + ' ' + vi.NroCodificacion as Trailer,
		isnull(R.idRemito,0) as idRemito,
		isnull(PE.idParteEntrega,0) as idParte,
		isnull(impreso,0) as impreso,
		ISNULL(anulado,0) as anulado
		
FROM  OrdenTrabajo OT
INNER JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
INNER JOIN REMITOS R ON R.idParte = PE.idParteEntrega 
left join Vehiculo v on v.idVehiculo = OT.idTrailer
left join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
left join Codificacion c on c.idCodificacion = vi.idCodificacion

left join Sector sec on (sec.idSector = SC.idSector)
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0 AND R.anulado = 1
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)		
		AND (PE.NroParte = @nroParte or @nroParte is null)
		AND (v.idVehiculo = @idTRailer or @idTRailer is null)
		AND (SC.idSector = @idSector OR @idSector IS NULL)
		AND (SC.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
AND SC.Baja = 0
ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC

END
