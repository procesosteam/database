USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[RemitosBonificadosGetByFilter]    Script Date: 10/07/2015 08:47:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[RemitosBonificadosGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer int = null,
	@idSector int,
	@idTipoServicio int,
	@idCondicion int,
	@nroRemito int = null
		
)
AS
BEGIN

SELECT	
		SC.idSolicitud,		
		isnull(sc.NroPedidoCliente,str(SC.NroSolicitud) +' '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as NroSolicitud,
		sc.NroPedidoCliente,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		PE.NroParte as NroParte,
		R.nroRemito as nroRemito,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		ISNULL(OT.idOrden,0) as idOrden,
		ISNULL(OT.idTrailer,0) as idTrailer, 
		e.Descripcion AS EMPRESA,
		sec.Descripcion as Sector,
		ts.Descripcion as TipoServicio,
		c.Codigo + ' ' + vi.NroCodificacion as Trailer,
		isnull(R.idRemito,0) as idRemito,
		isnull(PE.idParteEntrega,0) as idParte,
		isnull(impreso,0) as impreso,
		ISNULL(anulado,0) as anulado
		
FROM  OrdenTrabajo OT
INNER JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
LEFT JOIN RemitoDetalle RD ON RD.IDPARTE = PE.idParteEntrega
LEFT JOIN REMITOS R ON R.idRemito = RD.idRemito AND R.anulado = 0
LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
left join Solicitante s on (s.idSolicitante = SC.idSolicitante and s.idSector = SC.idSector)	
left join Sector sec on (sec.idSector = SC.idSector)
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0 
AND SC.Baja = 0
AND R.nroRemito = 0
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)		
		AND (PE.NroParte = @nroParte or @nroParte is null)
		AND (v.idVehiculo = @idTRailer or @idTRailer is null)
		AND (SC.idSector = @idSector OR @idSector IS NULL)
		AND (SC.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
		AND (R.NROREMITO = @NROREMITO OR @NROREMITO IS NULL)

ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC

END
