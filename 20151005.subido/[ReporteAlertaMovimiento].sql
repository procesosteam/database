USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[ReporteAlertaMovimiento]    Script Date: 10/07/2015 08:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ReporteAlertaMovimiento]	

AS

BEGIN
	SELECT * FROM(
	SELECT convert(varchar,SC.Fecha,103) as Fecha,
			SC.NroSolicitud,
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +' '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NSolicitud],
			ts.Descripcion as [TipoServicio],	
			e.Descripcion as [Empresa],
			sec.Descripcion as [Sector],
			s.Nombre as [Solicitante],
			isnull(cod.Codigo + ' ' + vi.NroCodificacion,sc.servicioaux) as [NroEquipo],
			c.Descripcion as [Condición],			
			CASE WHEN (SELECT COUNT(*) FROM SolicitudCliente
						WHERE NroSolicitud = SC.NroSolicitud AND BAJA = 0) >= 2 THEN					   
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = 'B' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			ELSE
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = 'Z' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE datediff(D, SC.Fecha, getDate() )   END
				END
			END AS [CantDias],
			lor.Descripcion as [Origen],
			m.pozoOrigen as [PozoOrigen],			
			lde.Descripcion as [Destino],
			m.pozoDestino as[PozoDestino],			
			SC.idSolicitud,
			ot.idOrden, 
			sc.fecha as fechaSC
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante
	left join Sector sec on (sec.idSector = SC.idSector)						
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join Condicion c on c.idCondicion = SC.idCondicion
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 		
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	left join Chofer chofer on chofer.idChofer = ot.idChofer
	left join Chofer acomp on acomp.idChofer = ot.idAcompanante
    left join Transporte tr on tr.idTransporte = ot.idTransporte
	WHERE SC.Baja = 0 
	AND ot.Baja=0
	AND (SC.Entrega = 1 AND SC.NroSolicitud NOT IN(SELECT NroSolicitud 
													FROM SolicitudCliente 															
													WHERE Entrega = 0
													AND Baja = 0))
	AND sc.idcondicion in (SELECT IDCONDICION FROM CONDICION WHERE DESCRIPCION LIKE 'DIARIO')
	)SC
	WHERE SC.CantDias >= 15
	ORDER BY SC.fechaSC DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
	
END
