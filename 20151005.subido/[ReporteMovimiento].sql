USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[ReporteMovimiento]    Script Date: 10/07/2015 08:51:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ReporteMovimiento]	
--declare	
	@fechaDesde datetime = null,
	@fechaHasta datetime = null,
	@idEmpresa int = null,
	@idSector int = null,
	@idCondicion int = null,
	@idTrailer int = null,
	@idLugarOrigen int = null,
	@idLugarDestino int = null,
	@idChofer int = null,
	@idVehiculo int = null,
	@NroSolicitud int = null,
	@idTipoServicio int = null,
	@remito bit
AS

BEGIN

	SELECT convert(varchar,SC.Fecha,103) as Fecha,
			--SC.NroSolicitud as [N° Solicitud],
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +' '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NSolicitud],
			ts.Descripcion as [Tipo de Servicio],	
			case when isnull(pe.Conexion,0) = 0 then 'N' else 'S' end as [Conexión],
			case when isnull(pe.Desconexion ,0) = 0 then 'N' else 'S' end as [Desconexión],
			e.Descripcion as [Empresa],
			sec.Descripcion as [Sector],
			s.Nombre as [Solicitante],
			isnull(cod.Codigo + ' ' + vi.NroCodificacion,sc.servicioaux) as [N° Equipo],
			c.Descripcion as [Condición],
			convert(varchar,ScRegreso.Fecha,103) as [Fecha Regreso],
			CASE WHEN (SELECT COUNT(*) FROM SolicitudCliente
						WHERE NroSolicitud = SC.NroSolicitud AND BAJA = 0) > 2 THEN					   
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = 'B' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			ELSE
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = 'Z' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			END AS [Cant. Días],
			lor.Descripcion as [Origen],
			m.pozoOrigen as [Pozo Origen],			
			lde.Descripcion as [Destino],
			m.pozoDestino as[Pozo Destino],
                        tr.itemverificacion as [Vehículo],
			convert(int,m.KmsEstimados) as [Kms],
			chofer.Apellido + ', ' + chofer.Nombre  as [Chofer],
			acomp.Apellido + ', ' +	acomp.Nombre  as [Acompañante],
			pe.NroParte as [N° Parte],
			pe.nroRemito as [N° Remito],
			u.nombre as Usuario
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante
	left join Sector sec on (sec.idSector = SC.idSector)						
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join Condicion c on c.idCondicion = SC.idCondicion
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 		
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	left join Chofer chofer on chofer.idChofer = ot.idChofer
	left join Chofer acomp on acomp.idChofer = ot.idAcompanante
    left join Transporte tr on tr.idTransporte = ot.idTransporte
	left join SolicitudCliente ScRegreso on ScRegreso.NroSolicitud = SC.NroSolicitud 
											and ScRegreso.Entrega = 0 and ScRegreso.Baja = 0 
	left join usuario u on u.idUsuario = sc.idUsuario
	WHERE SC.Baja = 0 AND ot.Baja=0
		AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
		AND (sec.idSector = @idSector or @idSector IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
        AND (ot.idTrailer = @idTrailer OR @idTrailer IS NULL)
		AND (M.idLugarOrigen = @idLugarOrigen OR @idLugarOrigen IS NULL)
		AND (M.idLugarDestino = @idLugarDestino OR @idLugarDestino IS NULL)
		AND (SC.idUsuario = @idChofer OR @idChofer IS NULL)
		AND (ot.idTransporte = @idVehiculo OR @idVehiculo IS NULL)
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
		AND (ts.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (((@remito = 1 and pe.nroremito is not null) or (@remito = 0 and pe.nroremito is null))
			or @remito is null)
	ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
END
