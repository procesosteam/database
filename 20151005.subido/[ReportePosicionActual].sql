USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[ReportePosicionActual]    Script Date: 10/07/2015 09:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ReportePosicionActual]
--declare	
	@idEmpresa int,
	@idSector int,
	@idCondicion int,
	@idTrailer int,
	@idLugarDestino int,
	@idTipoServicio int

AS

BEGIN

	SELECT  s.Empresa,
			s.Sector,
			s.[Condición],
			s.[Tipo de Servicio],
			c.Codigo + ' ' + vi.NroCodificacion as [N° Equipo],
			isnull(s.Origen,'En Base') as Origen,
			s.[Pozo Origen],
			s.Destino,
			s.[Pozo Destino],
			convert(varchar, s.fecha, 103) as Fecha,
			s.[N° Solicitud],
			s.[N° Parte],
			s.[N° Remito]
	FROM Vehiculo v
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	LEFT JOIN (
		SELECT 	e.Descripcion as [Empresa],
				sec.Descripcion as [Sector],
				c.Descripcion as [Condición],
				ts.Descripcion as [Tipo de Servicio],
				isnull(cod.Codigo + ' ' + vi.NroCodificacion,sc.servicioaux) as [N° Equipo],
				lor.Descripcion as [Origen],
				m.pozoOrigen as [Pozo Origen],			
				lde.Descripcion as [Destino],
				m.pozoDestino as[Pozo Destino],
				isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +' '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [N° Solicitud],
				pe.NroParte as [N° Parte],
				pe.nroRemito as [N° Remito], 
				OT.IDTRAILER,		
				SC.NROSOLICITUD,
				SC.IDSOLICITUD,
				SC.FECHA,
				e.idEmpresa,
				sec.idSector,
				c.idCondicion,
				ts.idTipoServicio,
				m.idLugarDestino
		FROM SolicitudCliente SC
		inner join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 	
		INNER JOIN (select * from dbo.UltimaOtiTrailer()) OTI ON OTI.IDsolicitud = sc.IDsolicitud
																--and OTI.fecha = SC.fecha	
		left join Solicitante s on s.idSolicitante = SC.idSolicitante
		left join Sector sec on (sec.idSector = SC.idSector)						
		left join Empresa e on e.idEmpresa = sec.idEmpresa
		left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
		left join Condicion c on c.idCondicion = SC.idCondicion
		LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
		LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
		LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
		left join Movimiento m on m.idMovimiento = SC.idMovimiento
		left join Lugar lDe on lDe.idLugar = m.idLugarDestino
		left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
		left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
		WHERE SC.Baja = 0 AND OT.BAJA = 0			
		) S
		ON S.IDTRAILER = v.idVehiculo
	WHERE  (S.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (S.idSector = @idSector or @idSector IS NULL)
			AND (S.idCondicion = @idCondicion OR @idCondicion IS NULL)
			AND (v.idVehiculo = @idTrailer OR @idTrailer IS NULL)
			AND (S.idLugarDestino = @idLugarDestino OR @idLugarDestino IS NULL)				
			AND (S.idTipoServicio = @idTipoServicio or @idTipoServicio is null)
	ORDER BY S.Fecha DESC,DBO.ABC(S.NroSolicitud, S.idSolicitud) DESC
	
END
