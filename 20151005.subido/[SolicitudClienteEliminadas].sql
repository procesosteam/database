USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteEliminadas]    Script Date: 10/07/2015 08:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SolicitudClienteEliminadas]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN
	
	SELECT  SC.idSolicitud,
			--SC.NroSolicitud,	
			SC.NroSolicitud,
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,
			ISNULL(c.Codigo + ' ' + vi.NroCodificacion, 'S/T') as Trailer,
			isnull(ot.idOrden,0) as idOrden,
			SC.Entrega,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.NuevoMovimiento,
			ISNULL(pe.idParteEntrega,0) as idParteEntrega
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante							
	left join Sector sec on (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio	
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud	and ot.Baja=0 
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	WHERE (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
			AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
			AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
			AND (v.idVehiculo = @idTRailer or @idTRailer is null)			
			AND SC.Baja = 1
	ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
			
END
