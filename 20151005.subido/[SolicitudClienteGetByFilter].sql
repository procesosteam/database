USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetByFilter]    Script Date: 10/07/2015 09:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[SolicitudClienteGetByFilter]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN

	SELECT distinct top 100 SC.idSolicitud,
			SC.NroSolicitud,
			sc.NroPedidoCliente,	
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,			
			isnull(c.Codigo + ' ' + vi.NroCodificacion, c2.Codigo + ' ' + vi2.NroCodificacion) as Trailer,
			isnull(ot.idOrden,0) as idOrden,
			isnull(SC.Entrega,0) as Entrega,
			SC.Fecha AS FECHSC,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.NUEVOMOVIMIENTO,
			DBO.ABC(SC.NroSolicitud, SC.idSolicitud) as Letra,
			dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud) as FechaRegreso,
			t2.idVehiculo
	FROM SolicitudCliente SC
	left join Solicitante s on (s.idSolicitante = SC.idSolicitante or SC.idSolicitante is null)
								and (s.idSector = SC.idSector)	
	left join Sector sec on (sec.idSector = s.idSector or s.idSector is null)
							and (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join SolicitudTrailerPosible st on sc.NroSolicitud = st.idSolicitud and sc.idTipoServicio = st.idTipoServicio
	left join Vehiculo t2 on t2.idVehiculo = st.idTrailer
	LEFT join VehiculoIdentificacion vi2 on vi2.idVehiculo = t2.idVehiculo
	LEFT join Codificacion c2 on c2.idCodificacion = vi2.idCodificacion 
	WHERE (NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
	AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
	AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
	AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
	AND (vi.idVehiculo = @idTRailer or @idTRailer is null)
	AND SC.Baja = 0
	AND ((SC.NroSolicitud IN (	SELECT NroSolicitud 
								FROM SOLICITUDCLIENTE 
								WHERE BAJA = 0 AND NUEVOMOVIMIENTO = 1 
								and NroSolicitud = SC.NroSolicitud
								and idTipoServicio <> SC.idTipoServicio) 
			AND NUEVOMOVIMIENTO = 1)
		 OR (NUEVOMOVIMIENTO =  0 
			AND NOT exists (SELECT *
							FROM SOLICITUDCLIENTE 
							WHERE BAJA = 0 AND NUEVOMOVIMIENTO = 1
							and NroSolicitud = SC.NroSolicitud
							and idTipoServicio = SC.idTipoServicio)))
	AND (SC.idSolicitud = (select MAX(idsolicitud) 
						   from SolicitudCliente
						   where Baja = 0
						   and NuevoMovimiento = 1
						   and NroSolicitud = SC.NroSolicitud
						   and idTipoServicio = SC.idTipoServicio)
		or NuevoMovimiento = 0)
	AND (SC.Entrega = 1 AND SC.NroPedidoCliente NOT IN(SELECT NroPedidoCliente
													FROM SolicitudCliente 															
													WHERE Entrega = 0
													AND Baja = 0))
	AND sc.NroSolicitud not in (select sc2.NroSolicitud
								from SolicitudCliente sc2
								where sc2.NroSolicitud = SC.NroSolicitud
								and sc2.Entrega = 0 and sc2.NuevoMovimiento = 0
								and sc2.NroPedidoCliente like '%Z')
	
								
	
	ORDER BY FECHSC DESC
END
