USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudRegresoGetByFilter]    Script Date: 10/07/2015 09:00:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SolicitudRegresoGetByFilter]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN
	
	SELECT distinct top 200 SC.idSolicitud,
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +' - '+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NroSolicitud],	
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,
			c.Codigo + ' ' + vi.NroCodificacion as Trailer,
			v.idVehiculo as IDTrailer,
			isnull(ot.idOrden,0) as idOrden,
			SC.Entrega,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.Fecha AS FECHAORD
	FROM SolicitudCliente SC
	left join Solicitante s on (s.idSolicitante = SC.idSolicitante or SC.idSolicitante is null)
								and (s.idSector = SC.idSector)	
	left join Sector sec on (sec.idSector = s.idSector or s.idSector is null)
							and (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud	and ot.Baja=0 
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	WHERE (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
			AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
			AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
			AND SC.Baja = 0
			and SC.Entrega = 0
			/*AND ScIda.Entrega = 1
			AND ScIda.Baja = 0*/
			AND (v.idVehiculo = @idTRailer or @idTRailer is null)
			
	ORDER BY FECHAORD DESC	
END
