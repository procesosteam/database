USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGet]    Script Date: 10/07/2015 09:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SolicitudTrailerPosibleGet]
(
	@nroSolicitud int,
	@idTipoServicio int
)

AS

BEGIN

	SELECT distinct v.idVehiculo,
			c.Codigo+ ' ' + vi.NroCodificacion as Descripcion
	FROM Vehiculo v 
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	INNER JOIN SolicitudTrailerPosible STP ON v.idVehiculo = STP.idTrailer
	WHERE idSolicitud = @nroSolicitud
	AND idTipoServicio = @idTipoServicio

END
