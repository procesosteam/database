USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetDisponibles]    Script Date: 10/06/2015 13:25:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TrailerGetDisponibles]
(
	@idEspecificacion VARCHAR(400)
)
AS

--DECLARE @idEspecificacion VARCHAR(400) = '1,2'


BEGIN

	SELECT DISTINCT  v.idVehiculo,
		c.Codigo + ' ' + vi.NroCodificacion as trailer,
		ed.idEspecificacion, 
		SC.NROSOLICITUD, 
		SC.ENTREGA,
		ET.Descripcion AS Especificacion,
		ts.idTipoServicio,
		TS.Descripcion as TipoServicio,
		1 as cant
	FROM Vehiculo v
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	left join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo
    LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = v.idVehiculo
	LEFT JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
										AND SC.Baja = 0 
                                        and  SC.Entrega = 0
										--and sc.nuevomovimiento = 1
										and UT.FECHA = SC.FechaAlta
										and sc.idTipoServicio = UT.idtipoServicio

	inner JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = ed.idEspecificacion
	inner JOIN TipoServicioEspecificacion TSE ON TSE.idEspecificacion = ET.idEspecificaciones
	inner JOIN TipoServicio TS ON TS.idTipoServicio = TSE.idTipoServicio
	WHERE  (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')) 
		                 OR @idEspecificacion IS NULL)
	and (sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null))
	
	order by trailer
END
