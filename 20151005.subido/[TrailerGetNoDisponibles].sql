USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetNoDisponibles]    Script Date: 10/07/2015 09:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TrailerGetNoDisponibles]
(
	@idEspecificacion VARCHAR(400) ,
	@NROSOLICITUD INT
)
AS
BEGIN

--DECLARE @idEspecificacion VARCHAR(400) ,
--@NROSOLICITUD INT
--SET @idEspecificacion = '32'
--SET @NROSOLICITUD = NULL

CREATE TABLE #DISP
(
	IDTRAILER INT,
	DESCRIPCION VARCHAR(150),
	IDESPECIFICACION INT, 
	NROSOLICITUD INT, 
	ENTREGA BIT,
	ESPECIFICACION VARCHAR(150),
	IDTIPOSERVICIO INT,
	TIPOSERVICIO VARCHAR(150),
	cant int
)

INSERT INTO #DISP
EXEC [TrailerGetDisponibles]@idEspecificacion
	
SELECT DISTINCT v.idVehiculo, 
				c.Codigo + ' ' + vi.NroCodificacion as trailer, 
				ed.IDESPECIFICACION,
				ET.Descripcion AS Especificacion,
				ts.idTipoServicio,
				TS.Descripcion as TipoServicio,
				1 AS CANT
FROM Vehiculo v 
inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
inner join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo
LEFT JOIN (SELECT DISTINCT OT.IDTRAILER, SC.NROSOLICITUD FROM ORDENTRABAJO OT
			INNER JOIN SOLICITUDCLIENTE SC ON SC.IDSOLICITUD = OT.IDSOLICITUDCLIENTE
			) OT ON OT.IDTRAILER = v.idVehiculo
inner JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = ed.idEspecificacion
inner JOIN TipoServicioEspecificacion TSE ON TSE.idEspecificacion = ET.idEspecificaciones
inner JOIN TipoServicio TS ON TS.idTipoServicio = TSE.idTipoServicio
WHERE vi.idVehiculo NOT IN (SELECT IDTRAILER FROM #DISP)
AND (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')) 
		                 OR @idEspecificacion IS NULL)
AND (OT.NROSOLICITUD = @NROSOLICITUD OR @NROSOLICITUD IS NULL)

DROP TABLE #DISP

END
