USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetVehiculos]    Script Date: 10/08/2015 15:38:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VehiculoGetVehiculos]
AS
BEGIN

	SELECT
		[idVehiculo],
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		[alta],
		[baja] ,
		esEquipo
	FROM [Vehiculo]
	WHERE
		[esEquipo] = 0
		and baja = 0
	ORDER BY Patente
END
