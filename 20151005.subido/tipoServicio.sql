USE [SIG]
GO

/****** Object:  StoredProcedure [dbo].[TipoServicioAdd]    Script Date: 10/06/2015 09:11:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TipoServicioAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@codTango varchar(50) = null,
	@Abreviacion varchar(10),
	@RequierePersonal bit,
	@idSector int,
	@RequiereKms bit,
	@RequiereMaterial bit,
	@RequiereCuadrilla bit,
	@MasEquipo bit,
    @RequiereTraslado bit,
    @RequiereEquipo bit,
    @RequiereKmsTaco bit,
    @RequiereChofer bit
)
AS
BEGIN


	INSERT
	INTO [TipoServicio]
	(	[Descripcion],
		[CodTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		[RequiereKms],
		RequiereMaterial,
		RequiereCuadrilla,		
		MasEquipo,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	)
	VALUES
	(	@Descripcion,
		@codTango,
		@Abreviacion,
		@RequierePersonal,
		@idSector,
		@RequiereKms,
		@RequiereMaterial,
		@RequiereCuadrilla,
		@MasEquipo,
		@RequiereTraslado,
		@RequiereEquipo ,
		@RequiereKmsTaco,
		@RequiereChofer 
		
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

/****** Object:  StoredProcedure [dbo].[TipoServicioGetAll]    Script Date: 10/06/2015 09:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[TipoServicioGetAll]
AS
BEGIN

	SELECT
		[idTipoServicio],
		[Descripcion],
		[codTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		RequiereKms,
		RequiereMaterial,
		RequiereCuadrilla,
		MasEquipo ,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	FROM [TipoServicio]
	WHERE ISNULL(BAJA,0) = 0
	ORDER BY Descripcion
END

GO

/****** Object:  StoredProcedure [dbo].[TipoServicioGetById]    Script Date: 10/06/2015 09:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[TipoServicioGetById]
(
	@idTipoServicio int
)
AS
BEGIN
	SELECT
		[idTipoServicio],
		[Descripcion],
		[codTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		RequiereKms,
		RequiereMaterial ,
		RequiereCuadrilla ,
		MasEquipo ,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	FROM [TipoServicio]
	WHERE
		([idTipoServicio] = @idTipoServicio)

END

GO

/****** Object:  StoredProcedure [dbo].[TipoServicioUpdate]    Script Date: 10/06/2015 09:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[TipoServicioUpdate]
(
	@idTipoServicio int,
	@Descripcion nvarchar(150),
	@codTango varchar(50),
	@Abreviacion varchar(10),
	@RequierePersonal bit,
	@idSector int,
	@RequiereKms bit,
	@RequiereMaterial bit,
	@RequiereCuadrilla bit,
	@MasEquipo bit,
    @RequiereTraslado bit,
    @RequiereEquipo bit,
    @RequiereKmsTaco bit,
    @RequiereChofer bit
)
AS
BEGIN

	UPDATE [TipoServicio]
	SET
		[Descripcion] = @Descripcion,
		codTango = @codTango,
		[Abreviacion] = @Abreviacion,
		[RequierePersonal] = @RequierePersonal,
		[idSector] = @idSector,
		[RequiereKms] = @RequiereKms,
		RequiereMaterial = @RequiereMaterial ,
		RequiereCuadrilla = @RequiereCuadrilla ,
		MasEquipo = @MasEquipo ,
		RequiereTraslado = @RequiereTraslado ,
		RequiereEquipo = @RequiereEquipo ,
		RequiereKmsTaco = @RequiereKmsTaco,
		RequiereChofer =	@RequiereChofer 
		
	WHERE
		[idTipoServicio] = @idTipoServicio


END

GO


