USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetTrailerDisponibles]    Script Date: 10/09/2015 08:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoGetTrailerDisponibles]
(
	@IDCLIENTE INT,
	@idSector int,
	@idEspecificacion varchar(400)
)
AS
BEGIN

SELECT distinct ac.idAcuerdoServicio,
				acc.idAcuerdoConcepto, 
				acc2.Costo as cant,
				ac.idTipoServicio,
				ts.Descripcion as TipoServicio,
				v.idVehiculo,
				c.Codigo + ' ' + vi.NroCodificacion as trailer,
				acc.idEspecificacion,
				et.Descripcion as Especificacion
				
FROM Acuerdo a
INNER JOIN AcuerdoServicio ac ON AC.idAcuerdo = A.idAcuerdo
inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = aC.idAcuerdoServicio and acc.Concepto not like '%Cant%'
inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like '%Cant%' 

inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.idEspecificaciones = acc2.idEspecificacion
inner join EspecificacionDetalle ed on ed.idEspecificacion = et.idEspecificaciones
inner join Vehiculo v on v.idVehiculo = ed.idEquipoPropio
inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
inner join Codificacion c on c.idCodificacion = vi.idCodificacion 

LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = v.idVehiculo
left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
									AND SC.Baja = 0 
                                    and  SC.Entrega = 0
									--and sc.nuevomovimiento = 1
									and UT.FECHA = SC.FechaAlta
									and sc.idTipoServicio = UT.idtipoServicio
WHERE a.baja = 0 
and a.nroRevision = (select MAX(nroRevision) as nroRevision
					from Acuerdo where baja = 0 and idCliente = @IDCLIENTE)
and a.idCliente = @IDCLIENTE
and a.idSector = @idSector
and acc.Concepto like '%Mens%'
and acc2.Concepto like '%Mens%'
and (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')))
and (sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null))

END
