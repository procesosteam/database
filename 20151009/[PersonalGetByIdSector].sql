USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdSector]    Script Date: 10/09/2015 13:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PersonalGetByIdSector]
(
	@idSector int
)
AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[NroLegajo],
		p.[Apellido] + ', ' + p.[Nombres] as apellido,
		p.[Nombres],
		p.[FechaNacimiento],
		p.[idSexo],
		p.[DNI],
		p.[CUIL],
		p.[idEstadoCivil],
		p.[idEstudios],
		p.[idNacionalidad],
		p.[idLocalidad],
		p.[Domicilio],
		p.[Telefono],
		p.[Mail],
		p.[idUsuarioAcceso],
		p.[idPersonalContacto],
		p.[idPersonalDatosLaborales],
		p.[idPersonalInformacionMedica],
		p.[idPersonalConduccion]
	FROM [Personal] p
	inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	inner join PersonalInformacionMedica pim on pim.idPersonalInformacionMedica = p.idPersonalInformacionMedica
	WHERE
		(pl.[idSector] = @idSector or @idSector is null)
		and pc.VtoCarnet >= GETDATE()
		

END
