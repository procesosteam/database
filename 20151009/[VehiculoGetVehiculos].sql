USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetVehiculos]    Script Date: 10/09/2015 09:30:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VehiculoGetVehiculos]
AS
BEGIN

	SELECT
		v.[idVehiculo],
		v.[idMarca],
		v.[Modelo],
		v.[Anio],
		v.[Patente],
		v.[idEmpresa],
		v.[idSector],
		v.[fechaAltaEmpresa],
		v.[fechaBajaEmpresa],
		v.[idUsuario],
		v.[fechaAlta],
		v.[Titular],
		v.[PotenciaMotor],
		v.[Color],
		v.[NroMotor],
		v.[NroChasis],
		v.[alta],
		v.[baja] ,
		v.esEquipo,
		c.Codigo + ' ' + vi.NroCodificacion as Codificacion
	FROM [Vehiculo] v
	INNER JOIN VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	WHERE
		v.[esEquipo] = 0
		and v.baja = 0
	ORDER BY c.Codigo, vi.NroCodificacion
END
