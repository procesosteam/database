

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleGetById];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetById]
(
	@idOrdenDetalle int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrdenDetalle] = @idOrdenDetalle)

END
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetByFilter]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleGetByFilter];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetByFilter]
(
	@idOrdenDetalle int,
	@idOrden int,
	@idPersonal int,
	@idEquipoCliente int,
	@idTipoServicio int,
	@idEquipoPropio int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrdenDetalle] = @idOrdenDetalle OR @idOrdenDetalle IS NULL) AND
		([idOrden] = @idOrden OR @idOrden IS NULL) AND
		([idPersonal] = @idPersonal OR @idPersonal IS NULL) AND
		([idEquipoCliente] = @idEquipoCliente OR @idEquipoCliente IS NULL) AND
		([idTipoServicio] = @idTipoServicio OR @idTipoServicio IS NULL) AND
		([idEquipoPropio] = @idEquipoPropio OR @idEquipoPropio IS NULL)

END
GO


IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleGetAll];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetAll]
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
END
GO


IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleUpdate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleUpdate];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleUpdate]
(
	@idOrdenDetalle int,
	@idOrden int,
	@idPersonal int,
	@idEquipoCliente int,
	@idTipoServicio int,
	@idEquipoPropio int
)
AS
BEGIN

	UPDATE [OrdenTrabajoDetalle]
	SET
		[idOrden] = @idOrden,
		[idPersonal] = @idPersonal,
		[idEquipoCliente] = @idEquipoCliente,
		[idTipoServicio] = @idTipoServicio,
		[idEquipoPropio] = @idEquipoPropio
	WHERE
		[idOrdenDetalle] = @idOrdenDetalle




END
GO


IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleAdd]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleAdd];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleAdd]
(
	@idOrden int,
	@idPersonal int = NULL,
	@idEquipoCliente int = NULL,
	@idTipoServicio int = NULL,
	@idEquipoPropio int = NULL
)
AS
BEGIN


	INSERT
	INTO [OrdenTrabajoDetalle]
	(
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	)
	VALUES
	(
		@idOrden,
		@idPersonal,
		@idEquipoCliente,
		@idTipoServicio,
		@idEquipoPropio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
GO


IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleDelete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
    DROP PROCEDURE [OrdenTrabajoDetalleDelete];
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleDelete]
(
	@idOrdenDetalle int
)
AS
BEGIN

	DELETE
	FROM [OrdenTrabajoDetalle]
	WHERE
		[idOrdenDetalle] = @idOrdenDetalle
END
GO

