USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetById]    Script Date: 11/03/2015 14:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].PersonalGetByIdOrden
(
	@idOrden int
)
AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[Apellido] +', ' + p.[Nombres] as apellido
	FROM [Personal] p
	INNER JOIN OrdenTrabajoDetalle otd on otd.idPersonal = p.idPersonal
	WHERE
		(otd.idOrden = @idOrden)

END
