USE SIG
GO

/****** Object:  Trigger [tg_AditoriaSS]    Script Date: 10/28/2015 10:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tg_AditoriaSS]'))
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[tg_AditoriaSS]
   ON  [dbo].[SolicitudCliente]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @IDUSER INT
    DECLARE @IDSS INT
    DECLARE @BAJA BIT = 0
    DECLARE @BAJASTR VARCHAR(100) = ''''
    DECLARE @Action as char(1)
    
    SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                         AND EXISTS(SELECT * FROM DELETED)
                        THEN ''U''  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN ''I''  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN ''D''  -- Set Action to Deleted.
                        ELSE NULL -- Skip. It may have been a "failed delete".   
                    END)
    
	SELECT @IDUSER = idUsuario, @IDSS = IDsolicitud , @BAJA = BAJA
	FROM INSERTED
	
	IF @Action = ''U''
	BEGIN
		IF @BAJA = 1
			BEGIN SET @BAJASTR = '' DELETE '' END
		ELSE
			BEGIN SET @BAJASTR = '' UPDATE '' END
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,''SOLICITUD CLIENTE''
			   ,@BAJASTR + '' IDSS: '' + STR(LTRIM(RTRIM(@IDSS))) 
			   ,GETDATE())
			   
	END
	IF @Action = ''I''
	BEGIN
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,''SOLICITUD CLIENTE''
			   ,''INSERT IDSS: '' + STR(LTRIM(RTRIM(@IDSS)))
			   ,GETDATE())
     END
      
    UPDATE SolicitudCliente 
	SET nropedidocliente = CASE WHEN nropedidocliente IS NULL OR (SELECT COUNT(*) FROM dbo.ArrayToTable(NROPEDIDOCLIENTE,''-''))<3 THEN
							isnull(nropedidocliente, str(nrosolicitud)) + '' - '' + dbo.ABC(Nrosolicitud, idSolicitud)
							ELSE
							NROPEDIDOCLIENTE
							END
	WHERE idsolicitud = @IDSS
	
END

'
GO

/****** Object:  Trigger [tg_NumeroRemito]    Script Date: 10/28/2015 10:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tg_NumeroRemito]'))
EXEC dbo.sp_executesql @statement = N'
CREATE TRIGGER [dbo].[tg_NumeroRemito]
   ON  [dbo].[Remitos]
   FOR INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @NRO INT
	SELECT @NRO = NROREMITO FROM INSERTED	
	
	IF (@NRO > 0 )
	BEGIN
    UPDATE REMITONUMERO
    SET NROREMITO = NROREMITO + 1
	END
END
'
GO
/****** Object:  Trigger [tg_AditoriaRemito]    Script Date: 10/28/2015 10:09:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tg_AditoriaRemito]'))
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[tg_AditoriaRemito]
   ON  [dbo].[Remitos]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @IDUSER INT
    DECLARE @IDRem INT
    DECLARE @Action as char(1)
    
    SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                         AND EXISTS(SELECT * FROM DELETED)
                        THEN ''U''  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN ''I''  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN ''D''  -- Set Action to Deleted.
                        ELSE NULL -- Skip. It may have been a "failed delete".   
                    END)
    
	SELECT @IDUSER = idUsuario, @IDRem = idRemito FROM INSERTED
	
	IF @Action = ''U''
	BEGIN
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,''REMITOS''
			   ,''UPDATE - IDREMITO: '' + STR(LTRIM(RTRIM(@IDRem)))
			   ,GETDATE())
	END
	IF @Action = ''I''
	BEGIN
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,''REMITO''
			   ,''INSERT - IDREMITO: '' + STR(LTRIM(RTRIM(@IDRem)))
			   ,GETDATE())
     END
END

'
GO
