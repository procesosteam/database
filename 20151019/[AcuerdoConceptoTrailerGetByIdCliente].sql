USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdCliente]    Script Date: 10/19/2015 12:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdCliente]
(
	@IDCLIENTE INT,
	@idSector int, 
	@idEspecificacion varchar(400)
)

AS

BEGIN

	SELECT DISTINCT AT.idAcuerdoConcepto,
			AT.idTrailer as idVehiculo,
			c.Codigo + ' '+ vi.NroCodificacion AS TRAILER,
			ac.idTipoServicio,
			acc.idEspecificacion,
			ts.Descripcion as TipoServicio,
			et.Descripcion as Especificacion,
			acc2.Costo as cant
			
	FROM Acuerdo a	
	INNER JOIN AcuerdoServicio ac ON AC.idAcuerdo = A.idAcuerdo
	inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = aC.idAcuerdoServicio and acc.Concepto not like '%Cant%'
	inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like '%Cant%' 
	INNER JOIN AcuerdoConceptoTrailer at on at.idAcuerdoConcepto = aCc.idAcuerdoConcepto
	
	inner join Vehiculo v on v.idVehiculo = at.idTrailer
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
	inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.idEspecificaciones = acc2.idEspecificacion
	left join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo
	LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = At.idTrailer 
	left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud AND SC.Baja = 0 
                                                    and  SC.Entrega = 0													
													and UT.FECHA = SC.FechaAlta
	WHERE a.idCliente = @IDCLIENTE
		and a.baja = 0
		and a.nroRevision = (select MAX(nroRevision) as nroRevision
							from Acuerdo where baja = 0 and idCliente = @IDCLIENTE and a.idSector = @idSector)
		and (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')) )

END
