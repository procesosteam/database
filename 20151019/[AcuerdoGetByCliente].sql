USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByCliente]    Script Date: 11/03/2015 18:20:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoGetByCliente]
(
	@IDSECTOR INT,
	@IDCLIENTE INT	
)
AS
BEGIN

SELECT *
FROM v_Acuerdo
WHERE idCliente = @IDCLIENTE
and idSector = @IDSECTOR

END
