USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByFilter]    Script Date: 11/06/2015 12:26:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoGetByFilter]
	(
		@nroAcuerdo varchar(150),
		@fechaDesde datetime,
		@fechaHasta datetime,
		@idCliente int
	)
	
AS
BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		ISNULL(A.NROREVISION, 1) AS NROREVISION,
		A.TITULO,
		S.Descripcion AS SECTOR
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN Sector S ON S.idSector = A.idSector
	WHERE A.BAJA = 0
	AND (A.IDCLIENTE = @IDCLIENTE OR @IDCLIENTE IS NULL)
	AND (A.FECHAINICIO >= @FECHADESDE OR @FECHADESDE IS NULL)
	AND (A.FECHAFIN >= @FECHAHASTA OR @FECHAHASTA IS NULL)
	AND ((A.NROACUERDO like + '%' + @NROACUERDO  + '%')  OR @NROACUERDO IS NULL)
	
	ORDER BY a.nroacuerdo , a.nrorevision desc, A.FECHAFIN DESC, A.FECHAINICIO DESC

END
