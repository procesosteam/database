USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetAll]    Script Date: 11/03/2015 14:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[KilometrosGetAll]
AS

BEGIN
	SELECT IDKMS,
			[idLugarDesde],
           [idLugarHasta],
           [Kms],
           [KmsEstablecidos]
	FROM KILOMETROS k
	LEFT JOIN Lugar lDesde on k.idLugarDesde = lDesde.idLugar
	LEFT JOIN Lugar lHas on k.idLugarHasta = lHas.idLugar
	WHERE isnull(lDesde.Baja,0) = 0
	AND	isnull(lHas.Baja,0) = 0
	
END
