USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetById]    Script Date: 11/03/2015 12:41:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetByIdOrden]
(
	@idOrden int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrden] = @idOrden)

END
