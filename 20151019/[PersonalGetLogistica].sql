USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetLogistica]    Script Date: 10/28/2015 11:29:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[PersonalGetLogistica]

AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[NroLegajo],
		p.[Apellido] + ', ' + p.[Nombres] as apellido,
		p.[Nombres],
		p.[FechaNacimiento],
		p.[idSexo],
		p.[DNI],
		p.[CUIL],
		p.[idEstadoCivil],
		p.[idEstudios],
		p.[idNacionalidad],
		p.[idLocalidad],
		p.[Domicilio],
		p.[Telefono],
		p.[Mail],
		p.[idUsuarioAcceso],
		p.[idPersonalContacto],
		p.[idPersonalDatosLaborales],
		p.[idPersonalInformacionMedica],
		p.[idPersonalConduccion]
	FROM [Personal] p
	inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	inner join PersonalInformacionMedica pim on pim.idPersonalInformacionMedica = p.idPersonalInformacionMedica
	Inner join Sector s on s.idSector = pl.idSector
	WHERE p.BAJA = 0 
		and pc.VtoCarnet >= GETDATE()
		and upper(s.Descripcion) like '%LOG%'
		and s.idEmpresa in(select idEmpresa from Empresa where interna=1)
	ORDER BY P.Apellido, P.Nombres

END
