USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[RPTOTI]    Script Date: 10/28/2015 09:16:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[RPTOTI]
(
--DECLARE
	@idOTI AS VARCHAR(250)
)
AS

BEGIN

SELECT DISTINCT 
         OrdenTrabajo.idOrden, 
         OrdenTrabajo.idSolicitudCliente, 
         SolicitudCliente.idMovimiento, 
         SolicitudCliente.NroPedidoCliente, 
         SolicitudCliente.NroSolicitud, 
         Cod.Codigo + ' ' + VI.NroCodificacion AS Transporte, 
         Cod2.Codigo + ' ' + VI2.NroCodificacion AS Trailer, 
         TipoServicio.Descripcion AS TipoServicio, 
         LugarOrigen.Descripcion AS LugarOrigen, 
         Solicitante.Apellido AS Solicitante, 
         Sector.Descripcion AS Sector, 
         LugarDestino.Descripcion AS LugarDestino, 
         Empresa.RazonSocial AS Empresa, 
         SolicitudCliente.Fecha, 
         Chofer.Apellido + ', ' + Chofer.Nombres AS Chofer, 
         Movimiento.pozoOrigen, Movimiento.pozoDestino, 
         Movimiento.KmsEstimados, 
         Acompanante.Apellido + ', ' + Acompanante.Nombres AS Acompanante,          
         SolicitudCliente.idTipoServicio, 
         sce.idEspecificacion, 
         et.Descripcion AS Especificacion, 
         OrdenTrabajoConcepto.Concepto, 
         OrdenTrabajoConcepto.Valor,
         c.Descripcion AS Condicion
FROM         OrdenTrabajo INNER JOIN
             SolicitudCliente ON OrdenTrabajo.idSolicitudCliente = SolicitudCliente.idSolicitud INNER JOIN
             Sector ON SolicitudCliente.idSector = Sector.idSector INNER JOIN              
             Empresa ON Empresa.idEmpresa = Sector.idEmpresa INNER JOIN
             TipoServicio ON SolicitudCliente.idTipoServicio = TipoServicio.idTipoServicio LEFT OUTER JOIN
             OrdenTrabajoConcepto ON OrdenTrabajo.idOrden = OrdenTrabajoConcepto.idOti LEFT OUTER JOIN
             Condicion AS c ON SolicitudCliente.idCondicion = c.idCondicion LEFT OUTER JOIN
             --VEHICULO
             Vehiculo AS v ON OrdenTrabajo.idTransporte = v.idVehiculo LEFT OUTER JOIN
             VehiculoIdentificacion AS VI ON VI.idVehiculo = v.idVehiculo LEFT OUTER JOIN
             Codificacion AS Cod ON Cod.idCodificacion = VI.idCodificacion LEFT OUTER JOIN
             --EQUIPO
             Vehiculo AS equipo ON OrdenTrabajo.idTrailer = equipo.idVehiculo LEFT OUTER JOIN
             VehiculoIdentificacion AS VI2 ON VI2.idVehiculo = equipo.idVehiculo LEFT OUTER JOIN
             Codificacion AS Cod2 ON Cod2.idCodificacion = VI2.idCodificacion LEFT OUTER JOIN
             
             Personal as chofer ON OrdenTrabajo.idChofer = Chofer.idPersonal LEFT OUTER JOIN
             Personal AS Acompanante ON OrdenTrabajo.idAcompanante = Acompanante.idPersonal LEFT OUTER JOIN
             
             Movimiento ON SolicitudCliente.idMovimiento = Movimiento.idMovimiento LEFT OUTER JOIN
             Lugar AS LugarOrigen ON Movimiento.idLugarOrigen = LugarOrigen.idLugar LEFT OUTER JOIN
             Lugar AS LugarDestino ON Movimiento.idLugarDestino = LugarDestino.idLugar LEFT OUTER JOIN
             Solicitante ON Solicitante.idSolicitante = SolicitudCliente.idSolicitante INNER JOIN
             
             Acuerdo A ON A.idCliente = Empresa.idEmpresa AND A.idSector = SolicitudCliente.idSector INNER JOIN
             AcuerdoServicio AS ase ON A.idAcuerdo = ase.idAcuerdo AND ase.idTipoServicio = SolicitudCliente.idTipoServicio LEFT OUTER JOIN
             AcuerdoConceptoCosto AS acc ON ase.idAcuerdoServicio = acc.idAcuerdoServicio LEFT OUTER JOIN
             
             SolicitudClienteEspecificacion sce on sce.idSolicitudCliente = SolicitudCliente.idSolicitud left join
             EspecificacionDetalle AS ed ON ed.idEquipoPropio = v.idVehiculo and sce.idEspecificacion = ed.idEspecificacion LEFT OUTER JOIN
             EspecificacionTecnica AS et ON et.idEspecificaciones = sce.idEspecificacion 
             
WHERE        (SolicitudCliente.Baja = 0) 
AND (OrdenTrabajo.BAJA = 0)
AND (OrdenTrabajo.idOrden IN
     (SELECT Fld
       FROM  dbo.ArrayToTable(@idOTI, ',') AS ArrayToTable_1)) 
 
                               
END
