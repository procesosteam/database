USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteAdd]    Script Date: 10/28/2015 17:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SolicitudClienteAdd]
(
	@Fecha smalldatetime = NULL,
	@FechaAlta smalldatetime = NULL,
	@idUsuario int = NULL,
	@idTipoServicio int = NULL,
	@idMovimiento int = NULL,
	@idSolicitante int = NULL,	
	@NroPedidoCliente nvarchar(50) = NULL,
	@NroSolicitud int = NULL,
	@Entrega bit = NULL,
	@idSector int = NULL,
	@ServicioAux nvarchar(500) = NULL,
	@FechaEstimada DATETIME = NULL,
	@nuevoMov bit = NULL,
	@idCondicion int = NULL,
	@idSSOrig int
)
AS
BEGIN

/*
IF @nuevoMov = 0 
BEGIN
	DECLARE @NRO_NUEVO  INT--, @NRO_ANTE INT

	SELECT @NRO_NUEVO = ISNULL(Numero,0) 
	FROM SolicitudClienteNumero
	
	SELECT @NroSolicitud = MAX(nrosolicitud)+1
	FROM solicitudcliente
	
	IF @NRO_NUEVO > @NroSolicitud
	BEGIN
		SET @NroSolicitud = @NroSolicitud
		UPDATE SolicitudClienteNumero
		SET NUMERO = @NroSolicitud
	END
	
END
*/

DECLARE @IDSC INT
set @IDSC = 0

SELECT @IDSC = idSolicitud
FROM SolicitudCliente
WHERE Baja = 0
AND isnull(YEAR(Fecha),0) = isnull(YEAR(@Fecha),0)
AND isnull(MONTH(Fecha),0) = isnull(MONTH(@Fecha),0)
AND isnull(DAY(Fecha),0) = isnull(DAY(@Fecha),0)
and isnull(idUsuario,0) = isnull(@idUsuario,0)
AND isnull(idTipoServicio,0) = isnull(@idTipoServicio,0)
AND isnull(idMovimiento,0) = isnull(@idMovimiento,0)
AND isnull(idSolicitante,0) = isnull(@idSolicitante,0)
AND isnull(NroSolicitud,0) = isnull(@NroSolicitud,0)
AND isnull(Entrega,0) = isnull(@Entrega,0)
AND isnull(idSector,0) = isnull(@idSector ,0)
AND isnull(ServicioAux,'') = isnull(@ServicioAux ,'')
AND isnull(NuevoMovimiento,0) = isnull(@nuevoMov,0)
AND isnull(idCondicion,0) = isnull(@idCondicion,0)

IF ( ISNULL(@IDSC,0) = 0)
BEGIN 
	INSERT
	INTO [SolicitudCliente]
	(
		[Fecha],
		[FechaAlta],
		[idUsuario],
		[idTipoServicio],
		[idMovimiento],
		[idSolicitante],
		[NroPedidoCliente],
		[NroSolicitud],
		[Entrega],
		[idSector],
		[ServicioAux],
		[fechaEstimada],
		[NuevoMovimiento],
		[idCondicion],
		[idSolicitudOrig]
	)
	VALUES
	(
		@Fecha,
		@FechaAlta,
		@idUsuario,
		@idTipoServicio,
		@idMovimiento,
		@idSolicitante,		
		@NroPedidoCliente,
		@NroSolicitud,
		@Entrega,
		@idSector,
		@ServicioAux,
		@FechaEstimada,
		@nuevoMov,
		@idCondicion,
		@idSSOrig
	)

	SELECT @IDSC = CAST(SCOPE_IDENTITY() as INT)
	
	
	IF ((SELECT COUNT(1) FROM SolicitudCliente sc
		WHERE NroSolicitud = @NroSolicitud
		--and (idMovimiento != null or idMovimiento!=0)
		)> 0)
	BEGIN
		
		UPDATE sc
		SET sc.idMovimiento = sc2.idMovimiento
		FROM SolicitudCliente sc
		inner join SolicitudCliente sc2 on	sc2.NroSolicitud = sc.nroSolicitud
											and sc2.idMovimiento != 0
											and sc2.Baja = 0	
											--
											and	right(sc.NroPedidoCliente,1) = right(sc2.NroPedidoCliente,1)			
		WHERE sc.NroSolicitud = @NroSolicitud
		and ((right(sc.NroPedidoCliente,1) = right(sc2.NroPedidoCliente,1)) or sc.NuevoMovimiento=0)
		and sc.idSolicitud in (select idSolicitudCliente from SolicitudClienteEspecificacion)
		
	END

END

SELECT @IDSC 

END
