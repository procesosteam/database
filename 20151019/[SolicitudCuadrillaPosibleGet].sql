USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGet]    Script Date: 11/13/2015 11:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolicitudCuadrillaPosibleGet]
(
	@nroSolicitud int,
	@idTipoServicio int
)

AS

BEGIN

	SELECT distinct c.idCuadrilla,
			c.Nombre, 
			c.FechaAlta			
	FROM Cuadrilla c
	INNER JOIN SolicitudTrailerPosible STP ON c.idCuadrilla = STP.idTrailer
	WHERE idSolicitud = @nroSolicitud
	AND idTipoServicio = @idTipoServicio

END
