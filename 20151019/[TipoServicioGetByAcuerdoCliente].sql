USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByAcuerdoCliente]    Script Date: 11/13/2015 08:45:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TipoServicioGetByAcuerdoCliente]
(
	@idCliente int,
	@idSector int
)

AS

BEGIN

	SELECT	distinct acs.idTipoServicio, 
			ts.Descripcion as TipoServicio,
			ts.Abreviacion,
			ts.RequierePersonal,
			ts.idSector,
			ts.RequiereTraslado,
			ts.RequiereCuadrilla,
			ts.RequiereEquipo,
			ts.RequiereKms,
			ts.RequiereKmsTaco
	FROM  Acuerdo a
	inner join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	inner join TipoServicio ts on ts.idTipoServicio= acs.idTipoServicio
	WHERE isnull(a.baja,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0);

	SELECT	distinct acs.idTipoServicio, 
			et.idEspecificaciones as idEspecificacion,
			et.Descripcion as EspecificacionTecnica,
			et.RequiereTraslado
			
	FROM    Acuerdo a
	--inner join (select MAX(isnull(nroRevision,0))as nroRev from acuerdo where idcliente=@idCliente) aa on aa.nroRev = a.nroRevision
	left join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	left join TipoServicioEspecificacion tse on tse.idTipoServicio = acs.idTipoServicio 
	left join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = acs.idAcuerdoServicio
	left join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.RequiereTraslado = 0 --and (et.RequiereCuadrilla = 1 and et.RequiereEquipo = 0)
	WHERE isnull(a.BAJA,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)
	and a.fechaInicio = (select MAX(fechaInicio)as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)		
	
		


END
