USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteGetByIdParte]    Script Date: 11/06/2015 13:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VerificaParteGetByIdParte]
	@idParte int
AS
BEGIN

	SELECT	VP.idParte,
			VP.idVerifica,
			VP.Cantidad,
			V.descripcion
	FROM VerificaParte VP
	INNER JOIN Verifica V ON V.idVerifica = VP.idVerifica
	WHERE idParte = @idParte

END
