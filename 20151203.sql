USE [SIG]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByCliente]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetByCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetByCliente]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RemitosAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosAdd]
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosGetById]
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetUltimaFecha]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetUltimaFecha]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosGetUltimaFecha]
GO
/****** Object:  StoredProcedure [dbo].[RemitosUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosUpdate]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteAdd]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteDelete]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteGetAll]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteGetById]
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitanteUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RemitosDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosDelete]
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosGetAll]
GO
/****** Object:  StoredProcedure [dbo].[TrailerAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerAdd]
GO
/****** Object:  StoredProcedure [dbo].[TrailerDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerDelete]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerGetAll]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerGetById]
GO
/****** Object:  StoredProcedure [dbo].[TrailerUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerUpdate]
GO
/****** Object:  StoredProcedure [dbo].[TransporteAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransporteAdd]
GO
/****** Object:  StoredProcedure [dbo].[TransporteDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransporteDelete]
GO
/****** Object:  StoredProcedure [dbo].[TransporteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransporteGetAll]
GO
/****** Object:  StoredProcedure [dbo].[TransporteGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransporteGetById]
GO
/****** Object:  StoredProcedure [dbo].[TransporteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransporteUpdate]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioEspecificacionAdd]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionGetByIdTipoServicio]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionGetByIdTipoServicio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioEspecificacionGetByIdTipoServicio]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioEspecificacionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioGetAll]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByAcuerdoCliente]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetByAcuerdoCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioGetByAcuerdoCliente]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioGetById]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioUpdate]
GO
/****** Object:  StoredProcedure [dbo].[UsuarioAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsuarioAdd]
GO
/****** Object:  StoredProcedure [dbo].[UsuarioDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsuarioDelete]
GO
/****** Object:  StoredProcedure [dbo].[UsuarioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsuarioGetAll]
GO
/****** Object:  StoredProcedure [dbo].[UsuarioGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsuarioGetById]
GO
/****** Object:  StoredProcedure [dbo].[UsuarioUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UsuarioUpdate]
GO
/****** Object:  View [dbo].[v_Acuerdo]    Script Date: 12/03/2015 17:51:46 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_Acuerdo]'))
DROP VIEW [dbo].[v_Acuerdo]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesAdd]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesDelete]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesGetById]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetByIdVehiculo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesGetByIdVehiculo]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoFilesUpdate]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGestionGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGestionGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGestionGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGetById]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetByIdSector]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetByIdSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGetByIdSector]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetEquipos]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetEquipos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGetEquipos]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetVehiculos]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetVehiculos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoGetVehiculos]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoHistorialGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoHistorialGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoHistorialGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionAdd]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionDelete]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetAllBaja]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionGetById]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetByIdVehiculo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionGetByIdVehiculo]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoIdentificacionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroAdd]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroDelete]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroGetById]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetByIdVehiculo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroGetByIdVehiculo]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoSeguroUpdate]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoAdd]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoDelete]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoAdd]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoDelete]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoGetById]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetByIdVehiculo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoGetByIdVehiculo]
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehiculoVencimientoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[TipoUsuarioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoUsuarioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoUsuarioGetAll]
GO
/****** Object:  StoredProcedure [dbo].[TipoUsuariosGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoUsuariosGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoUsuariosGetById]
GO
/****** Object:  StoredProcedure [dbo].[VerificaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VerificaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VerificaParteAdd]
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VerificaParteGetById]
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteGetByIdParte]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteGetByIdParte]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VerificaParteGetByIdParte]
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VerificaParteUpdate]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCuadrillaPosibleGet]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudCuadrillaPosibleGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudCuadrillaPosibleGet]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudEspecificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudEspecificacionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudEspecificacionAdd]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudEspecificacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudEspecificacionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudEspecificacionDelete]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioAdd]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TipoServicioDelete]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleAdd]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleDelete]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGet]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleGet]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleGetAll]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleGetById]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudTrailerPosibleUpdate]
GO
/****** Object:  StoredProcedure [dbo].[SectorAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorAdd]
GO
/****** Object:  StoredProcedure [dbo].[SectorDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorDelete]
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorGetAll]
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAllByIdEmpresa]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAllByIdEmpresa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorGetAllByIdEmpresa]
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAllByIdEmpresaInterna]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAllByIdEmpresaInterna]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorGetAllByIdEmpresaInterna]
GO
/****** Object:  StoredProcedure [dbo].[SectorGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorGetById]
GO
/****** Object:  StoredProcedure [dbo].[SectorUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SectorUpdate]
GO
/****** Object:  StoredProcedure [dbo].[SexoAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SexoAdd]
GO
/****** Object:  StoredProcedure [dbo].[SexoDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SexoDelete]
GO
/****** Object:  StoredProcedure [dbo].[SexoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SexoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[SexoGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SexoGetById]
GO
/****** Object:  StoredProcedure [dbo].[SexoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SexoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[RemitoGetRPT]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoGetRPT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoGetRPT]
GO
/****** Object:  StoredProcedure [dbo].[RemitosBonificadosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosBonificadosGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosBonificadosGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[ReporteAlertaMovimiento]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReporteAlertaMovimiento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReporteAlertaMovimiento]
GO
/****** Object:  StoredProcedure [dbo].[ReporteMovimiento]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReporteMovimiento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReporteMovimiento]
GO
/****** Object:  StoredProcedure [dbo].[ReportePosicionActual]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportePosicionActual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportePosicionActual]
GO
/****** Object:  StoredProcedure [dbo].[RPTOTI]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPTOTI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPTOTI]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteEliminadas]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteEliminadas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteEliminadas]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudHistorialGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudHistorialGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudHistorialGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudRegresoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudRegresoGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudRegresoGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[PersonalAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalConduccionAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalConduccionDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalConduccionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalConduccionGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalConduccionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalContactoAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalContactoDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalContactoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalContactoGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalContactoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetByIdPersonal]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetByIdPersonal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesGetByIdPersonal]
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalFilesUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGestionGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGestionGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGestionGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdOrden]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdOrden]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetByIdOrden]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdSector]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdSector]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetByIdSector]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdSectorEmpresa]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdSectorEmpresa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetByIdSectorEmpresa]
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetLogistica]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetLogistica]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalGetLogistica]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDatosLaboralesAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDatosLaboralesDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDatosLaboralesGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDatosLaboralesGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDatosLaboralesUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PersonalDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalInformacionMedicaAdd]
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalInformacionMedicaDelete]
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalInformacionMedicaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalInformacionMedicaGetById]
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalInformacionMedicaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PersonalToXLS]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalToXLS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalToXLS]
GO
/****** Object:  StoredProcedure [dbo].[PersonalUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonalUpdate]
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProvinciaAdd]
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProvinciaDelete]
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProvinciaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProvinciaGetById]
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProvinciaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RelacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionAdd]
GO
/****** Object:  StoredProcedure [dbo].[RelacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionDelete]
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetAllBaja]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionGetById]
GO
/****** Object:  StoredProcedure [dbo].[RelacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RelacionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleAdd]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleAddGetByRemito]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleAddGetByRemito]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleAddGetByRemito]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleDelete]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleGetAll]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleGetById]
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoDetalleUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RemitoGetNroRemito]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoGetNroRemito]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoGetNroRemito]
GO
/****** Object:  StoredProcedure [dbo].[RemitoNumeroUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoNumeroUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitoNumeroUpdate]
GO
/****** Object:  StoredProcedure [dbo].[RemitosAnuladosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosAnuladosGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RemitosAnuladosGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[BancoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BancoAdd]
GO
/****** Object:  StoredProcedure [dbo].[BancoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BancoDelete]
GO
/****** Object:  StoredProcedure [dbo].[BancoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BancoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[BancoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BancoGetById]
GO
/****** Object:  StoredProcedure [dbo].[BancoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[BancoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionAdd]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionDelete]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionGetById]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetEquipo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetEquipo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionGetEquipo]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetVehiculo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetVehiculo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionGetVehiculo]
GO
/****** Object:  StoredProcedure [dbo].[CodificacionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodificacionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[CondicionAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CondicionAdd]
GO
/****** Object:  StoredProcedure [dbo].[CondicionDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CondicionDelete]
GO
/****** Object:  StoredProcedure [dbo].[CondicionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CondicionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[CondicionGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CondicionGetById]
GO
/****** Object:  StoredProcedure [dbo].[CondicionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CondicionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaAdd]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaDelete]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGestionGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaGestionGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaGetById]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalAdd]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalDelete]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalDeleteByIdCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalDeleteByIdCuadrilla]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalDeleteByIdCuadrilla]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalGetAll]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalGetById]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetByIdCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetByIdCuadrilla]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalGetByIdCuadrilla]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaPersonalUpdate]
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CuadrillaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[ChoferAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferAdd]
GO
/****** Object:  StoredProcedure [dbo].[ChoferDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferDelete]
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferGetById]
GO
/****** Object:  StoredProcedure [dbo].[ChoferUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ChoferUpdate]
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DepartamentoAdd]
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DepartamentoDelete]
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DepartamentoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DepartamentoGetById]
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DepartamentoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaAdd]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaDelete]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaGetById]
GO
/****** Object:  StoredProcedure [dbo].[DiagramaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DiagramaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioAdd]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioDelete]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioGetAll]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetByIdAcuerdo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoServicioGetByIdAcuerdo]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoHerderGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoHerderGetByIdAcuerdo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoHerderGetByIdAcuerdo]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloAdd]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloDelete]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloGetAll]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetByIdAcuerdo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloGetByIdAcuerdo]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoTituloUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoVerificarFechas]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoVerificarFechas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoVerificarFechas]
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CategoriaCarnetAdd]
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CategoriaCarnetDelete]
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CategoriaCarnetGetAll]
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CategoriaCarnetGetById]
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CategoriaCarnetUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerAdd]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerDelete]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerGetAll]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerUpdate]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoDelete]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDetalleGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoDetalleGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoDetalleGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGestionGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGestionGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetAnteriorById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetAnteriorById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetAnteriorById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoByConEsp]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoByConEsp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoByConEsp]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoAdd]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoAlerta]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoAlerta]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoAlerta]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoAdd]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoDelete]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoGetById]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]
GO
/****** Object:  StoredProcedure [dbo].[EquiposAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposAdd]
GO
/****** Object:  StoredProcedure [dbo].[EquiposDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposDelete]
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposGetById]
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetByIdCliente]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetByIdCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposGetByIdCliente]
GO
/****** Object:  StoredProcedure [dbo].[EquiposUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquiposUpdate]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleAdd]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleDelete]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleDeleteByIdEsp]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleDeleteByIdEsp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleDeleteByIdEsp]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleGetById]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetByIdEspecificacion]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetByIdEspecificacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleGetByIdEspecificacion]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionDetalleUpdate]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaAdd]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaDelete]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetById]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdSolicitud]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByIdSolicitud]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetByIdSolicitud]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdTipoServicio]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByIdTipoServicio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetByIdTipoServicio]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetCuadrilla]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetCuadrilla]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetEquipo]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetEquipo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaGetEquipo]
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EspecificacionTecnicaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaAdd]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaDelete]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetAllInternas]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetAllInternas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaGetAllInternas]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaGetById]
GO
/****** Object:  StoredProcedure [dbo].[EmpresaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EmpresaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[EquipoGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquipoGestionGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EquipoGestionGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstadoCivilAdd]
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstadoCivilDelete]
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstadoCivilGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstadoCivilGetById]
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstadoCivilUpdate]
GO
/****** Object:  StoredProcedure [dbo].[EstudiosAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstudiosAdd]
GO
/****** Object:  StoredProcedure [dbo].[EstudiosDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstudiosDelete]
GO
/****** Object:  StoredProcedure [dbo].[EstudiosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstudiosGetAll]
GO
/****** Object:  StoredProcedure [dbo].[EstudiosGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstudiosGetById]
GO
/****** Object:  StoredProcedure [dbo].[EstudiosUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EstudiosUpdate]
GO
/****** Object:  StoredProcedure [dbo].[FuncionAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionAdd]
GO
/****** Object:  StoredProcedure [dbo].[FuncionDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionDelete]
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionGetById]
GO
/****** Object:  StoredProcedure [dbo].[FuncionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[FuncionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KilometrosAdd]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KilometrosDelete]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KilometrosGetAll]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetBy]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetBy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KilometrosGetBy]
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[KilometrosGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadAdd]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadDelete]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadGetAll]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadGetById]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetByIdProvincia]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetByIdProvincia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadGetByIdProvincia]
GO
/****** Object:  StoredProcedure [dbo].[LocalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LocalidadUpdate]
GO
/****** Object:  StoredProcedure [dbo].[LugarAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LugarAdd]
GO
/****** Object:  StoredProcedure [dbo].[LugarDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LugarDelete]
GO
/****** Object:  StoredProcedure [dbo].[LugarGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LugarGetAll]
GO
/****** Object:  StoredProcedure [dbo].[LugarGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LugarGetById]
GO
/****** Object:  StoredProcedure [dbo].[LugarUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LugarUpdate]
GO
/****** Object:  StoredProcedure [dbo].[MarcaAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MarcaAdd]
GO
/****** Object:  StoredProcedure [dbo].[MarcaDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MarcaDelete]
GO
/****** Object:  StoredProcedure [dbo].[MarcaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MarcaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[MarcaGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MarcaGetById]
GO
/****** Object:  StoredProcedure [dbo].[MarcaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MarcaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[ModulosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModulosGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModulosGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ModulosGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModulosGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModulosGetById]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadAdd]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadDelete]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetAllBaja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadGetAllBaja]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadGetById]
GO
/****** Object:  StoredProcedure [dbo].[ModalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ModalidadUpdate]
GO
/****** Object:  StoredProcedure [dbo].[MovimientoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MovimientoAdd]
GO
/****** Object:  StoredProcedure [dbo].[MovimientoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MovimientoDelete]
GO
/****** Object:  StoredProcedure [dbo].[MovimientoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MovimientoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[MovimientoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MovimientoGetById]
GO
/****** Object:  StoredProcedure [dbo].[MovimientoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MovimientoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NacionalidadAdd]
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NacionalidadDelete]
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NacionalidadGetAll]
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NacionalidadGetById]
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[NacionalidadUpdate]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoAdd]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoAdd]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoAdd]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoDelete]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetById]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoGetById]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetByIdOrden]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetByIdOrden]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoGetByIdOrden]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoConceptoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDelete]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDelete]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleAdd]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleDelete]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleGetAll]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleGetById]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetByIdOrden]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetByIdOrden]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleGetByIdOrden]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoDetalleUpdate]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoGetAll]
GO
/****** Object:  StoredProcedure [dbo].[getIdFilesByIdPersonal]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getIdFilesByIdPersonal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getIdFilesByIdPersonal]
GO
/****** Object:  StoredProcedure [dbo].[GetPersonalFilesByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPersonalFilesByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPersonalFilesByFilter]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoGetById]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PaisAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaisAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PaisAdd]
GO
/****** Object:  StoredProcedure [dbo].[PaisGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaisGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PaisGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParteEntregaAdd]
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParteEntregaDelete]
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParteEntregaGetAll]
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParteEntregaGetById]
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ParteEntregaUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionAdd]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionDelete]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionGetById]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetByLegajo]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetByLegajo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionGetByLegajo]
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisoConduccionUpdate]
GO
/****** Object:  StoredProcedure [dbo].[PermisosAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisosAdd]
GO
/****** Object:  StoredProcedure [dbo].[PermisosDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisosDelete]
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisosGetAll]
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisosGetById]
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetIdTipoUsuario]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetIdTipoUsuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PermisosGetIdTipoUsuario]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoRegresoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoRegresoGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoRegresoGetByFilter]
GO
/****** Object:  View [dbo].[v_SolicitudCliente]    Script Date: 12/03/2015 17:51:46 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_SolicitudCliente]'))
DROP VIEW [dbo].[v_SolicitudCliente]
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetByIdSolicitud]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetByIdSolicitud]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OrdenTrabajoGetByIdSolicitud]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteSinOTI]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteSinOTI]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteSinOTI]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetNoDisponibles]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetNoDisponibles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerGetNoDisponibles]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetSolicitudAnterior]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetSolicitudAnterior]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerGetSolicitudAnterior]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDiff]    Script Date: 12/03/2015 17:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDiff]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetDiff]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdCliente]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetByIdCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdCliente]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetTrailerDisponibles]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetTrailerDisponibles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AcuerdoGetTrailerDisponibles]
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetDisponibles]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetDisponibles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TrailerGetDisponibles]
GO
/****** Object:  UserDefinedFunction [dbo].[UltimaOtiTrailer]    Script Date: 12/03/2015 17:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UltimaOtiTrailer]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UltimaOtiTrailer]
GO
/****** Object:  StoredProcedure [dbo].[CodTangoGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodTangoGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CodTangoGetByFilter]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudServicioGetUltimoNro]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudServicioGetUltimoNro]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudServicioGetUltimoNro]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteAdd]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteAdd]
GO
/****** Object:  StoredProcedure [dbo].[AuditoriaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditoriaGetByFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AuditoriaGetByFilter]
GO
/****** Object:  View [dbo].[v_Auditoria]    Script Date: 12/03/2015 17:51:46 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_Auditoria]'))
DROP VIEW [dbo].[v_Auditoria]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteGetAll]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetById]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteGetById]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetByNro]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetByNro]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteGetByNro]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetFechaRegreso]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetFechaRegreso]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteGetFechaRegreso]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteUpdate]
GO
/****** Object:  UserDefinedFunction [dbo].[FechaRegreso]    Script Date: 12/03/2015 17:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FechaRegreso]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FechaRegreso]
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteDelete]    Script Date: 12/03/2015 17:51:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SolicitudClienteDelete]
GO
/****** Object:  UserDefinedFunction [dbo].[ABC]    Script Date: 12/03/2015 17:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ABC]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ABC]
GO
/****** Object:  UserDefinedFunction [dbo].[ArrayToTable]    Script Date: 12/03/2015 17:51:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArrayToTable]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ArrayToTable]
GO
/****** Object:  UserDefinedFunction [dbo].[ArrayToTable]    Script Date: 12/03/2015 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArrayToTable]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[ArrayToTable] 
( 
	@array VarChar(4000), 
	@delimiter VarChar(6) 
)

/*
Name: ArrayToTable
Return Values: TABLE ( ( VarChar ) Fld )
Description: Receives a delimited list of values and returns them as
a single column table of varchar values
*/

RETURNS @tblReturn TABLE (
Fld NVarChar( 500 )
)
AS

BEGIN

-- track where in the text we are...
DECLARE @startPos Int
DECLARE @endPos Int

-- each element that we find in the text
DECLARE @elementValue VarChar(500)

SET @array = @array + @delimiter
SET @startPos = 0
SET @endPos = CHARINDEX(@delimiter, @array, @startPos)


-- loop until we reach the end of the text
WHILE ( (@endPos > 0) And ( @startPos < LEN(@array) ) )
BEGIN

SET @elementValue = CONVERT( VarChar(500), LTRIM( RTRIM( SUBSTRING( @array, @startPos, @endPos - @startPos ))))

-- did we find a valid element?
IF( LEN(@elementValue) > 0 )
INSERT INTO @tblReturn ([Fld]) VALUES (@elementValue)

-- move the pointers
SET @startPos = @endPos + LEN( @delimiter )
SET @endPos = CHARINDEX( @delimiter, @array, @startPos )

END

RETURN 

END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[ABC]    Script Date: 12/03/2015 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ABC]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[ABC]
(	
	@NroSolicitud INT,
	@idSolicitud int
)

RETURNS VARCHAR(2)

AS
BEGIN

DECLARE @ABC VARCHAR(55)
SET @ABC = ''A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z''

DECLARE @tblReturn TABLE (
	LETRA NVarChar(1),
	ROWID INT
)

INSERT INTO @tblReturn
	SELECT Fld AS LETRA, ROW_NUMBER()OVER(ORDER BY Fld) as RowId
	FROM DBO.ArrayToTable(@ABC,'','')	
	
DECLARE @ResultVar VARCHAR(2),
@idTipoServicio int

SELECT @idTipoServicio = idTipoServicio
from SolicitudCliente
where idSolicitud = @idSolicitud

SELECT @ResultVar = LETRA
FROM (SELECT NROSOLICITUD,
		CASE WHEN ENTREGA = 0 THEN 26 ELSE 
			CASE WHEN ENTREGA = 1 AND NUEVOMOVIMIENTO = 0 THEN 1 ELSE
			ROW_NUMBER()OVER(ORDER BY fecha) END END AS ROWID, 
		idsolicitud
		FROM SolicitudCliente
		WHERE NroSolicitud = @NroSolicitud
		and idTipoServicio = @idTipoServicio
		and baja = 0
		) SC
INNER JOIN @tblReturn A ON A.ROWID = SC.ROWID
where sc.idSolicitud = @idSolicitud
RETURN @ResultVar

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteDelete]
(
	@idSolicitud int
)
AS
BEGIN

	DELETE
	FROM [SolicitudCliente]
	WHERE
		[idSolicitud] = @idSolicitud
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[FechaRegreso]    Script Date: 12/03/2015 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FechaRegreso]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[FechaRegreso]
(
	@NroSolicitud INT,
	@idSolicitud int
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar datetime
	SET @ResultVar = NULL
	
	SELECT TOP 1  @ResultVar = FECHA
	FROM (
		SELECT	NROSOLICITUD, FECHA,
				idsolicitud,
				ROW_NUMBER()OVER(ORDER BY fecha) AS ROWID
		FROM SolicitudCliente
		WHERE NroSolicitud = @NroSolicitud
		and baja = 0
	) A
	WHERE @idSolicitud > idSolicitud
	ORDER BY ROWID DESC

	IF(@ResultVar IS NULL )
		BEGIN SET @ResultVar = GETDATE() END
		
	RETURN @ResultVar

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteUpdate]
(
	@idSolicitud int,
	@Fecha smalldatetime,
	@Hora smalldatetime,
	@FechaAlta smalldatetime,
	@idUsuario int,
	@idTipoServicio int,
	@idEspecificaciones int,
	@idMovimiento int,
	@idSolicitante int,
	@idCondicion int,
	@NroPedidoCliente nvarchar(50),
	@NroSolicitud int,
	@Baja bit,
	@Entrega bit,
	@idSector int,
	@ServicioAux nvarchar(500),
	@FechaEstimada datetime,
	@nuevoMov bit,
	@idSSOrig int
)
AS
BEGIN

	UPDATE [SolicitudCliente]
	SET
		[Fecha] = @Fecha,
		[Hora] = @Hora,
		[FechaAlta] = @FechaAlta,
		[idUsuario] = @idUsuario,
		[idTipoServicio] = @idTipoServicio,
		[idEspecificaciones] = @idEspecificaciones,
		[idMovimiento] = @idMovimiento,
		[idSolicitante] = @idSolicitante,
		[idCondicion] = @idCondicion,
		[NroPedidoCliente] = @NroPedidoCliente,
		[NroSolicitud] = @NroSolicitud,
		[Baja] = @Baja,
		[Entrega] = @Entrega,
		[idSector] = @idSector,
		[ServicioAux] = @ServicioAux,
		[fechaEstimada]= @FechaEstimada,
		[NuevoMovimiento] = @nuevoMov,
		[idSolicitudOrig] = @idSSOrig
	WHERE
		[idSolicitud] = @idSolicitud


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetFechaRegreso]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetFechaRegreso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteGetFechaRegreso]
(
	@NroSolicitud INT,
	@idSolicitud INT,
	@Fecha datetime
)
AS

BEGIN

	 SELECT CASE WHEN (SELECT COUNT(*) FROM SolicitudCliente
					   WHERE NroSolicitud = @NroSolicitud) > 2 THEN					   
				CASE WHEN DBO.ABC(@NroSolicitud, @idSolicitud) = ''B'' THEN
				(datediff(D, dbo.FechaRegreso(@NroSolicitud, @idSolicitud),@Fecha) + 1)
				ELSE datediff(D, dbo.FechaRegreso(@NroSolicitud, @idSolicitud),@Fecha) END
			ELSE
				CASE WHEN DBO.ABC(@NroSolicitud, @idSolicitud) = ''Z'' THEN
				(datediff(D, dbo.FechaRegreso(@NroSolicitud, @idSolicitud),@Fecha) + 1)
				ELSE datediff(D, dbo.FechaRegreso(@NroSolicitud, @idSolicitud),@Fecha) END
			END AS [CantDias]

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetByNro]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetByNro]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteGetByNro]
(
	@NroSolicitud int
)
AS
BEGIN

	SELECT
		[idSolicitud],
		[Fecha],
		[Hora],
		[FechaAlta],
		[idUsuario],
		isnull([idTipoServicio],0) as idTipoServicio,
		isnull([idEspecificaciones],0) as idEspecificaciones,
		isnull([idMovimiento],0) as idMovimiento,
		isnull([idSolicitante],0) as idSolicitante,
		ISNULL(idSector,0) as idSector,
		isnull([idCondicion],0) as idCondicion,
		[NroPedidoCliente],
		[NroSolicitud],
		[Baja],
		Entrega,
		rtrim(ltrim([ServicioAux])) as ServicioAux,
		ISNULL(FechaEstimada,''01/01/1900'') as FechaEstimada,
		ISNULL(NuevoMovimiento,0) as NuevoMovimiento,
		DBO.ABC(NroSolicitud, idSolicitud) as Letra,
		convert(varchar,dbo.FechaRegreso(NroSolicitud, idSolicitud),103) as FechaRegreso
	FROM [SolicitudCliente]
	WHERE
		([NroSolicitud] = @NroSolicitud)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteGetById]
(
	@idSolicitud int
)
AS
BEGIN

	SELECT
		[idSolicitud],
		[Fecha],
		[Hora],
		[FechaAlta],
		[idUsuario],
		isnull([idTipoServicio],0) as idTipoServicio,
		isnull([idEspecificaciones],0) as idEspecificaciones,
		isnull([idMovimiento],0) as idMovimiento,
		isnull([idSolicitante],0) as idSolicitante,
		ISNULL(idSector,0) as idSector,
		isnull([idCondicion],0) as idCondicion,
		[NroPedidoCliente],
		[NroSolicitud],
		[Baja],
		Entrega,
		rtrim(ltrim([ServicioAux])) as ServicioAux,
		ISNULL(FechaEstimada,''01/01/1900'') as FechaEstimada,
		ISNULL(NuevoMovimiento,0) as NuevoMovimiento,
		DBO.ABC(NroSolicitud, idSolicitud) as Letra,
		convert(varchar,dbo.FechaRegreso(NroSolicitud, idSolicitud),103) as FechaRegreso,
		ISNULL(idSolicitudOrig,0) as idSolicitudOrig
	FROM [SolicitudCliente]
	WHERE
		([idSolicitud] = @idSolicitud)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteGetAll]
AS
BEGIN

	SELECT
		[idSolicitud],
		[Fecha],
		[Hora],
		[FechaAlta],
		[idUsuario],
		[idTipoServicio],
		[idEspecificaciones],
		[idMovimiento],
		[idSolicitante],
		[idCondicion],
		[NroPedidoCliente],
		[NroSolicitud],
		[baja],
		ISNULL([idsector],0) as idSector,
		ISNULL(FechaEstimada,''01/01/1900'') as FechaEstimada,
		ISNULL(NuevoMovimiento,0) as NuevoMovimiento,
		DBO.ABC(NroSolicitud, idSolicitud) as letra,
		convert(varchar,dbo.FechaRegreso(NroSolicitud, idSolicitud),103) as FechaRegreso
	FROM [SolicitudCliente]
END
' 
END
GO
/****** Object:  View [dbo].[v_Auditoria]    Script Date: 12/03/2015 17:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_Auditoria]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[v_Auditoria]
AS
SELECT     a.Tabla, a.Accion, a.Fecha AS fechaA, sc.NroSolicitud, sc.Fecha AS FechaSC, u.Apellido + '', '' + u.Nombre AS Usuario, sc.idSolicitud, sc.Baja
FROM         dbo.Auditoria AS a INNER JOIN
                      dbo.Usuario AS u ON u.idUsuario = a.idUsuario INNER JOIN
                      dbo.SolicitudCliente AS sc ON a.Accion LIKE ''%'' + STR(sc.idSolicitud) + ''%''
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'v_Auditoria', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 6
               Left = 274
               Bottom = 126
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sc"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 193
               Right = 708
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Auditoria'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'v_Auditoria', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Auditoria'
GO
/****** Object:  StoredProcedure [dbo].[AuditoriaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditoriaGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AuditoriaGetByFilter] 
(
@NROSOLICITUD INT,
@FECHADESDE DATETIME,
@FECHAHASTA DATETIME
)
AS

BEGIN

DECLARE @NRO INT,
@FECHAD DATETIME,
@FECHAH DATETIME

SET @NRO = @NROSOLICITUD
SET @FECHAD = @FECHAD
SET @FECHAH = @FECHAHASTA

select
TABLA,
NROSOLICITUD,
dbo.ABC(NROSOLICITUD, idSolicitud) as LETRA,
convert(varchar(10),FECHASC,103) AS [FECHA SOLICITUD],
case when baja = 1 then ''SI'' else ''NO'' end as BAJA,
CASE WHEN ACCION LIKE ''%INSERT%'' THEN ''NUEVO''
WHEN ACCION LIKE ''%UPDATE%'' THEN ''MODIFICO''
ELSE ACCION
END AS ACCION,
FECHAA AS [FECHA AUDITORIA],
USUARIO
from v_auditoria
where (NroSolicitud = @NRO OR @NRO IS NULL)
AND (FechaA >= @FECHADESDE OR @FECHADESDE IS NULL)
AND (FechaA <= @FECHAHASTA OR @FECHAHASTA IS NULL)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteAdd]
(
	@Fecha smalldatetime = NULL,
	@FechaAlta smalldatetime = NULL,
	@idUsuario int = NULL,
	@idTipoServicio int = NULL,
	@idMovimiento int = NULL,
	@idSolicitante int = NULL,	
	@NroPedidoCliente nvarchar(50) = NULL,
	@NroSolicitud int = NULL,
	@Entrega bit = NULL,
	@idSector int = NULL,
	@ServicioAux nvarchar(500) = NULL,
	@FechaEstimada DATETIME = NULL,
	@nuevoMov bit = NULL,
	@idCondicion int = NULL,
	@idSSOrig int
)
AS
BEGIN

/*
IF @nuevoMov = 0 
BEGIN
	DECLARE @NRO_NUEVO  INT--, @NRO_ANTE INT

	SELECT @NRO_NUEVO = ISNULL(Numero,0) 
	FROM SolicitudClienteNumero
	
	SELECT @NroSolicitud = MAX(nrosolicitud)+1
	FROM solicitudcliente
	
	IF @NRO_NUEVO > @NroSolicitud
	BEGIN
		SET @NroSolicitud = @NroSolicitud
		UPDATE SolicitudClienteNumero
		SET NUMERO = @NroSolicitud
	END
	
END
*/

DECLARE @IDSC INT
set @IDSC = 0

SELECT @IDSC = idSolicitud
FROM SolicitudCliente
WHERE Baja = 0
AND isnull(YEAR(Fecha),0) = isnull(YEAR(@Fecha),0)
AND isnull(MONTH(Fecha),0) = isnull(MONTH(@Fecha),0)
AND isnull(DAY(Fecha),0) = isnull(DAY(@Fecha),0)
and isnull(idUsuario,0) = isnull(@idUsuario,0)
AND isnull(idTipoServicio,0) = isnull(@idTipoServicio,0)
AND isnull(idMovimiento,0) = isnull(@idMovimiento,0)
AND isnull(idSolicitante,0) = isnull(@idSolicitante,0)
AND isnull(NroSolicitud,0) = isnull(@NroSolicitud,0)
AND isnull(Entrega,0) = isnull(@Entrega,0)
AND isnull(idSector,0) = isnull(@idSector ,0)
AND isnull(ServicioAux,'''') = isnull(@ServicioAux ,'''')
AND isnull(NuevoMovimiento,0) = isnull(@nuevoMov,0)
AND isnull(idCondicion,0) = isnull(@idCondicion,0)

IF ( ISNULL(@IDSC,0) = 0)
BEGIN 
	INSERT
	INTO [SolicitudCliente]
	(
		[Fecha],
		[FechaAlta],
		[idUsuario],
		[idTipoServicio],
		[idMovimiento],
		[idSolicitante],
		[NroPedidoCliente],
		[NroSolicitud],
		[Entrega],
		[idSector],
		[ServicioAux],
		[fechaEstimada],
		[NuevoMovimiento],
		[idCondicion],
		[idSolicitudOrig]
	)
	VALUES
	(
		@Fecha,
		@FechaAlta,
		@idUsuario,
		@idTipoServicio,
		@idMovimiento,
		@idSolicitante,		
		@NroPedidoCliente,
		@NroSolicitud,
		@Entrega,
		@idSector,
		@ServicioAux,
		@FechaEstimada,
		@nuevoMov,
		@idCondicion,
		@idSSOrig
	)

	SELECT @IDSC = CAST(SCOPE_IDENTITY() as INT)
	
	
	IF ((SELECT COUNT(1) FROM SolicitudCliente sc
		WHERE NroSolicitud = @NroSolicitud
		--and (idMovimiento != null or idMovimiento!=0)
		)> 0)
	BEGIN
		
		UPDATE sc
		SET sc.idMovimiento = sc2.idMovimiento
		FROM SolicitudCliente sc
		inner join SolicitudCliente sc2 on	sc2.NroSolicitud = sc.nroSolicitud
											and sc2.idMovimiento != 0
											and sc2.Baja = 0	
											--
											and	right(sc.NroPedidoCliente,1) = right(sc2.NroPedidoCliente,1)			
		WHERE sc.NroSolicitud = @NroSolicitud
		and ((right(sc.NroPedidoCliente,1) = right(sc2.NroPedidoCliente,1)) or sc.NuevoMovimiento=0)
		and sc.idSolicitud in (select idSolicitudCliente from SolicitudClienteEspecificacion)
		
	END

END

SELECT @IDSC 

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudServicioGetUltimoNro]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudServicioGetUltimoNro]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudServicioGetUltimoNro]
AS


BEGIN 

	DECLARE @NRO_NUEVO  INT, @NRO_ANTE INT

	SELECT @NRO_NUEVO = ISNULL(Numero,0) 
	FROM SolicitudClienteNumero
	
	SELECT @NRO_ANTE = MAX(nrosolicitud)
	FROM solicitudcliente
	
	IF (@NRO_NUEVO = @NRO_ANTE +3)
	BEGIN
		UPDATE SolicitudClienteNumero
		SET NUMERO = @NRO_ANTE + 1
	END
	ELSE
	BEGIN	
		UPDATE SolicitudClienteNumero
		SET NUMERO = NUMERO + 1
	END
	
	SELECT NUMERO AS NroSolicitud
	FROM SolicitudClienteNumero
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodTangoGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodTangoGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodTangoGetByFilter] 
@DESCRIP VARCHAR (100)

AS
BEGIN


	SELECT cod_articu, descripcio + '' '' + desc_adic as descripcio
	FROM [V-SRVTANGO1\SQLEXPRESS_AXOFT].[PROCESOS_PATAGONICOS__SRL].DBO.[sta11]
	WHERE descripcio LIKE ''%''+@DESCRIP+''%'' or @DESCRIP is null
	
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[UltimaOtiTrailer]    Script Date: 12/03/2015 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UltimaOtiTrailer]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[UltimaOtiTrailer]
(
)
RETURNS 
@tableOTI TABLE 
(
	FECHA DATETIME,
	nroSolicitud int,
	IDSolicitud int,
	idTrailer int,
	idtipoServicio int
)
AS
BEGIN

	INSERT INTO @tableOTI
	
	/*
	SELECT TOP 1 SC.FECHAALTA AS FECHA,SC.NROSOLICITUD, OT.IDSOLICITUDCLIENTE, OT.IDTRAILER, sc.idtipoServicio
	FROM OrdenTrabajo ot 
	INNER JOIN SOLICITUDCLIENTE SC ON  sc.idSolicitud = ot.idSolicitudCliente										
	where sc.Baja = 0 and ot.BAJA = 0	
	ORDER BY OT.FECHAALTA DESC
	*/
	SELECT SC.FECHAALTA AS FECHA,SC.NROSOLICITUD, OT.IDSOLICITUDCLIENTE, OT.IDTRAILER, sc.idtipoServicio
	FROM OrdenTrabajo ot 
	INNER JOIN SOLICITUDCLIENTE SC on sc.idSolicitud = ot.idSolicitudCliente 
	WHERE ot.idOrden = (SELECT top 1 OTi.idOrden
						FROM OrdenTrabajo oti 
						WHERE BAJA = 0 
						and oti.idTrailer = ot.idTrailer
						ORDER BY FECHAALTA DESC)
	ORDER BY OT.FECHAALTA DESC
	--AND ot.idSolicitudCliente in (SELECT MAX(idSolicitud)
	--							  FROM SolicitudCliente 
	--							  WHERE baja = 0
	--								and NroSolicitud = SC.NroSolicitud								
	--								group by NroSolicitud)

	--group by OT.idsolicitudCLIENTE, SC.FechaAlta, SC.NroSolicitud,ot.IDTRAILER, sc.idtiposervicio
	
	/*(
		SELECT MAX(SC.FECHA)AS FECHA, MAX(NROSOLICITUD)AS NROSOLICITUD,OT.IDTRAILER
		FROM OrdenTrabajo ot 
		INNER JOIN SolicitudCliente sc on sc.idSolicitud = ot.idSolicitudCliente
		WHERE OT.BAJA = 0 
		AND sc.baja = 0
		AND  ot.idtrailer is not null				
		GROUP BY OT.IDTRAILER
	) U
	INNER JOIN SOLICITUDCLIENTE SC ON SC.NROSOLICITUD = U.nroSolicitud AND
										SC.FECHA = U.FECHA AND SC.BAJA = 0*/
	
	RETURN 
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetDisponibles]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetDisponibles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerGetDisponibles]
(
	@idEspecificacion VARCHAR(400)
)
AS

--DECLARE @idEspecificacion VARCHAR(400) = ''18,19''


BEGIN

	SELECT DISTINCT  v.idVehiculo,
		v.trailer,
		v.idEspecificacion, 
		SC.NROSOLICITUD, 
		SC.ENTREGA,
		ET.Descripcion AS Especificacion,
		ts.idTipoServicio,
		TS.Descripcion as TipoServicio,
		1 as cant
	FROM 	
	(--VEHICULO
	(select v.idVehiculo,
	c.Codigo + '' '' + vi.NroCodificacion as trailer, ed.idEspecificacion
	from Vehiculo v
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	left join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo) 
	union 
	(--CUADRILLA
	select c.idCuadrilla, c.Nombre, ed.idEspecificacion
	from Cuadrilla c
	left join EspecificacionDetalle ed on ed.idCuadrilla = c.idCuadrilla) 
	)v
	
    LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = v.idVehiculo
	LEFT JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
										AND SC.Baja = 0 
                                        and  SC.Entrega = 0
										--and sc.nuevomovimiento = 1
										and UT.FECHA = SC.FechaAlta
										and sc.idTipoServicio = UT.idtipoServicio

	inner JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = v.idEspecificacion
	left JOIN TipoServicioEspecificacion TSE ON TSE.idEspecificacion = ET.idEspecificaciones
	inner JOIN TipoServicio TS ON TS.idTipoServicio = TSE.idTipoServicio
	WHERE  (v.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,'','')) 
		                 OR @idEspecificacion IS NULL)
	and (sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null))
	
	order by trailer
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetTrailerDisponibles]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetTrailerDisponibles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoGetTrailerDisponibles]
(
	@IDCLIENTE INT,
	@idSector int,
	@idEspecificacion varchar(400)
)
AS
BEGIN

SELECT distinct ac.idAcuerdoServicio,
				acc.idAcuerdoConcepto, 
				acc2.Costo as cant,
				ac.idTipoServicio,
				ts.Descripcion as TipoServicio,
				v.idVehiculo,
				c.Codigo + '' '' + vi.NroCodificacion as trailer,
				acc.idEspecificacion,
				et.Descripcion as Especificacion
				
FROM Acuerdo a
INNER JOIN AcuerdoServicio ac ON AC.idAcuerdo = A.idAcuerdo
inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = aC.idAcuerdoServicio and acc.Concepto not like ''%Cant%''
inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like ''%Cant%'' 

inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.idEspecificaciones = acc2.idEspecificacion
inner join EspecificacionDetalle ed on ed.idEspecificacion = et.idEspecificaciones
inner join Vehiculo v on v.idVehiculo = ed.idEquipoPropio
inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
inner join Codificacion c on c.idCodificacion = vi.idCodificacion 

LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = v.idVehiculo
left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
									AND SC.Baja = 0 
                                    and  SC.Entrega = 0
									--and sc.nuevomovimiento = 1
									and UT.FECHA = SC.FechaAlta
									and sc.idTipoServicio = UT.idtipoServicio
WHERE a.baja = 0 
and a.nroRevision = (select MAX(nroRevision) as nroRevision
					from Acuerdo where baja = 0 and idCliente = @IDCLIENTE)
and a.idCliente = @IDCLIENTE
and a.idSector = @idSector
and acc.Concepto like ''%Mens%''
and acc2.Concepto like ''%Mens%''
and (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,'','')))
and (sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null))

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdCliente]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetByIdCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdCliente]
(
	@IDCLIENTE INT,
	@idSector int, 
	@idEspecificacion varchar(400)
)

AS

BEGIN

	SELECT DISTINCT AT.idAcuerdoConcepto,
			AT.idTrailer as idVehiculo,
			c.Codigo + '' ''+ vi.NroCodificacion AS TRAILER,
			ac.idTipoServicio,
			acc.idEspecificacion,
			ts.Descripcion as TipoServicio,
			et.Descripcion as Especificacion,
			acc2.Costo as cant
			
	FROM Acuerdo a	
	INNER JOIN AcuerdoServicio ac ON AC.idAcuerdo = A.idAcuerdo
	inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = aC.idAcuerdoServicio and acc.Concepto not like ''%Cant%''
	inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like ''%Cant%'' 
	INNER JOIN AcuerdoConceptoTrailer at on at.idAcuerdoConcepto = aCc.idAcuerdoConcepto
	
	inner join Vehiculo v on v.idVehiculo = at.idTrailer
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
	inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.idEspecificaciones = acc2.idEspecificacion
	left join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo
	LEFT JOIN (select * from dbo.UltimaOtiTrailer()) UT ON UT.idTrailer = At.idTrailer 
	left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud AND SC.Baja = 0 
                                                    and  SC.Entrega = 0													
													and UT.FECHA = SC.FechaAlta
	WHERE a.idCliente = @IDCLIENTE
		and a.baja = 0
		and a.nroRevision = (select MAX(nroRevision) as nroRevision
							from Acuerdo where baja = 0 and idCliente = @IDCLIENTE and a.idSector = @idSector)
		and (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,'','')) )

END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetDiff]    Script Date: 12/03/2015 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDiff]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[GetDiff]
(
	@VAR1 VARCHAR(MAX),
	@VAR2 VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

DECLARE @C INT
DECLARE @AUXDIF VARCHAR(MAX)

SET @C = 0
/*SET @VAR1 = ''titulo 5''
SET @VAR2 = ''TITULO 5''*/
SET @AUXDIF = ''''

IF @VAR1 <> @VAR2
BEGIN
	WHILE substring(@VAR1, @c, 1) = substring(@VAR2, @c, 1) --AND @C = LEN(@VAR1)
	BEGIN
	  SET @c = @c + 1      
	  --PRINT STR(@C) + '' - '' +substring(@VAR1, @c, 1) + ''-'' + substring(@VAR2, @c, 1)
	END;

	SET @AUXDIF = substring(@VAR2, @c, LEN(@VAR2));
	
END
 
IF LEN(@AUXDIF) > 0
BEGIN SET @AUXDIF = substring(@VAR2, 0, @c) + '' '' + @AUXDIF + '' '' END
ELSE
BEGIN SET @AUXDIF = '''' END


/***************************************

SET @VAR1 = REVERSE(@VAR1)
SET @VAR2 = REVERSE(@VAR2)
SET @C = 0
WHILE substring(@VAR1, @c, 1) = substring(@VAR2, @c, 1) --AND @C = LEN(@VAR1)
BEGIN
  SET @c = @c + 1      
  --PRINT STR(@C) + '' - '' +substring(@VAR1, @c, 1) + ''-'' + substring(@VAR2, @c, 1)
END;

PRINT '' <SPAN> '' + REVERSE(substring(@VAR2, @c, LEN(@VAR2)))+ '' </SPAN> '' + REVERSE(substring(@VAR2, 0, @C))


*/
	RETURN @AUXDIF
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetSolicitudAnterior]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetSolicitudAnterior]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerGetSolicitudAnterior]
(
	@NROSOLICITUD INT
)

AS

BEGIN
	
	DECLARE @IDTRAILER INT 
	SET @IDTRAILER = 0
	
	SELECT	@IDTRAILER = OT.idTrailer
	FROM SolicitudCliente SC
	INNER JOIN OrdenTrabajo OT ON OT.idSolicitudCliente = SC.idSolicitud
	WHERE	NroSolicitud = @NROSOLICITUD
			AND SC.Entrega = 1

SELECT	ISNULL(@IDTRAILER,0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetNoDisponibles]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetNoDisponibles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerGetNoDisponibles]
(
	@idEspecificacion VARCHAR(400) ,
	@NROSOLICITUD INT
)
AS
BEGIN

--DECLARE @idEspecificacion VARCHAR(400) ,
--@NROSOLICITUD INT
--SET @idEspecificacion = ''32''
--SET @NROSOLICITUD = NULL

CREATE TABLE #DISP
(
	IDTRAILER INT,
	DESCRIPCION VARCHAR(150),
	IDESPECIFICACION INT, 
	NROSOLICITUD INT, 
	ENTREGA BIT,
	ESPECIFICACION VARCHAR(150),
	IDTIPOSERVICIO INT,
	TIPOSERVICIO VARCHAR(150),
	cant int
)

INSERT INTO #DISP
EXEC [TrailerGetDisponibles]@idEspecificacion
	
SELECT DISTINCT v.idVehiculo, 
				c.Codigo + '' '' + vi.NroCodificacion as trailer, 
				ed.IDESPECIFICACION,
				ET.Descripcion AS Especificacion,
				ts.idTipoServicio,
				TS.Descripcion as TipoServicio,
				1 AS CANT
FROM Vehiculo v 
inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
inner join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo
LEFT JOIN (SELECT DISTINCT OT.IDTRAILER, SC.NROSOLICITUD FROM ORDENTRABAJO OT
			INNER JOIN SOLICITUDCLIENTE SC ON SC.IDSOLICITUD = OT.IDSOLICITUDCLIENTE
			) OT ON OT.IDTRAILER = v.idVehiculo
inner JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = ed.idEspecificacion
inner JOIN TipoServicioEspecificacion TSE ON TSE.idEspecificacion = ET.idEspecificaciones
inner JOIN TipoServicio TS ON TS.idTipoServicio = TSE.idTipoServicio
WHERE vi.idVehiculo NOT IN (SELECT IDTRAILER FROM #DISP)
AND (ed.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,'','')) 
		                 OR @idEspecificacion IS NULL)
AND (OT.NROSOLICITUD = @NROSOLICITUD OR @NROSOLICITUD IS NULL)

DROP TABLE #DISP

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteSinOTI]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteSinOTI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteSinOTI]

AS
BEGIN

SELECT sc.idSolicitud, 
       isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NroSolicitud],
       convert(varchar,sc.Fecha,103) as Fecha, 
		e.Descripcion as [Empresa],
		sec.Descripcion as [Sector],
		ts.Descripcion as [TipoServicio],
		lor.Descripcion as [Origen],
		m.pozoOrigen as [PozoOrigen],			
		lde.Descripcion as [Destino],
		m.pozoDestino as[PozoDestino],
		u.Apellido + '', ''+ u.nombre as Usuario

FROM SolicitudCliente sc
left join Sector sec on (sec.idSector = SC.idSector)					
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
left join Movimiento m on m.idMovimiento = SC.idMovimiento
left join Lugar lDe on lDe.idLugar = m.idLugarDestino
left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
left join usuario u on u.idUsuario = sc.idUsuario
WHERE sc.idSolicitud NOT IN(
		SELECT idSolicitudCliente
		FROM OrdenTrabajo
		WHERE Baja = 0)
AND SC.Baja = 0
ORDER BY SC.FECHA DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetByIdSolicitud]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetByIdSolicitud]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoGetByIdSolicitud]
(

	@idSolicitud int
)
AS
BEGIN

	SELECT
		ot.[idOrden],
		ot.[idTransporte],
		ot.[idEquipo],
		ot.[Horario],
		ot.[Salida],
		ot.[Llegada],
		ot.[KmsRecorridos],
		ot.[idSolicitudCliente],
		ot.[Controlo],
		ot.[Verifico],
		ot.[idTrailer],
		ot.[idChofer],
		ot.[idAcompanante]
	FROM [OrdenTrabajo] ot
	inner join SolicitudCliente sc on sc.idSolicitud = ot.idSolicitudCliente
	inner join dbo.UltimaOtiTrailer() oti on oti.idTrailer = ot.idTrailer and sc.idSolicitud = oti.IDSolicitud
	where
		(oti.NroSolicitud in (select distinct nroSolicitud 
							from  SolicitudCliente
							where idSolicitud = @idSolicitud))

END
' 
END
GO
/****** Object:  View [dbo].[v_SolicitudCliente]    Script Date: 12/03/2015 17:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_SolicitudCliente]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[v_SolicitudCliente]
AS

SELECT  SC.idSolicitud,
		SC.NroSolicitud,
		SC.Fecha, 
		TS.idTipoServicio,
		TS.Descripcion AS TIPOSERVICIO, 
		ET.idEspecificaciones,
		ET.Descripcion AS ESPECIFICACION, 
		M.idLugarOrigen, 
		Ld.Descripcion AS LugarDestino,
		M.pozoDestino,
		LO.Descripcion AS LugarOrigen,
		M.pozoOrigen,
		M.KmsEstimados, 
		S.idSolicitante,
		S.Nombre AS SOLICITANTE, 
        Sec.Descripcion AS SECTOR, 
        E.idEmpresa,
        E.Descripcion AS EMPRESA, 
        S.Telefono, 
        C.idCondicion,
        C.Descripcion AS CONDICION, 
        SC.NroPedidoCliente,
        OT.idOrden,
        SC.ENTREGA  as EntregaSolicitud,
        OT.Entrega as EntregaOT,
        PE.Entrega AS EntregaPE
FROM    dbo.SolicitudCliente AS SC INNER JOIN
		  dbo.TipoServicio AS TS ON TS.idTipoServicio = SC.idTipoServicio LEFT JOIN
		  dbo.EspecificacionTecnica AS ET ON ET.idEspecificaciones = SC.idEspecificaciones INNER JOIN
		  dbo.Movimiento AS M ON M.idMovimiento = SC.idMovimiento INNER JOIN
		  dbo.Solicitante AS S ON S.idSolicitante = SC.idSolicitante INNER JOIN
		  dbo.Sector AS Sec ON Sec.idSector = S.idSector INNER JOIN
		  dbo.Usuario AS U ON SC.idUsuario = U.idUsuario INNER JOIN
		  dbo.Condicion AS C ON C.idCondicion = SC.idCondicion INNER JOIN
		  dbo.Empresa AS E ON E.idEmpresa = S.idEmpresa LEFT JOIN
		  dbo.OrdenTrabajo AS OT ON OT.idSolicitudCliente = SC.idSolicitud and OT.Baja = 0
		  LEFT JOIN dbo.ParteEntrega PE ON OT.idOrden = PE.idOrdenTrabajo
		INNER JOIN Lugar Ld ON Ld.idLugar = M.idLugarDestino
		INNER JOIN Lugar LO ON LO.idLugar = M.idLugarOrigen
		

WHERE SC.BAJA = 0
'
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoRegresoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoRegresoGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE  PROCEDURE [dbo].[OrdenTrabajoRegresoGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime
)
AS
BEGIN

SELECT	OT.idOrden,
		OT.idTrailer, 
		c.Codigo + '' '' + vi.NroCodificacion,
		SC.NroSolicitud,
		SC.TIPOSERVICIO,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		SC.EMPRESA,
		SC.SOLICITANTE,
		SC.idSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega
FROM  OrdenTrabajo OT
INNER JOIN v_SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
LEFT JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 

WHERE OT.Baja = 0
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (sc.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)
		AND SC.EntregaSolicitud = 0
		--AND SC.NroSolicitud not in (SELECT NroSolicitud 
		--							FROM SolicitudCliente 
		--							WHERE ENTREGA = 1
		--							and Baja = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetIdTipoUsuario]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetIdTipoUsuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisosGetIdTipoUsuario]
(
	@idTipoPermiso int
)
AS
BEGIN

	SELECT	P.idPermiso,
			P.idModulo,
			P.idTipoUsuario,
			P.Escritura,
			P.Lectura
	FROM Permisos P
	WHERE idTipoUsuario = @idTipoPermiso
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisosGetById]
(
	@idPermiso int
)
AS

BEGIN

	SELECT P.idPermiso,
			P.idModulo,
			P.idTipoUsuario,
			P.Lectura,
			P.Escritura
	FROM Permisos P
	WHERE idPermiso = @idPermiso
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisosGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisosGetAll]
AS

BEGIN

	SELECT P.idPermiso,
			P.idModulo,
			P.idTipoUsuario,
			P.Lectura,
			P.Escritura
	FROM Permisos P
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisosDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisosDelete]
(
	@IDPERMISO INT
)
AS

BEGIN

	DELETE FROM Permisos
	WHERE idPermiso = @IDPERMISO

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisosAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisosAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisosAdd]
(
	@idModulo int,
	@idTipoUsuario int,
	@Lectura bit,
	@Escritura bit
)
AS

BEGIN

	INSERT INTO Permisos
	(
		idModulo,
		idTipoUsuario,
		Lectura,
		Escritura
	)
	VALUES
	(
		@idModulo,
		@idTipoUsuario,
		@Lectura,
		@Escritura
	)
	
	SELECT CAST(SCOPE_IDENTITY() as INT)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionUpdate]
(
	@idPermiso int,
	@Descripcion varchar(100),
	@idPersonalACargo int,
	@idPersonal int,
	@Fecha smalldatetime,
	@FechaOriginal smalldatetime
)
AS
BEGIN

	UPDATE [PermisoConduccion]
	SET
		[Descripcion] = @Descripcion,
		[idPersonalACargo] = @idPersonalACargo,
		[idPersonal] = @idPersonal,
		[Fecha] = @Fecha,
		[FechaOriginal] = @FechaOriginal
	WHERE
		[idPermiso] = @idPermiso




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetByLegajo]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetByLegajo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionGetByLegajo]
(
	@idLegajo int
)
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
		
	FROM [PermisoConduccion]
	WHERE
		[idPersonal] = @idLegajo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionGetById]
(
	@idPermiso int
)
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	FROM [PermisoConduccion]
	WHERE
		([idPermiso] = @idPermiso)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionGetAll]
AS
BEGIN

	SELECT
		[idPermiso],
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	FROM [PermisoConduccion]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionDelete]
(
	@idPermiso int
)
AS
BEGIN

	DELETE
	FROM [PermisoConduccion]
	WHERE
		[idPermiso] = @idPermiso
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PermisoConduccionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PermisoConduccionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PermisoConduccionAdd]
(
	@Descripcion varchar(100) = NULL,
	@idPersonalACargo int = NULL,
	@idPersonal int = NULL,
	@Fecha smalldatetime = NULL,
	@FechaOriginal smalldatetime = NULL
)
AS
BEGIN


	INSERT
	INTO [PermisoConduccion]
	(
		[Descripcion],
		[idPersonalACargo],
		[idPersonal],
		[Fecha],
		[FechaOriginal]
	)
	VALUES
	(
		@Descripcion,
		@idPersonalACargo,
		@idPersonal,
		@Fecha,
		@FechaOriginal
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ParteEntregaUpdate]
(
	@idParteEntrega int,
	@Fecha smalldatetime,
	@horaSalida datetime,
	@horaLlegada datetime,
	@idOrdenTrabajo int,
	@entrega bit,
	@nroParte varchar(50),
	@adicionales varchar(150),
	@conexion bit,
	@desconexion bit,
	@observacion varchar(550),
	@nroRemito varchar(100)
)
AS
BEGIN

	UPDATE [ParteEntrega]
	SET
		[Fecha] = @Fecha,
		[horasalida] = @horaSalida,
		[HoraLlegada] = @horaLlegada,
		[idOrdenTrabajo] = @idOrdenTrabajo,
		[Entrega] = @entrega,
		[NroParte] = @nroParte,
		Adicionales = @adicionales,
		Conexion = @conexion,
		Desconexion = @desconexion,
		[observacion] = @observacion,
		[nroRemito] = @nroRemito
	WHERE
		[idParteEntrega] = @idParteEntrega




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ParteEntregaGetById]
(
	@idParteEntrega int
)
AS
BEGIN

	SELECT
		[idParteEntrega],
		[Fecha],
		HoraLlegada,
		HoraSalida,
		idOrdenTrabajo,
		Entrega,
		NroParte,
		Adicionales,
		isnull(Conexion, 0) as Conexion,
		isnull(Desconexion,0) as desconexion,
		observacion
		,nroRemito
	FROM [ParteEntrega]
	WHERE
		([idParteEntrega] = @idParteEntrega)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ParteEntregaGetAll]
AS
BEGIN

	SELECT
		[idParteEntrega],
		[Fecha],
		HoraSalida,
		HoraLlegada,
		idOrdenTrabajo,
		Entrega,
		NroParte,
		Adicionales,
		Conexion,
		Desconexion,
		Observacion,
		nroRemito 
	FROM [ParteEntrega]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ParteEntregaDelete]
(
	@idParteEntrega int
)
AS
BEGIN

	DELETE
	FROM [ParteEntrega]
	WHERE
		[idParteEntrega] = @idParteEntrega
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ParteEntregaAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParteEntregaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ParteEntregaAdd]
(
	@Fecha smalldatetime = NULL,
	@horaSalida datetime,
	@horaLlegada datetime,
	@idOrdenTrabajo int,
	@entrega bit,
	@nroParte varchar(50),
	@adicionales varchar(150),
	@conexion bit,
	@desconexion bit,
	@observacion varchar(550),
	@nroRemito varchar(100)
)
AS
BEGIN

DECLARE @IDPE INT
SET @IDPE = 0
SELECT @IDPE = isnull(idPARTEENTREGA,0)
FROM PARTEENTREGA
WHERE  isnull(fecha,0) = isnull(@Fecha,0)	
	AND isnull(idOrdenTrabajo,0) = isnull(@idOrdenTrabajo,0)
	AND isnull(entrega,0) = isnull(@entrega,0)
	AND isnull(nroParte,0) = isnull(@nroParte,0)
	AND isnull(Adicionales,0) = isnull(@adicionales,0)
	AND isnull(conexion,0) = isnull(@conexion,0)
	AND isnull(desconexion,0) = isnull(@desconexion,0)
	AND isnull(observacion,0) = isnull(@observacion,0)
	AND isnull(nroRemito,0) = isnull(@nroRemito,0)

IF (ISNULL(@IDPE,0) = 0)
BEGIN
	INSERT
	INTO [ParteEntrega]
	(
		[Fecha],
		HoraSalida,
		HoraLlegada,
		idOrdenTrabajo,
		Entrega,
		NroParte,
		Adicionales,
		Conexion,
		Desconexion,
		Observacion,
		nroRemito
	)
	VALUES
	(
		@Fecha,
		@horaSalida,
		@horaLlegada,
		@idOrdenTrabajo,
		@entrega,
		@nroParte,
		@adicionales,
		@conexion,
		@desconexion,
		@observacion,
		@nroRemito
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

ELSE
BEGIN SELECT @IDPE END

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PaisGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaisGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PaisGetAll]
AS
BEGIN

	SELECT
		[CodigoPais],
		[Nombre]
	FROM [Pais]
	ORDER BY Nombre
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PaisAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaisAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PaisAdd]
(
	@Nombre nvarchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Pais]
	(
		[Nombre]
	)
	VALUES
	(
		@Nombre
	)


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoUpdate]
(
	@idOrden int,
	@idTransporte int,
	@idEquipo int,
	@Horario smalldatetime,
	@Salida nvarchar(50),
	@Llegada nvarchar(50),
	@KmsRecorridos decimal(18,2),
	@idSolicitudCliente int,
	@Controlo nvarchar(150),
	@Verifico nvarchar(150),
	@idTrailer int,
	@idChofer int,
	@idAcompanante int,
	@baja bit,
	@entrega bit
)
AS
BEGIN

	UPDATE [OrdenTrabajo]
	SET
		[idTransporte] = @idTransporte,
		[idEquipo] = @idEquipo,
		[Horario] = @Horario,
		[Salida] = @Salida,
		[Llegada] = @Llegada,
		[KmsRecorridos] = @KmsRecorridos,
		[idSolicitudCliente] = @idSolicitudCliente,
		[Controlo] = @Controlo,
		[Verifico] = @Verifico,
		[idTrailer] = @idTrailer,
		[idChofer] = @idChofer,
		[idAcompanante] = @idAcompanante,
		[baja] = @baja,
		[Entrega] = @entrega
	WHERE
		[idOrden] = @idOrden




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoGetById]
(
	@idOrden int
)
AS
BEGIN

	SELECT
		[idOrden],
		[idTransporte],
		[idEquipo],
		[Horario],
		[Salida],
		[Llegada],
		[KmsRecorridos],
		[idSolicitudCliente],
		[Controlo],
		[Verifico],
		[idTrailer],
		[idChofer],
		[idAcompanante]
	FROM [OrdenTrabajo]
	WHERE
		([idOrden] = @idOrden)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPersonalFilesByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPersonalFilesByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetPersonalFilesByFilter]
	(
		@NOMBRE varchar(150),
		@IDPERSONAL int,
		@LONGUITUD int
	)
	
AS
BEGIN

	SELECT 
		PF.Nombre,
		PF.Ruta,
		SUBSTRING (PF.Nombre,@LONGUITUD,150) as FECHA,
		PF.idPersonalFiles
		
	FROM PersonalFiles PF
	WHERE 
	(PF.idPersonal = @idPersonal)
	AND (PF.Nombre like + ''%'' + @nombre  + ''%'')
	
	ORDER BY PF.Nombre DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[getIdFilesByIdPersonal]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getIdFilesByIdPersonal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[getIdFilesByIdPersonal] 
	@idPersonal int
AS
BEGIN
	SELECT 
		F.idPersonalFiles
	FROM PersonalFiles f
	WHERE f.idPersonalFiles=@idPersonal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoGetAll]
AS
BEGIN

	SELECT
		[idOrden],
		[idTransporte],
		[idEquipo],
		[Horario],
		[Salida],
		[Llegada],
		[KmsRecorridos],
		[idSolicitudCliente],
		[Controlo],
		[Verifico],
		[idTrailer],
		idChofer,
		idAcompanante
	FROM [OrdenTrabajo]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleUpdate]
(
	@idOrdenDetalle int,
	@idOrden int,
	@idPersonal int,
	@idEquipoCliente int,
	@idTipoServicio int,
	@idEquipoPropio int
)
AS
BEGIN

	UPDATE [OrdenTrabajoDetalle]
	SET
		[idOrden] = @idOrden,
		[idPersonal] = @idPersonal,
		[idEquipoCliente] = @idEquipoCliente,
		[idTipoServicio] = @idTipoServicio,
		[idEquipoPropio] = @idEquipoPropio
	WHERE
		[idOrdenDetalle] = @idOrdenDetalle




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetByIdOrden]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetByIdOrden]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetByIdOrden]
(
	@idOrden int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrden] = @idOrden)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetById]
(
	@idOrdenDetalle int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrdenDetalle] = @idOrdenDetalle)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetByFilter]
(
	@idOrdenDetalle int,
	@idOrden int,
	@idPersonal int,
	@idEquipoCliente int,
	@idTipoServicio int,
	@idEquipoPropio int
)
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
	WHERE
		([idOrdenDetalle] = @idOrdenDetalle OR @idOrdenDetalle IS NULL) AND
		([idOrden] = @idOrden OR @idOrden IS NULL) AND
		([idPersonal] = @idPersonal OR @idPersonal IS NULL) AND
		([idEquipoCliente] = @idEquipoCliente OR @idEquipoCliente IS NULL) AND
		([idTipoServicio] = @idTipoServicio OR @idTipoServicio IS NULL) AND
		([idEquipoPropio] = @idEquipoPropio OR @idEquipoPropio IS NULL)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleGetAll]
AS
BEGIN

	SELECT
		[idOrdenDetalle],
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	FROM [OrdenTrabajoDetalle]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleDelete]
(
	@idOrdenDetalle int
)
AS
BEGIN

	DELETE
	FROM [OrdenTrabajoDetalle]
	WHERE
		[idOrdenDetalle] = @idOrdenDetalle
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDetalleAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDetalleAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OrdenTrabajoDetalleAdd]
(
	@idOrden int,
	@idPersonal int = NULL,
	@idEquipoCliente int = NULL,
	@idTipoServicio int = NULL,
	@idEquipoPropio int = NULL
)
AS
BEGIN


	INSERT
	INTO [OrdenTrabajoDetalle]
	(
		[idOrden],
		[idPersonal],
		[idEquipoCliente],
		[idTipoServicio],
		[idEquipoPropio]
	)
	VALUES
	(
		@idOrden,
		@idPersonal,
		@idEquipoCliente,
		@idTipoServicio,
		@idEquipoPropio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoDelete]
(
	@idOrden int
)
AS
BEGIN

	DELETE
	FROM [OrdenTrabajo]
	WHERE
		[idOrden] = @idOrden
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoUpdate]
(
	@idOtiConcepto int,
	@idOti int,
	@Concepto varchar(150),
	@Valor varchar(150),
	@idTipoServicio int
)
AS
BEGIN

	UPDATE [OrdenTrabajoConcepto]
	SET
		[idOti] = @idOti,
		[Concepto] = @Concepto,
		[Valor] = @Valor,
		[idTipoServicio] = @idTipoServicio
	WHERE
		[idOtiConcepto] = @idOtiConcepto




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetByIdOrden]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetByIdOrden]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoGetByIdOrden]
(
	@idOrden int
)
AS
BEGIN

	SELECT
		[idOtiConcepto],
		[idOti],
		[Concepto],
		[Valor],
		[idTipoServicio]
	FROM [OrdenTrabajoConcepto]
	WHERE idOti = @idOrden
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoGetById]
(
	@idOtiConcepto int
)
AS
BEGIN

	SELECT
		[idOtiConcepto],
		[idOti],
		[Concepto],
		[Valor],
		[idTipoServicio]
	FROM [OrdenTrabajoConcepto]
	WHERE
		([idOtiConcepto] = @idOtiConcepto)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoGetAll]
AS
BEGIN

	SELECT
		[idOtiConcepto],
		[idOti],
		[Concepto],
		[Valor],
		[idTipoServicio]
	FROM [OrdenTrabajoConcepto]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoDelete]
(
	@idOtiConcepto int
)
AS
BEGIN

	DELETE
	FROM [OrdenTrabajoConcepto]
	WHERE
		[idOtiConcepto] = @idOtiConcepto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoConceptoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoConceptoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoConceptoAdd]
(
	@idOti int,
	@Concepto varchar(150) = NULL,
	@Valor varchar(150) = NULL,
	@idTipoServicio int
)
AS
BEGIN


	INSERT
	INTO [OrdenTrabajoConcepto]
	(
		[idOti],
		[Concepto],
		[Valor],
		[idTipoServicio]
	)
	VALUES
	(
		@idOti,
		@Concepto,
		@Valor,
		@idTipoServicio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE  PROCEDURE [dbo].[OrdenTrabajoAdd]
(
	@idTransporte int = NULL,
	@idEquipo int = NULL,
	@Horario smalldatetime = NULL,
	@Salida nvarchar(50) = NULL,
	@Llegada nvarchar(50) = NULL,
	@KmsRecorridos decimal(18,2) = NULL,
	@idSolicitudCliente int = NULL,
	@Controlo nvarchar(150) = NULL,
	@Verifico nvarchar(150) = NULL,
	@idTrailer int = NULL,
	@idChofer int = null,
	@idAcompanante int = null,
	@entrega bit = null
)
AS
BEGIN

DECLARE @IDOT INT
SET @IDOT = 0
SELECT @IDOT = isnull(idOrden,0)
FROM ORDENTRABAJO
WHERE  isnull(idTransporte,0) = isnull(@idTransporte,0)
	AND isnull(idEquipo,0) = isnull(@idequipo,0)
	AND isnull(KmsRecorridos,0) = isnull(@KmsRecorridos,0)
	AND isnull(idSolicitudCliente,0) = isnull(@idSolicitudCliente,0)
	AND isnull(Controlo,0) = isnull(@Controlo,0)
	AND isnull(Verifico,0) = isnull(@Verifico,0)
	AND isnull(idTrailer,0) = isnull(@idTrailer,0)
	AND isnull(idChofer,0) = isnull(@idChofer,0)
	AND isnull(idAcompanante,0) = isnull(@idAcompanante,0)
	AND isnull(entrega,0) = isnull(@entrega,0)

IF (ISNULL(@IDOT,0) = 0)
BEGIN
	INSERT
	INTO [OrdenTrabajo]
	(
		[idTransporte],
		[idEquipo],
		[Horario],
		[Salida],
		[Llegada],
		[KmsRecorridos],
		[idSolicitudCliente],
		[Controlo],
		[Verifico],
		[idTrailer],
		[idChofer],
		[idAcompanante],
		[Entrega]
		
	)
	VALUES
	(
		@idTransporte,
		@idEquipo,
		@Horario,
		@Salida,
		@Llegada,
		@KmsRecorridos,
		@idSolicitudCliente,
		@Controlo,
		@Verifico,
		@idTrailer,
		@idChofer,
		@idAcompanante,
		@entrega
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
ELSE
BEGIN SELECT @IDOT END


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[NacionalidadUpdate]
(
	@idNacionalidad int,
	@Descripcion varchar(150)
)
AS
BEGIN

	UPDATE [Nacionalidad]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idNacionalidad] = @idNacionalidad




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[NacionalidadGetById]
(
	@idNacionalidad int
)
AS
BEGIN

	SELECT
		[idNacionalidad],
		[Descripcion]
	FROM [Nacionalidad]
	WHERE
		([idNacionalidad] = @idNacionalidad)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[NacionalidadGetAll]
AS
BEGIN

	SELECT
		[idNacionalidad],
		[Descripcion]
	FROM [Nacionalidad]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[NacionalidadDelete]
(
	@idNacionalidad int
)
AS
BEGIN

	DELETE
	FROM [Nacionalidad]
	WHERE
		[idNacionalidad] = @idNacionalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[NacionalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NacionalidadAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[NacionalidadAdd]
(
	@Descripcion varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Nacionalidad]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MovimientoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MovimientoUpdate]
(
	@idMovimiento int,
	@idLugarDesde int,
	@idLugarHasta int,
	@Origen nvarchar(50) = NULL,
	@Destino nvarchar(50) = NULL,
	@KmsEstimados decimal(18,2) = NULL,
	@FechaEstimada smalldatetime = NULL
)
AS
BEGIN

	UPDATE [Movimiento]
	SET
		[idLugarOrigen] = @idLugarDesde,
		[idLugarDestino] = @idLugarHasta,
		[pozoDestino] = UPPER(@Destino),
		[pozoOrigen] = UPPER(@Origen),
		[KmsEstimados] = @KmsEstimados,
		[FechaEstimada] = @FechaEstimada
	WHERE
		[idMovimiento] = @idMovimiento




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MovimientoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MovimientoGetById]
(
	@idMovimiento int
)
AS
BEGIN

	SELECT
		[idMovimiento],
		isnull(idLugarDestino,0) as idLugarDestino,
		isnull(idLugarOrigen,0) as idLugarOrigen,
		[pozoOrigen],
		[pozoDestino],
		[KmsEstimados],
		[FechaEstimada]
	FROM [Movimiento]
	WHERE
		([idMovimiento] = @idMovimiento)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MovimientoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MovimientoGetAll]
AS
BEGIN

	SELECT
		[idMovimiento],
		isnull(idLugarDestino,0) as idLugarDestino,
		isnull(idLugarOrigen,0) as idLugarOrigen,
		[pozoOrigen],
		[pozoDestino],
		[KmsEstimados],
		[FechaEstimada]
	FROM [Movimiento]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MovimientoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MovimientoDelete]
(
	@idMovimiento int
)
AS
BEGIN

	DELETE
	FROM [Movimiento]
	WHERE
		[idMovimiento] = @idMovimiento
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MovimientoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovimientoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MovimientoAdd]
(
	@idLugarOrigen int,
	@idLugarDestino int,
	@Origen nvarchar(50) = NULL,
	@Destino nvarchar(50) = NULL,
	@KmsEstimados decimal(18,2) = NULL,
	@FechaEstimada smalldatetime = NULL
)
AS
BEGIN


	INSERT
	INTO [Movimiento]
	(
		[idLugarDestino],
		[idLugarOrigen],
		[pozoOrigen],
		[pozoDestino],
		[KmsEstimados],
		[FechaEstimada]
	)
	VALUES
	(
		@idLugarDestino,
		@idLugarOrigen,
		UPPER(@Origen),
		UPPER(@Destino),
		@KmsEstimados,
		@FechaEstimada
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadUpdate]
(
	@idModalidad int,
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN

	UPDATE [Modalidad]
	SET
		[Descripcion] = @Descripcion,
		[Baja] = @Baja
	WHERE
		[idModalidad] = @idModalidad




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadGetById]
(
	@idModalidad int
)
AS
BEGIN

	SELECT
		[idModalidad],
		[Descripcion],
		[Baja]
	FROM [Modalidad]
	WHERE
		([idModalidad] = @idModalidad)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadGetAllBaja]
AS
BEGIN

	SELECT
		[idModalidad],
		[Descripcion],
		[Baja]
	FROM [Modalidad]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadGetAll]
AS
BEGIN

	SELECT
		[idModalidad],
		[Descripcion],
		[Baja]
	FROM [Modalidad]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadDelete]
(
	@idModalidad int
)
AS
BEGIN

	DELETE
	FROM [Modalidad]
	WHERE
		[idModalidad] = @idModalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModalidadAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModalidadAdd]
(
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN


	INSERT
	INTO [Modalidad]
	(
		[Descripcion],
		[Baja]
	)
	VALUES
	(
		@Descripcion,
		@Baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModulosGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModulosGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModulosGetById]
(
	@IDModulo INT
)
AS
BEGIN
	SELECT idModulo,
			Descripcion
	FROM Modulos
	WHERE idModulo = @IDModulo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ModulosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModulosGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ModulosGetAll]
AS
BEGIN
	SELECT idModulo,
			Descripcion
	FROM Modulos
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MarcaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MarcaUpdate]
(
	@idMarca int,
	@Descripcion varchar(150)
)
AS
BEGIN

	UPDATE [Marca]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idMarca] = @idMarca




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MarcaGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MarcaGetById]
(
	@idMarca int
)
AS
BEGIN

	SELECT
		[idMarca],
		[Descripcion]
	FROM [Marca]
	WHERE
		([idMarca] = @idMarca)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MarcaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MarcaGetAll]
AS
BEGIN

	SELECT
		[idMarca],
		[Descripcion]
	FROM [Marca]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MarcaDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MarcaDelete]
(
	@idMarca int
)
AS
BEGIN

	DELETE
	FROM [Marca]
	WHERE
		[idMarca] = @idMarca
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[MarcaAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[MarcaAdd]
(
	@Descripcion varchar(150)
)
AS
BEGIN


	INSERT
	INTO [Marca]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LugarUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LugarUpdate]
(
	@idLugar int,
	@Descripcion nvarchar(150)
)
AS
BEGIN

	UPDATE [Lugar]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idLugar] = @idLugar




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LugarGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LugarGetById]
(
	@idLugar int
)
AS
BEGIN

	SELECT
		[idLugar],
		[Descripcion]
	FROM [Lugar]
	WHERE
		([idLugar] = @idLugar)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LugarGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LugarGetAll]
AS
BEGIN

	SELECT
		[idLugar],
		[Descripcion]
	FROM [Lugar]
	WHERE ISNULL(Baja,0) = 0
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LugarDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LugarDelete]
(
	@idLugar int
)
AS
BEGIN

	UPDATE [Lugar]
	SET BAJA = 1
	WHERE
		[idLugar] = @idLugar
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LugarAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LugarAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LugarAdd]
(
	@Descripcion nvarchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Lugar]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadUpdate]
(
	@idLocalidad int,
	@idDepartamento int,
	@Nombre nvarchar(250)
)
AS
BEGIN

	UPDATE [Localidad]
	SET
		[idDepartamento] = @idDepartamento,
		[Nombre] = @Nombre
	WHERE
		[idLocalidad] = @idLocalidad




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetByIdProvincia]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetByIdProvincia]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadGetByIdProvincia]
(
 @IDPROVINCIA INT
)
AS

BEGIN

SELECT * FROM Localidad
WHERE idDepartamento IN(SELECT idDepartamento FROM Departamento 
						WHERE idProvincia = @IDPROVINCIA)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadGetById]
(
	@idLocalidad int
)
AS
BEGIN

	SELECT
		[idLocalidad],
		[idDepartamento],
		[Nombre]
	FROM [Localidad]
	WHERE
		([idLocalidad] = @idLocalidad)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadGetAll]
AS
BEGIN

	SELECT
		[idLocalidad],
		[idDepartamento],
		[Nombre]
	FROM [Localidad]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadDelete]
(
	@idLocalidad int
)
AS
BEGIN

	DELETE
	FROM [Localidad]
	WHERE
		[idLocalidad] = @idLocalidad
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[LocalidadAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocalidadAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LocalidadAdd]
(
	@idDepartamento int,
	@Nombre nvarchar(250)
)
AS
BEGIN


	INSERT
	INTO [Localidad]
	(
		[idDepartamento],
		[Nombre]
	)
	VALUES
	(
		@idDepartamento,
		@Nombre
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[KilometrosGetByFilter]
(
	@idDesde int,
	@idHasta int
)
AS

BEGIN
	SELECT IDKMS,
			[idLugarDesde],
           [idLugarHasta],
           [Kms],
           KmsEstablecidos
	FROM KILOMETROS k
	LEFT JOIN Lugar lDesde on k.idLugarDesde = lDesde.idLugar
	LEFT JOIN Lugar lHas on k.idLugarHasta = lHas.idLugar
	WHERE isnull(lDesde.Baja,0) = 0
	AND	isnull(lHas.Baja,0) = 0
	AND (k.idLugarDesde = @idDesde or @idDesde is null)
	AND (k.idLugarHasta = @idHasta or @idHasta is null)
	ORDER BY lDesde.Descripcion, lHas.Descripcion
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetBy]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetBy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[KilometrosGetBy]
	@idLugarDesde int,
	@idLugarHasta int
AS
BEGIN

	
	SELECT idKms,
			idLugarDesde,
			idLugarHasta,
			Kms,
			KmsEstablecidos
	FROM Kilometros
	WHERE (idLugarDesde = @idLugarDesde
			AND idLugarHasta = @idLugarHasta)
			or
			(idLugarHasta = @idLugarDesde
			AND idLugarDesde = @idLugarHasta)
			
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[KilometrosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[KilometrosGetAll]
AS

BEGIN
	SELECT IDKMS,
			[idLugarDesde],
           [idLugarHasta],
           [Kms],
           [KmsEstablecidos]
	FROM KILOMETROS k
	LEFT JOIN Lugar lDesde on k.idLugarDesde = lDesde.idLugar
	LEFT JOIN Lugar lHas on k.idLugarHasta = lHas.idLugar
	WHERE isnull(lDesde.Baja,0) = 0
	AND	isnull(lHas.Baja,0) = 0
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[KilometrosDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[KilometrosDelete]
(
	@IDKMS INT
)
AS

BEGIN
	
	DELETE FROM KILOMETROS
	WHERE IDKMS = @IDKMS
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[KilometrosAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KilometrosAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[KilometrosAdd]
(
	@idLugarDesde INT,
	@idLugarHasta INT,
	@Kms DECIMAL(18,2),
	@KmsEstablecidos BIT
)
AS

BEGIN

INSERT INTO [Kilometros]
           (
           [idLugarDesde],
           [idLugarHasta],
           [Kms],
           [KmsEstablecidos]
           )
     VALUES
           (
           @idLugarDesde,
           @idLugarHasta,
           @Kms,
           @KmsEstablecidos
           )
           
	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionUpdate]
(
	@idFuncion int,
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN

	UPDATE [Funcion]
	SET
		[Descripcion] = @Descripcion,
		[Baja] = @Baja
	WHERE
		[idFuncion] = @idFuncion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionGetById]
(
	@idFuncion int
)
AS
BEGIN

	SELECT
		[idFuncion],
		[Descripcion],
		[Baja]
	FROM [Funcion]
	WHERE
		([idFuncion] = @idFuncion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionGetAllBaja]
AS
BEGIN

	SELECT
		[idFuncion],
		[Descripcion],
		[Baja]
	FROM [Funcion]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionGetAll]
AS
BEGIN

	SELECT
		[idFuncion],
		[Descripcion],
		[Baja]
	FROM [Funcion]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionDelete]
(
	@idFuncion int
)
AS
BEGIN

	DELETE
	FROM [Funcion]
	WHERE
		[idFuncion] = @idFuncion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[FuncionAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FuncionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FuncionAdd]
(
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN


	INSERT
	INTO [Funcion]
	(
		[Descripcion],
		[Baja]
	)
	VALUES
	(
		@Descripcion,
		@Baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstudiosUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstudiosUpdate]
(
	@idEstudio int,
	@Descripcion varchar(150)
)
AS
BEGIN

	UPDATE [Estudios]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idEstudio] = @idEstudio




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstudiosGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstudiosGetById]
(
	@idEstudio int
)
AS
BEGIN

	SELECT
		[idEstudio],
		[Descripcion]
	FROM [Estudios]
	WHERE
		([idEstudio] = @idEstudio)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstudiosGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstudiosGetAll]
AS
BEGIN

	SELECT
		[idEstudio],
		[Descripcion]
	FROM [Estudios]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstudiosDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstudiosDelete]
(
	@idEstudio int
)
AS
BEGIN

	DELETE
	FROM [Estudios]
	WHERE
		[idEstudio] = @idEstudio
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstudiosAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstudiosAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstudiosAdd]
(
	@Descripcion varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Estudios]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstadoCivilUpdate]
(
	@idEstadoCivil int,
	@Descripcion varchar(150)
)
AS
BEGIN

	UPDATE [EstadoCivil]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idEstadoCivil] = @idEstadoCivil




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstadoCivilGetById]
(
	@idEstadoCivil int
)
AS
BEGIN

	SELECT
		[idEstadoCivil],
		[Descripcion]
	FROM [EstadoCivil]
	WHERE
		([idEstadoCivil] = @idEstadoCivil)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstadoCivilGetAll]
AS
BEGIN

	SELECT
		[idEstadoCivil],
		[Descripcion]
	FROM [EstadoCivil]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstadoCivilDelete]
(
	@idEstadoCivil int
)
AS
BEGIN

	DELETE
	FROM [EstadoCivil]
	WHERE
		[idEstadoCivil] = @idEstadoCivil
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EstadoCivilAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCivilAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EstadoCivilAdd]
(
	@Descripcion varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [EstadoCivil]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquipoGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquipoGestionGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquipoGestionGetByFilter]
	(
		@patente varchar(250),
		@ruta varchar(250),
		@vCedula int,
		@vPatente int,
		@vtv varchar(50),
		@idCodificacion int,
		@idEmpresa int
		
	)
	
AS

BEGIN

	SELECT V.idVehiculo,
		V.Patente,		
		E.RazonSocial AS EMPRESA,
		M.Descripcion AS MARCA,
		SE.Descripcion AS Sector,
		(C.Codigo) + '' ''+ CONVERT(VARCHAR(10),vi.NroCodificacion) AS Codificacion ,
		CONVERT(VARCHAR(10),VV.vencimientoCedula, 103) AS vencimientoCedula,
		CONVERT(VARCHAR(10),VV.vencimientoPatente, 103) AS vencimientoPatente,
		CONVERT(VARCHAR(10),VV.vencimientoLeasing, 103) AS vencimientoLeasing,
		CONVERT(VARCHAR(10),VV.vencimientoVTV, 103) AS vencimientoVTV,
		CONVERT(VARCHAR(10),VV.vencimiento, 103) AS vencimiento,
		CONVERT(VARCHAR(10),VS.vigenciaHasta, 103) AS VencimientoSeguro
		
	FROM Vehiculo V
	left JOIN VehiculoIdentificacion vi on vi.idVehiculo  = v.idVehiculo
	left JOIN VehiculoVencimiento VV ON V.idVehiculo = VV.idVehiculo 
	left JOIN Codificacion C ON C.idCodificacion = vi.idCodificacion
	left JOIN EMPRESA E ON E.idEmpresa = V.idEmpresa
	left JOIN Sector SE ON SE.idSector = V.idSector
													
	left JOIN Marca M ON M.idMarca = V.idMarca
	left JOIN VehiculoSeguro VS ON VS.idVehiculo = V.idVehiculo
				
	WHERE
	(V.esEquipo = 1)
	AND (V.baja = 0)
	AND (C.idCodificacion = @idCodificacion OR @idCodificacion IS NULL)
	AND (V.Patente LIKE ''%'' + @patente + ''%'' OR (@patente IS NULL) )
	AND (V.idEmpresa = @idEmpresa or @idEmpresa is NULL)	
	AND (VV.vencimientoCedula >= @vCedula OR @vCedula IS NULL)
	AND (VV.vencimientoPatente >= @vPatente OR @vPatente IS NULL)
	AND (VV.NroVTV = @vtv OR @vtv IS NULL)
	AND (VV.RUTA = @ruta OR @ruta IS NULL)
	
		
	ORDER BY VV.vencimientoPatente,VS.vigenciaHasta, VV.vencimiento, VV.vencimientoVTV, VV.vencimientoCedula, VV.vencimientoLeasing,V.Patente desc

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaUpdate]
(
	@idEmpresa int,
	@Descripcion nvarchar(150),
	@RazonSocial varchar(250),
	@Domicilio varchar(150),
	@CondicionIVA varchar(50),
	@CUIT varchar(50),
	@codTango varchar(50),
	@interna bit
)
AS
BEGIN

	UPDATE [Empresa]
	SET
		[Descripcion] = @Descripcion,
		[RazonSocial] = @RazonSocial,
		[Domicilio] = @Domicilio,
		[CondicionIVA] = @CondicionIVA,
		[CUIT] = @CUIT,
		[codTango] = @codTango,
		[interna] = @interna
	WHERE
		[idEmpresa] = @idEmpresa




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaGetById]
(
	@idEmpresa int
)
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna]
	FROM [Empresa]
	WHERE
		([idEmpresa] = @idEmpresa)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaGetByFilter]
(
	@nombre varchar(150)
)
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		isnull([interna],0) as interna
	FROM [Empresa]
	where (Descripcion like ''%'' + @nombre + ''%'' or @nombre is null)
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetAllInternas]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetAllInternas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaGetAllInternas]
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna]
	FROM [Empresa]
	
	WHERE
		interna=1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaGetAll]
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna]
	FROM [Empresa]
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaDelete]
(
	@idEmpresa int
)
AS
BEGIN

	DELETE
	FROM [Empresa]
	WHERE
		[idEmpresa] = @idEmpresa
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EmpresaAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpresaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EmpresaAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@RazonSocial varchar(250) = NULL,
	@Domicilio varchar(150) = NULL,
	@CondicionIVA varchar(50) = NULL,
	@CUIT varchar(50) = NULL,
	@codTango varchar(50) = NULL,
	@interna bit = NULL
)
AS
BEGIN


	INSERT
	INTO [Empresa]
	(
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna]
	)
	VALUES
	(
		@Descripcion,
		@RazonSocial,
		@Domicilio,
		@CondicionIVA,
		@CUIT,
		@codTango,
		@interna
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaUpdate]
(
	@idEspecificaciones int,
	@Descripcion nvarchar(150),
	@RequiereCuadrilla bit,
	@RequiereEquipo bit,
	@EquipoPropio bit,
    @RequierePersonal bit,
    @RequiereTraslado bit,
    @RequiereMaterial bit
)
AS
BEGIN

	UPDATE [EspecificacionTecnica]
	SET
		[Descripcion] = @Descripcion,
		[RequiereCuadrilla] = @RequiereCuadrilla,
		[RequiereEquipo] = @RequiereEquipo,
		[EquipoPropio] = @EquipoPropio,
		[RequierePersonal] = @RequierePersonal,
		[RequiereTraslado] = @RequiereTraslado,
		[RequiereMaterial] = @RequiereMaterial
		
	WHERE
		[idEspecificaciones] = @idEspecificaciones




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetEquipo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetEquipo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetEquipo]
(
	@equipo bit,
	@cuadrilla bit,
	@personal bit,
	@traslado bit,
	@material bit
)
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
		RequierePersonal,
		RequiereTraslado,
		RequiereMaterial
	FROM [EspecificacionTecnica]
	where (isnull(RequiereEquipo,0) = @equipo or @equipo is null)
	and (isnull(RequiereCuadrilla,0) = @cuadrilla or @cuadrilla is null)
	and (isnull(RequierePersonal,0) = @personal or @personal is null)
	and (isnull(RequiereTraslado,0) = @traslado or @traslado is null)
	and (isnull(RequiereMaterial,0) = @material or @material is null)
	order by Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetCuadrilla]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetCuadrilla]
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio]
	FROM [EspecificacionTecnica]
	where RequiereCuadrilla = 1
	order by Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdTipoServicio]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByIdTipoServicio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetByIdTipoServicio]
(
	@idTipoServicio int
)
AS
BEGIN

	SELECT
		et.[idEspecificaciones],
		et.[Descripcion],
		et.[RequiereCuadrilla],
		et.[RequiereEquipo],
		et.[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica] ET
	INNER JOIN TipoServicioEspecificacion TS ON TS.idEspecificacion = et.idEspecificaciones
	WHERE
		(TS.idTipoServicio = @idTipoServicio)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByIdSolicitud]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByIdSolicitud]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetByIdSolicitud]
(
	@idSolicitud int
)
AS
BEGIN

	SELECT
		et.[idEspecificaciones],
		et.[Descripcion],
		et.[RequiereCuadrilla],
		et.[RequiereEquipo],
		et.[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica] ET
	INNER JOIN SolicitudClienteEspecificacion TS ON TS.idEspecificacion = et.idEspecificaciones
	WHERE
		(TS.idSolicitudCliente = @idSolicitud)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetById]
(
	@idEspecificaciones int
)
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	FROM [EspecificacionTecnica]
	WHERE
		([idEspecificaciones] = @idEspecificaciones)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetByFilter]
(
	@descripcion varchar(150)
)
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		RequiereCuadrilla,
		RequiereEquipo
	FROM [EspecificacionTecnica]
	where descripcion like ''%''+@descripcion+''%'' or @descripcion is null
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaGetAll]
AS
BEGIN

	SELECT
		[idEspecificaciones],
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio]
	FROM [EspecificacionTecnica]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaDelete]
(
	@idEspecificaciones int
)
AS
BEGIN

	DELETE
	FROM [EspecificacionTecnica]
	WHERE
		[idEspecificaciones] = @idEspecificaciones
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionTecnicaAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionTecnicaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionTecnicaAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@RequiereCuadrilla bit = NULL,
	@RequiereEquipo bit = NULL,
	@EquipoPropio bit = NULL,
    @RequierePersonal bit,
    @RequiereTraslado bit,
    @RequiereMateriales bit
)
AS
BEGIN


	INSERT
	INTO [EspecificacionTecnica]
	(
		[Descripcion],
		[RequiereCuadrilla],
		[RequiereEquipo],
		[EquipoPropio],
        [RequierePersonal],
        [RequiereTraslado],
        [RequiereMaterial]
	)
	VALUES
	(
		@Descripcion,
		@RequiereCuadrilla,
		@RequiereEquipo,
		@EquipoPropio,
		@RequierePersonal,
		@RequiereTraslado,
		@RequiereMateriales
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleUpdate]
(
	@idEspecificacionDetalle int,
	@idEspecificacion int,
	@idCuadrilla int,
	@idEquipoCliente int,
	@idEquipoPropio int
)
AS
BEGIN

	UPDATE [EspecificacionDetalle]
	SET
		[idEspecificacion] = @idEspecificacion,
		[idCuadrilla] = @idCuadrilla,
		[idEquipoCliente] = @idEquipoCliente,
		[idEquipoPropio] = @idEquipoPropio
	WHERE
		[idEspecificacionDetalle] = @idEspecificacionDetalle




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetByIdEspecificacion]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetByIdEspecificacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleGetByIdEspecificacion]
(
	@idEspecificacion int
)
AS
BEGIN

	SELECT
		[idEspecificacionDetalle],
		[idEspecificacion],
		[idCuadrilla],
		[idEquipoCliente],
		[idEquipoPropio]
	FROM [EspecificacionDetalle]
	WHERE
		([idEspecificacion] = @idEspecificacion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleGetById]
(
	@idEspecificacionDetalle int
)
AS
BEGIN

	SELECT
		[idEspecificacionDetalle],
		[idEspecificacion],
		[idCuadrilla],
		[idEquipoCliente],
		[idEquipoPropio]
	FROM [EspecificacionDetalle]
	WHERE
		([idEspecificacionDetalle] = @idEspecificacionDetalle)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleGetByFilter]
(
	@idEspecificacionDetalle int,
	@idEspecificacion int,
	@idCuadrilla int,
	@idEquipoCliente int,
	@idEquipoPropio int
)
AS
BEGIN

	SELECT
		[idEspecificacionDetalle],
		[idEspecificacion],
		[idCuadrilla],
		[idEquipoCliente],
		[idEquipoPropio]
	FROM [EspecificacionDetalle]
	WHERE
		([idEspecificacionDetalle] = @idEspecificacionDetalle OR @idEspecificacionDetalle IS NULL) AND
		([idEspecificacion] = @idEspecificacion OR @idEspecificacion IS NULL) AND
		([idCuadrilla] = @idCuadrilla OR @idCuadrilla IS NULL) AND
		([idEquipoCliente] = @idEquipoCliente OR @idEquipoCliente IS NULL) AND
		([idEquipoPropio] = @idEquipoPropio OR @idEquipoPropio IS NULL)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleGetAll]
AS
BEGIN

	SELECT
		[idEspecificacionDetalle],
		[idEspecificacion],
		[idCuadrilla],
		[idEquipoCliente],
		[idEquipoPropio]
	FROM [EspecificacionDetalle]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleDeleteByIdEsp]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleDeleteByIdEsp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleDeleteByIdEsp]
(
	@idEspecificacion int
)
AS
BEGIN

	DELETE
	FROM [EspecificacionDetalle]
	WHERE
		[idEspecificacion] = @idEspecificacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleDelete]
(
	@idEspecificacionDetalle int
)
AS
BEGIN

	DELETE
	FROM [EspecificacionDetalle]
	WHERE
		[idEspecificacionDetalle] = @idEspecificacionDetalle
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EspecificacionDetalleAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EspecificacionDetalleAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EspecificacionDetalleAdd]
(
	@idEspecificacion int = NULL,
	@idCuadrilla int = NULL,
	@idEquipoCliente int = NULL,
	@idEquipoPropio int = NULL
)
AS
BEGIN


	INSERT
	INTO [EspecificacionDetalle]
	(
		[idEspecificacion],
		[idCuadrilla],
		[idEquipoCliente],
		[idEquipoPropio]
	)
	VALUES
	(
		@idEspecificacion,
		@idCuadrilla,
		@idEquipoCliente,
		@idEquipoPropio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposUpdate]
(
	@idEquipo int,
	@Descripcion nvarchar(150),
	@idCliente int
)
AS
BEGIN

	UPDATE [Equipos]
	SET
		[Descripcion] = @Descripcion,
		idEmpresa = @idCliente
	WHERE
		[idEquipo] = @idEquipo

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetByIdCliente]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetByIdCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposGetByIdCliente]
(
	@idCliente int
)
AS
BEGIN

	SELECT
		[idEquipo],
		[Descripcion],
		idEmpresa
	FROM [Equipos]
	WHERE
		([idEmpresa] = @idCliente)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposGetById]
(
	@idEquipo int
)
AS
BEGIN

	SELECT
		[idEquipo],
		[Descripcion],
		idEmpresa
	FROM [Equipos]
	WHERE
		([idEquipo] = @idEquipo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposGetAll]
AS
BEGIN

	SELECT
		[idEquipo],
		[Descripcion],
		idEmpresa
	FROM [Equipos]
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposDelete]
(
	@idEquipo int
)
AS
BEGIN

	DELETE
	FROM [Equipos]
	WHERE
		[idEquipo] = @idEquipo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[EquiposAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EquiposAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[EquiposAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@idCliente int
)
AS
BEGIN


	INSERT
	INTO [Equipos]
	(
		[Descripcion],
		idEmpresa
	)
	VALUES
	(
		@Descripcion,
		@idCliente
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoGetByIdAcuerdoServicio]
(
	@idAcuerdoServicio int
)
AS
BEGIN

	SELECT
		[idAcuerdoConcepto],
		[Concepto],
		[Costo],
		[idEspecificacion],
		[idAcuerdoServicio],
		ET.DESCRIPCION AS ESPECIFICACION
	FROM [AcuerdoConceptoCosto] ACC
	LEFT JOIN ESPECIFICACIONTECNICA ET ON ET.IDESPECIFICACIONES = ACC.IDESPECIFICACION
	WHERE
		([idAcuerdoServicio] = @idAcuerdoServicio)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoGetByIdAcuerdo]
(
	@idAcuerdo int
)
AS
BEGIN
SELECT DISTINCT  
				acc.idAcuerdoConcepto, 
				acc.Concepto,
				acc.Costo, 
				max(cast(replace(DBO.GETDIFF(ACCANT.COSTO,acc.Costo),'' '', '''')as float)) AS COSTODIF, 
				accant.Costo as costoAnt,
				acc.idEspecificacion,
				case when (accant.Costo <> acc.costo) then
				999999 
				else
				isnull(ACCANT.IDESPECIFICACION, 999999) end AS ESPDIF, 
				ACCANT.IDESPECIFICACION as ESPAnt,
				acc.idAcuerdoServicio, ACC. idAcuerdoServicio AS SERVDIF, 
				ET.Descripcion /*+  case when (accant.Costo <> acc.costo Or ACCANT.Costo IS null ) then
									''['' +convert(varchar,999999) +'']'' else '''' end */
							  AS Especificacion,ac.orden
FROM            AcuerdoConceptoCosto  acc 
INNER JOIN		AcuerdoServicio ACS ON acc.idAcuerdoServicio = ACS.idAcuerdoServicio 
LEFT  JOIN EspecificacionTecnica ET ON acc.idEspecificacion = ET.idEspecificaciones

INNER JOIN ACUERDO A ON A.IDACUERDO = ACS.IDACUERDO /*ACUERDO ACTUAL*/
LEFT  JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = a.idCliente/*ACUERDO ANTERIOR*/
LEFT  JOIN ACUERDOSERVICIO ATANT ON ATANT.IDACUERDO = AANT.IDACUERDO AND ATANT.IDTIPOSERVICIO = ACS.IDTIPOSERVICIO and ACS.Requiere = ATANT.Requiere/*SERVICIOS ANTERIOR*/
LEFT  JOIN AcuerdoConceptoCosto ACCANT ON ACCANT.idAcuerdoServicio = ATANT.idAcuerdoServicio AND ACCANT.IDESPECIFICACION = ACC.IDESPECIFICACION /*ESPECIFICACIONES ANTERIOR*/
										and accant.costo = acc.costo
LEFT JOIN AcuerdoConcepto AC on ac.Concepto = acc.Concepto
WHERE        (ACS.idAcuerdo = @idAcuerdo)
GROUP BY acc.idAcuerdoConcepto, 
			acc.Concepto,
			acc.Costo, 
			acc.idEspecificacion,
			ACCANT.IDESPECIFICACION, 			
			acc.idAcuerdoServicio, 
			ACC.idAcuerdoServicio, 
			ET.Descripcion,accant.Costo,a.nroRevision
			,ac.orden
ORDER BY ac.orden
/*
SELECT * FROM AcuerdoConceptoCosto acc 
INNER JOIN		AcuerdoServicio ACS ON acc.idAcuerdoServicio = ACS.idAcuerdoServicio 
WHERE ACS.IDACUERDO IN(SELECT IDACUERDO FROM ACUERDO WHERE NROACUERDO LIKE ''AC%'' AND BAJA = 0)
*/

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoGetById]
(
	@idAcuerdoConcepto int
)
AS
BEGIN

	SELECT
		[idAcuerdoConcepto],
		[Concepto],
		[Costo],
		[idEspecificacion],
		[idAcuerdoServicio]
	FROM [AcuerdoConceptoCosto]
	WHERE
		([idAcuerdoConcepto] = @idAcuerdoConcepto)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoGetAll]
AS
BEGIN

	SELECT
		[idAcuerdoConcepto],
		[Concepto],
		[Costo],
		[idEspecificacion],
		[idAcuerdoServicio]
	FROM [AcuerdoConceptoCosto]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoDelete]
(
	@idAcuerdoConcepto int
)
AS
BEGIN

	DELETE
	FROM [AcuerdoConceptoCosto]
	WHERE
		[idAcuerdoConcepto] = @idAcuerdoConcepto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoAdd]
(
	@Concepto varchar(150),
	@Costo decimal(18,2),
	@idEspecificacion int,
	@idAcuerdoServicio int
)
AS
BEGIN


	INSERT
	INTO [AcuerdoConceptoCosto]
	(
		[Concepto],
		[Costo],
		[idEspecificacion],
		[idAcuerdoServicio]
	)
	VALUES
	(
		@Concepto,
		@Costo,
		@idEspecificacion,
		@idAcuerdoServicio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoAlerta]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoAlerta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoAlerta]

AS

BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		AMAX.NROREVISION,
		A.TITULO
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN (SELECT MAX(IDACUERDO)AS IDACUERDO, MAX(ISNULL(NROREVISION, 1)) AS NROREVISION, NROACUERDO
				FROM ACUERDO
				GROUP BY NROACUERDO) AMAX ON  AMAX.IDACUERDO = A.IDACUERDO
	WHERE A.BAJA = 0
	AND  (FECHAFIN between CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),112))
			and DATEADD(DAY,45,CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),112))) )

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[AcuerdoAdd]
(
	@nroAcuerdo varchar(50),
	@idCliente int,
	@fechaInicio datetime = NULL,
	@fechaFin datetime = NULL,
	@fechaEmision datetime = NULL,
	@nroRevision int = NULL,
	@titulo varchar(250) = NULL,
	@tipoMoneda varchar(50) = NULL,
	@idSector int
)
AS
BEGIN


	INSERT
	INTO [Acuerdo]
	(
		[titulo],
		[nroAcuerdo],
		[idCliente],
		[fechaInicio],
		[fechaFin],
		[fechaEmision],
		[nroRevision],
		[tipoMoneda],
		[idSector]	
	
	)
	VALUES
	(
		@titulo,
		@nroAcuerdo,
		@idCliente,
		@fechaInicio,
		@fechaFin,
		@fechaEmision,
		@nroRevision,
		@tipoMoneda,
		@idSector
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoByConEsp]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoByConEsp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoByConEsp]
(
	@CONESP VARCHAR(1) 
)
AS

BEGIN

	SELECT idConcepto,
			Concepto,
			ConEsp
	FROM AcuerdoConcepto
	WHERE ConEsp = @CONESP or (@CONESP is null and ConEsp is null)
	ORDER BY orden

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetAnteriorById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetAnteriorById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoGetAnteriorById]
(	
	@idAcuerdo int
)
AS
BEGIN

SELECT [idAcuerdo],
		[nroAcuerdo],
		[titulo],
		[idCliente],
		[fechaInicio],
		[fechaFin],		
		[fechaEmision],
		[nroRevision],
		[tipoMoneda]
FROM ACUERDO
WHERE BAJA = 0
AND NROACUERDO =
(
	SELECT NROACUERDO
	FROM ACUERDO
	WHERE IDACUERDO = @idAcuerdo)
AND NROREVISION =(
	SELECT NROREVISION-1
	FROM ACUERDO
	WHERE IDACUERDO = @idAcuerdo)
	
	
	/*SELECT a.[idAcuerdo],
		a.[nroAcuerdo],
		a.[titulo],
		a.[idCliente],
		a.[fechaInicio],aAnt.[fechaInicio],
		a.[fechaFin],		
		a.[fechaEmision],
		a.[nroRevision],
		a.[tipoMoneda]
FROM ACUERDO a
inner join acuerdo aAnt on aAnt.idAcuerdo = 47 
							and aAnt.nroAcuerdo = a.nroAcuerdo 
							and a.nroRevision = (aAnt.nroRevision-1)
WHERE a.BAJA = 0*/
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[AcuerdoGetAll]
AS
BEGIN

	SELECT
		[idAcuerdo],
		[nroAcuerdo],
		[titulo],
		[idCliente],
		[fechaInicio],
		[fechaFin],
		[fechaEmision],
		[nroRevision],
		[tipoMoneda],
		[idSector]
	FROM [Acuerdo]
	WHERE BAJA = 0
	ORDER BY NROACUERDO, NROREVISION
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGestionGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoGestionGetByFilter]
	(
		@nroAcuerdo varchar,
		@fechaDesde datetime,
		@fechaHasta datetime,
		@idCliente int
	)
	
AS
BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		AMAX.NROREVISION,
		A.TITULO,
		s.Descripcion as Sector
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN Sector s on s.idSector = A.idSector
	INNER JOIN (SELECT MAX(IDACUERDO)AS IDACUERDO, MAX(ISNULL(NROREVISION, 1)) AS NROREVISION, NROACUERDO
				FROM ACUERDO
				GROUP BY NROACUERDO) AMAX ON  AMAX.IDACUERDO = A.IDACUERDO
	WHERE A.BAJA = 0
	AND A.FECHAFIN > CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),112))
	AND (A.IDCLIENTE = @IDCLIENTE OR @IDCLIENTE IS NULL)
	AND (A.FECHAINICIO >= @FECHADESDE OR @FECHADESDE IS NULL)
	AND (A.FECHAFIN >= @FECHAHASTA OR @FECHAHASTA IS NULL)
	AND (A.NROACUERDO like ''%'' + @NROACUERDO  + ''%'' OR @NROACUERDO IS NULL)
	
	ORDER BY a.nroacuerdo , a.nrorevision desc, A.FECHAFIN DESC, A.FECHAINICIO DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDetalleGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoDetalleGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoDetalleGetById]
(
	@idAcuerdo int
)
AS

BEGIN

	
	select distinct ac.*,
			acc.idAcuerdoConcepto,
			acc.Concepto,
			acc.Costo,
			acc2.Costo as cant,
			ts.Descripcion as tipoServicio, 
			acc.idEspecificacion,
			et.Descripcion as especificacion,
			v.idVehiculo,
			vi.NroCodificacion + '' '' + c.Codigo as codificacion
	from AcuerdoServicio ac
	inner join TipoServicio ts on ts.idTipoServicio = ac.idTipoServicio
	inner join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = ac.idAcuerdoServicio and acc.Concepto not like ''%Cant%''
	inner join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like ''%Cant%'' 
	inner join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and acc2.idEspecificacion = et.idEspecificaciones
	inner join EspecificacionDetalle ed on ed.idEspecificacion = acc.idEspecificacion and acc2.idEspecificacion = et.idEspecificaciones
	inner join Vehiculo v on v.idVehiculo = ed.idEquipoPropio
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	inner join Acuerdo a on a.idAcuerdo = ac.idAcuerdo
	where ac.idAcuerdo = @idAcuerdo and a.baja = 0
	and acc.Concepto like ''%Mensual%''
	and acc2.Concepto like ''%Mensual%''
	and v.idVehiculo not in(select AT2.idTrailer from AcuerdoConceptoTrailer AT2
							INNER JOIN AcuerdoConceptoCosto ACC2 ON ACC2.idAcuerdoConcepto = AT2.idAcuerdoConcepto
							INNER JOIN AcuerdoServicio ASE2 ON ASE2.idAcuerdoServicio = ACC2.idAcuerdoServicio
							INNER JOIN Acuerdo A2 ON A2.idAcuerdo = ASE2.idAcuerdo
							where A2.idCliente <> a.idCliente and A2.baja = 0)


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoDelete]
(
	@idAcuerdo int
)
AS
BEGIN

	DELETE
	FROM [Acuerdo]
	WHERE
		[idAcuerdo] = @idAcuerdo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerUpdate]
(
	@idAcuerdoConceptoTrailer int,
	@idAcuerdoConcepto int,
	@idTrailer int
)
AS
BEGIN

	UPDATE [AcuerdoConceptoTrailer]
	SET
		[idAcuerdoConcepto] = @idAcuerdoConcepto,
		[idTrailer] = @idTrailer
	WHERE
		[idAcuerdoConceptoTrailer] = @idAcuerdoConceptoTrailer




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerGetByIdAcuerdo]
(
@IDACUERDO INT
)
AS
--declare @IDCLIETE INT= 40
BEGIN

	SELECT DISTINCT
		AT.[idAcuerdoConceptoTrailer],
		AT.[idAcuerdoConcepto],
		AT.[idTrailer],
		vi.NroCodificacion + '' '' + c.Codigo AS Trailer,
		v.idVehiculo,
		TS.Descripcion AS TIPOSERVICIO,
		TS.idTipoServicio,
		ET.Descripcion AS ESPECIFICACION
		
	FROM [AcuerdoConceptoTrailer] AT
	INNER JOIN AcuerdoConceptoCosto ACC ON ACC.idAcuerdoConcepto = AT.idAcuerdoConcepto
	INNER JOIN AcuerdoServicio ASE ON ASE.idAcuerdoServicio = ACC.idAcuerdoServicio
	INNER JOIN Acuerdo A ON A.idAcuerdo = ASE.idAcuerdo
	INNER JOIN Vehiculo v ON v.idVehiculo = AT.idTrailer
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion =vi.idCodificacion
	INNER JOIN TipoServicio TS ON TS.idTipoServicio = ASE.idTipoServicio
	INNER JOIN EspecificacionTecnica ET ON ET.idEspecificaciones = ACC.idEspecificacion
	WHERE A.idAcuerdo = @IDACUERDO
	AND AT.idTrailer not in(select idTrailer from AcuerdoConceptoTrailer AT2
							INNER JOIN AcuerdoConceptoCosto ACC2 ON ACC2.idAcuerdoConcepto = AT2.idAcuerdoConcepto
							INNER JOIN AcuerdoServicio ASE2 ON ASE2.idAcuerdoServicio = ACC2.idAcuerdoServicio
							INNER JOIN Acuerdo A2 ON A2.idAcuerdo = ASE2.idAcuerdo
							where A2.idCliente <> A.idCliente and A2.baja = 0)
							

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerGetById]
(
	@idAcuerdoConceptoTrailer int
)
AS
BEGIN

	SELECT
		[idAcuerdoConceptoTrailer],
		[idAcuerdoConcepto],
		[idTrailer]
	FROM [AcuerdoConceptoTrailer]
	WHERE
		([idAcuerdoConceptoTrailer] = @idAcuerdoConceptoTrailer)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerGetAll]
AS
BEGIN

	SELECT
		[idAcuerdoConceptoTrailer],
		[idAcuerdoConcepto],
		[idTrailer]
	FROM [AcuerdoConceptoTrailer]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerDelete]
(
	@idAcuerdoConceptoTrailer int
)
AS
BEGIN

	DELETE
	FROM [AcuerdoConceptoTrailer]
	WHERE
		[idAcuerdoConceptoTrailer] = @idAcuerdoConceptoTrailer
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoTrailerAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoTrailerAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoTrailerAdd]
(
	@idAcuerdoConcepto int,
	@idTrailer int
)
AS
BEGIN


	INSERT
	INTO [AcuerdoConceptoTrailer]
	(
		[idAcuerdoConcepto],
		[idTrailer]
	)
	VALUES
	(
		@idAcuerdoConcepto,
		@idTrailer
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CategoriaCarnetUpdate]
(
	@idCarnet int,
	@descripcion varchar(50)
)
AS
BEGIN

	UPDATE [CategoriaCarnet]
	SET
		[descripcion] = @descripcion
	WHERE
		[idCarnet] = @idCarnet




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CategoriaCarnetGetById]
(
	@idCarnet int
)
AS
BEGIN

	SELECT
		[idCarnet],
		[descripcion]
	FROM [CategoriaCarnet]
	WHERE
		([idCarnet] = @idCarnet)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CategoriaCarnetGetAll]
AS
BEGIN

	SELECT
		[idCarnet],
		[descripcion]
	FROM [CategoriaCarnet]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CategoriaCarnetDelete]
(
	@idCarnet int
)
AS
BEGIN

	DELETE
	FROM [CategoriaCarnet]
	WHERE
		[idCarnet] = @idCarnet
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CategoriaCarnetAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CategoriaCarnetAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CategoriaCarnetAdd]
(
	@idCarnet int,
	@descripcion varchar(50)
)
AS
BEGIN


	INSERT
	INTO [CategoriaCarnet]
	(
		[idCarnet],
		[descripcion]
	)
	VALUES
	(
		@idCarnet,
		@descripcion
	)


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoVerificarFechas]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoVerificarFechas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoVerificarFechas]
	(		
		@fechaDesde datetime,
		@fechaHasta datetime,
		@idCliente int,
		@idSector int
	)
	
AS
BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		AMAX.NROREVISION,
		A.TITULO
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN (SELECT MAX(IDACUERDO)AS IDACUERDO, MAX(ISNULL(NROREVISION, 1)) AS NROREVISION, NROACUERDO
				FROM ACUERDO
				GROUP BY NROACUERDO) AMAX ON  AMAX.IDACUERDO = A.IDACUERDO
	WHERE A.BAJA = 0	
	AND (A.IDCLIENTE = @IDCLIENTE OR @IDCLIENTE IS NULL) 
	AND (A.idSector = @idSector or @idSector is null) and (
		(fechainicio between @fechaDesde and @fechaHasta) or
		(fechafin between @fechaDesde and @fechaHasta ) or 
		(fechainicio <= @fechaDesde and fechafin > @fechaHasta ) or 
		(fechainicio >= @fechaDesde and fechafin < @fechaHasta )
    )
	

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoUpdate]
(
	@idAcuerdo int,
	@nroAcuerdo varchar(50),
	@idCliente int,
	@fechaInicio datetime,
	@fechaFin datetime,
	@baja bit,
	@fechaEmision datetime = NULL,
	@nroRevision int = NULL,
	@titulo varchar(250) = NULL,
	@tipoMoneda varchar(50) = null,
	@idSector int
)
AS
BEGIN

	UPDATE [Acuerdo]
	SET
		[titulo] = @titulo,
		[nroAcuerdo] = @nroAcuerdo,
		[idCliente] = @idCliente,
		[fechaInicio] = @fechaInicio,
		[fechaFin] = @fechaFin,
		[baja] = @baja,
		[fechaEmision] = @fechaEmision,
		[nroRevision] = @nroRevision,
		[tipoMoneda] = @tipoMoneda,
		[idSector] = @idSector
	WHERE
		[idAcuerdo] = @idAcuerdo




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloUpdate]
(
	@idAcuerdoTitulo int,
	@titulo varchar(250),
	@detalle varchar(MAX),
	@idAcuerdo int
)
AS
BEGIN

	UPDATE [AcuerdoTitulo]
	SET
		[titulo] = @titulo,
		[detalle] = @detalle,
		[idAcuerdo] = @idAcuerdo
	WHERE
		[idAcuerdoTitulo] = @idAcuerdoTitulo




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetByIdAcuerdo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloGetByIdAcuerdo]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT DISTINCT
		AT.[idAcuerdoTitulo],
		AT.[titulo],
		MIN(DBO.GETDIFF(ATANT.[titulo],AT.[titulo])) AS TITULODIF,		
		AT.[detalle],
		MIN(DBO.GETDIFF(ATANT.[detalle],AT.[detalle])) AS DETALLEDIF,		
		AT.[idAcuerdo]
	FROM [AcuerdoTitulo]  AT
	INNER JOIN ACUERDO A ON A.IDACUERDO = AT.IDACUERDO
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0
	LEFT JOIN ACUERDOTITULO ATANT ON ATANT.IDACUERDO = AANT.IDACUERDO
	WHERE
		(AT.[idAcuerdo] = @idAcuerdo) and AT.titulo <> ''header''
	GROUP BY AT.[idAcuerdoTitulo],
		AT.[titulo],
		AT.[detalle],
		AT.[idAcuerdo]

END

/*
SELECT * FROM [AcuerdoTitulo] WHERE IDACUERDO = 49
--SELECT * FROM ACUERDO  WHERE IDCLIENTE = 9
SELECT * FROM [AcuerdoTitulo] WHERE IDACUERDO = 45
*/
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloGetById]
(
	@idAcuerdoTitulo int
)
AS
BEGIN

	SELECT
		[idAcuerdoTitulo],
		[titulo],
		[detalle],
		[idAcuerdo]
	FROM [AcuerdoTitulo]
	WHERE
		([idAcuerdoTitulo] = @idAcuerdoTitulo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloGetAll]
AS
BEGIN

	SELECT
		[idAcuerdoTitulo],
		[titulo],
		[detalle],
		[idAcuerdo]
	FROM [AcuerdoTitulo]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloDelete]
(
	@idAcuerdoTitulo int
)
AS
BEGIN

	DELETE
	FROM [AcuerdoTitulo]
	WHERE
		[idAcuerdoTitulo] = @idAcuerdoTitulo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoTituloAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoTituloAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoTituloAdd]
(
	@titulo varchar(250),
	@detalle varchar(MAX) = NULL,
	@idAcuerdo int
)
AS
BEGIN


	INSERT
	INTO [AcuerdoTitulo]
	(
		[titulo],
		[detalle],
		[idAcuerdo]
	)
	VALUES
	(
		@titulo,
		@detalle,
		@idAcuerdo
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoHerderGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoHerderGetByIdAcuerdo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoHerderGetByIdAcuerdo]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT DISTINCT
		AT.[idAcuerdoTitulo],
		AT.[titulo],
		MIN(DBO.GETDIFF(ATANT.[titulo],AT.[titulo])) AS TITULODIF,		
		AT.[detalle],
		MIN(DBO.GETDIFF(ATANT.[detalle],AT.[detalle])) AS DETALLEDIF,		
		AT.[idAcuerdo]
	FROM [AcuerdoTitulo]  AT
	INNER JOIN ACUERDO A ON A.IDACUERDO = AT.IDACUERDO
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0
	LEFT JOIN ACUERDOTITULO ATANT ON ATANT.IDACUERDO = AANT.IDACUERDO
	WHERE
		(AT.[idAcuerdo] = @idAcuerdo) and AT.titulo = ''header''
	GROUP BY AT.[idAcuerdoTitulo],
		AT.[titulo],
		AT.[detalle],
		AT.[idAcuerdo]

END

/*
SELECT * FROM [AcuerdoTitulo] WHERE IDACUERDO = 49
--SELECT * FROM ACUERDO  WHERE IDCLIENTE = 9
SELECT * FROM [AcuerdoTitulo] WHERE IDACUERDO = 45
*/
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetByIdAcuerdo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetByIdAcuerdo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioGetByIdAcuerdo]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT distinct
		AC.[idAcuerdoServicio],
		AC.[idAcuerdo],
		AC.[idTipoServicio],
		ATANT.[idTipoServicio] AS IDTIPOSERVICIODIF,
		TS.DESCRIPCION AS SERVICIO,
		ac.Requiere
	FROM [AcuerdoServicio] AC
	INNER JOIN TIPOSERVICIO TS ON TS.IDTIPOSERVICIO = AC.IDTIPOSERVICIO
	INNER JOIN ACUERDO A ON A.IDACUERDO = AC.IDACUERDO
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and A.idCliente = AANT.idCliente
	LEFT JOIN ACUERDOSERVICIO ATANT ON ATANT.IDACUERDO = AANT.IDACUERDO AND ATANT.IDTIPOSERVICIO = AC.IDTIPOSERVICIO
	WHERE
		(AC.[idAcuerdo] = @idAcuerdo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioGetById]
(
	@idAcuerdoServicio int
)
AS
BEGIN

	SELECT
		[idAcuerdoServicio],
		[idAcuerdo],
		[idTipoServicio],
		Requiere
	FROM [AcuerdoServicio]
	WHERE
		([idAcuerdoServicio] = @idAcuerdoServicio)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioGetAll]
AS
BEGIN

	SELECT
		[idAcuerdoServicio],
		[idAcuerdo],
		[idTipoServicio],
		Requiere
	FROM [AcuerdoServicio]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioDelete]
(
	@idAcuerdoServicio int
)
AS
BEGIN

	DELETE
	FROM [AcuerdoServicio]
	WHERE
		[idAcuerdoServicio] = @idAcuerdoServicio
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioAdd]
(
	@idAcuerdo int,
	@idTipoServicio int,
	@requiere varchar(1)
)
AS
BEGIN


	INSERT
	INTO [AcuerdoServicio]
	(
		[idAcuerdo],
		[idTipoServicio],
		[Requiere]
	)
	VALUES
	(
		@idAcuerdo,
		@idTipoServicio,
		@requiere
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[AcuerdoGetById]
(
	@idAcuerdo int
)
AS
BEGIN

	SELECT DISTINCT
		A.[idAcuerdo],
		A.[nroAcuerdo],
		A.[titulo],
		DBO.GETDIFF(A.[titulo],AANT.[titulo]) AS TITULODIF,
		A.[idCliente],
		E.RAZONSOCIAL,
		A.[fechaInicio],
		A.[fechaFin],
		A.[fechaEmision],
		A.[nroRevision],
		A.[tipoMoneda],
		AT.detalle as header,
		A.idSector,
		s.Descripcion as Sector
	FROM [Acuerdo] A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = E.idEmpresa
	LEFT JOIN AcuerdoTitulo AT on AT.idAcuerdo = A.idAcuerdo and AT.titulo = ''header''
	left join Sector s on s.idSector = a.idSector
	WHERE
		(A.[idAcuerdo] = @idAcuerdo)
		AND A.baja = 0

END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoGetByFilter]
	(
		@nroAcuerdo varchar(150),
		@fechaDesde datetime,
		@fechaHasta datetime,
		@idCliente int
	)
	
AS
BEGIN

	SELECT A.IDACUERDO,
		A.NROACUERDO,		
		CONVERT(VARCHAR(10),A.FECHAINICIO, 103) AS FECHAINICIO,
		CONVERT(VARCHAR(10),A.FECHAFIN, 103) AS FECHAFIN,		
		E.DESCRIPCION AS CLIENTE,
		CONVERT(VARCHAR(10),A.FECHAEMISION, 103) AS FECHAEMISION,
		ISNULL(A.NROREVISION, 1) AS NROREVISION,
		A.TITULO,
		S.Descripcion AS SECTOR
		
	FROM ACUERDO A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	INNER JOIN Sector S ON S.idSector = A.idSector
	WHERE A.BAJA = 0
	AND (A.IDCLIENTE = @IDCLIENTE OR @IDCLIENTE IS NULL)
	AND (A.FECHAINICIO >= @FECHADESDE OR @FECHADESDE IS NULL)
	AND (A.FECHAFIN >= @FECHAHASTA OR @FECHAHASTA IS NULL)
	AND ((A.NROACUERDO like + ''%'' + @NROACUERDO  + ''%'')  OR @NROACUERDO IS NULL)
	
	ORDER BY a.nroacuerdo , a.nrorevision desc, A.FECHAFIN DESC, A.FECHAINICIO DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaUpdate]
(
	@idDiagrama int,
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN

	UPDATE [Diagrama]
	SET
		[Descripcion] = @Descripcion,
		[Baja] = @Baja
	WHERE
		[idDiagrama] = @idDiagrama




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaGetById]
(
	@idDiagrama int
)
AS
BEGIN

	SELECT
		[idDiagrama],
		[Descripcion],
		[Baja]
	FROM [Diagrama]
	WHERE
		([idDiagrama] = @idDiagrama)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaGetAllBaja]
AS
BEGIN

	SELECT
		[idDiagrama],
		[Descripcion],
		[Baja]
	FROM [Diagrama]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaGetAll]
AS
BEGIN

	SELECT
		[idDiagrama],
		[Descripcion],
		[Baja]
	FROM [Diagrama]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaDelete]
(
	@idDiagrama int
)
AS
BEGIN

	DELETE
	FROM [Diagrama]
	WHERE
		[idDiagrama] = @idDiagrama
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DiagramaAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiagramaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DiagramaAdd]
(
	@Descripcion varchar(250),
	@Baja bit
)
AS
BEGIN


	INSERT
	INTO [Diagrama]
	(
		[Descripcion],
		[Baja]
	)
	VALUES
	(
		@Descripcion,
		@Baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DepartamentoUpdate]
(
	@idDepartamento int,
	@idProvincia int,
	@Nombre nvarchar(250)
)
AS
BEGIN

	UPDATE [Departamento]
	SET
		[idProvincia] = @idProvincia,
		[Nombre] = @Nombre
	WHERE
		[idDepartamento] = @idDepartamento




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DepartamentoGetById]
(
	@idDepartamento int
)
AS
BEGIN

	SELECT
		[idDepartamento],
		[idProvincia],
		[Nombre]
	FROM [Departamento]
	WHERE
		([idDepartamento] = @idDepartamento)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DepartamentoGetAll]
AS
BEGIN

	SELECT
		[idDepartamento],
		[idProvincia],
		[Nombre]
	FROM [Departamento]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DepartamentoDelete]
(
	@idDepartamento int
)
AS
BEGIN

	DELETE
	FROM [Departamento]
	WHERE
		[idDepartamento] = @idDepartamento
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[DepartamentoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepartamentoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DepartamentoAdd]
(
	@idProvincia int,
	@Nombre nvarchar(250)
)
AS
BEGIN


	INSERT
	INTO [Departamento]
	(
		[idProvincia],
		[Nombre]
	)
	VALUES
	(
		@idProvincia,
		@Nombre
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferUpdate]
(
	@idChofer int,
	@Nombre varchar(250),
	@Apellido varchar(250),
	@Direccion varchar(250),
	@Telefono varchar(50),
	@DNI varchar(50)
)
AS
BEGIN

	UPDATE [Chofer]
	SET
		[Nombre] = @Nombre,
		[Apellido] = @Apellido,
		[Direccion] = @Direccion,
		[Telefono] = @Telefono,
		[DNI] = @DNI
	WHERE
		[idChofer] = @idChofer




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferGetById]
(
	@idChofer int
)
AS
BEGIN

	SELECT
		[idChofer],
		[Nombre],
		[Apellido],
		[Direccion],
		[Telefono],
		[DNI]
	FROM [Chofer]
	WHERE
		([idChofer] = @idChofer)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferGetAllBaja]
AS
BEGIN

	SELECT
		[idChofer],
		[Nombre],
		[Apellido],
		[Direccion],
		[Telefono],
		[DNI]
	FROM [Chofer]
	where baja = 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferGetAll]
AS
BEGIN

	SELECT
		[idChofer],
		[Nombre],
		[Apellido],
		[Direccion],
		[Telefono],
		[DNI]
	FROM [Chofer]
	where isnull(baja ,0) = 0
        ORDER BY APELLIDO, NOMBRE
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferDelete]
(
	@idChofer int
)
AS
BEGIN

	UPDATE [Chofer]
	SET BAJA = 1
	WHERE
		[idChofer] = @idChofer
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChoferAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChoferAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ChoferAdd]
(
	@Nombre varchar(250) = NULL,
	@Apellido varchar(250) = NULL,
	@Direccion varchar(250) = NULL,
	@Telefono varchar(50) = NULL,
	@DNI varchar(50) = NULL
)
AS
BEGIN


	INSERT
	INTO [Chofer]
	(
		[Nombre],
		[Apellido],
		[Direccion],
		[Telefono],
		[DNI]
	)
	VALUES
	(
		@Nombre,
		@Apellido,
		@Direccion,
		@Telefono,
		@DNI
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaUpdate]
(
	@idCuadrilla int,
	@Nombre varchar(150),
	@Baja bit,
	@idUsuario int,
	@FechaAlta datetime,
	@idSector int,
	@idEmpresa int
)
AS
BEGIN

	UPDATE [Cuadrilla]
	SET
		[Nombre] = @Nombre,
		[Baja] = @Baja,
		[idUsuario] = @idUsuario,
		[FechaAlta] = @FechaAlta,
		idSector = @idSector,
		idEmpresa = @idEmpresa
	WHERE
		[idCuadrilla] = @idCuadrilla




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalUpdate]
(
	@idCuadrillaPersonal int,
	@idCuadrilla int,
	@idPersonal int,
	@FechaActivo datetime,
	@Activo bit,
	@FechaBaja datetime
)
AS
BEGIN

	UPDATE [CuadrillaPersonal]
	SET
		[idCuadrilla] = @idCuadrilla,
		[idPersonal] = @idPersonal,
		[FechaActivo] = @FechaActivo,
		[Activo] = @Activo,
		[FechaBaja] = @FechaBaja
	WHERE
		[idCuadrillaPersonal] = @idCuadrillaPersonal




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetByIdCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetByIdCuadrilla]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalGetByIdCuadrilla]
(
	@idCuadrilla int
)
AS
BEGIN

	
	SELECT
		cp.[idCuadrillaPersonal],
		cp.[idCuadrilla],
		cp.[idPersonal],
		cp.[FechaActivo],
		cp.[Activo],
		cp.[FechaBaja]
	FROM [CuadrillaPersonal] cp 
	inner join ( SELECT idPersonal, max(FechaActivo) as fecha
					FROM CuadrillaPersonal
					where idCuadrilla=@idCuadrilla
					group by idPersonal) pp on pp.idPersonal = cp.idPersonal and cp.FechaActivo = pp.fecha
	inner join Personal p on p.idPersonal = cp.idPersonal
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	WHERE
		(cp.[idCuadrilla] = @idCuadrillA)
		--and pc.VtoCarnet < GETDATE()
		--and Activo = 1
	order by FechaActivo desc

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalGetById]
(
	@idCuadrillaPersonal int
)
AS
BEGIN

	SELECT
		[idCuadrillaPersonal],
		[idCuadrilla],
		[idPersonal],
		[FechaActivo],
		[Activo],
		[FechaBaja]
	FROM [CuadrillaPersonal]
	WHERE
		([idCuadrillaPersonal] = @idCuadrillaPersonal)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalGetByFilter]
(
	@idCuadrillaPersonal int,
	@idCuadrilla int,
	@idPersonal int,
	@FechaActivo datetime,
	@Activo bit,
	@FechaBaja datetime
)
AS
BEGIN

	SELECT
		[idCuadrillaPersonal],
		[idCuadrilla],
		[idPersonal],
		[FechaActivo],
		[Activo],
		[FechaBaja]
	FROM [CuadrillaPersonal]
	WHERE
		([idCuadrillaPersonal] = @idCuadrillaPersonal OR @idCuadrillaPersonal IS NULL) AND
		([idCuadrilla] = @idCuadrilla OR @idCuadrilla IS NULL) AND
		([idPersonal] = @idPersonal OR @idPersonal IS NULL) AND
		([FechaActivo] = @FechaActivo OR @FechaActivo IS NULL) AND
		([Activo] = @Activo OR @Activo IS NULL) AND
		([FechaBaja] = @FechaBaja OR @FechaBaja IS NULL)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalGetAll]
AS
BEGIN

	SELECT
		[idCuadrillaPersonal],
		[idCuadrilla],
		[idPersonal],
		[FechaActivo],
		[Activo],
		[FechaBaja]
	FROM [CuadrillaPersonal]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalDeleteByIdCuadrilla]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalDeleteByIdCuadrilla]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalDeleteByIdCuadrilla]
(
	@idCuadrilla int
)
AS
BEGIN

	DELETE
	FROM [CuadrillaPersonal]
	WHERE
		[idCuadrilla] = @idCuadrilla
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalDelete]
(
	@idCuadrillaPersonal int
)
AS
BEGIN

	DELETE
	FROM [CuadrillaPersonal]
	WHERE
		[idCuadrillaPersonal] = @idCuadrillaPersonal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaPersonalAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaPersonalAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaPersonalAdd]
(
	@idCuadrilla int,
	@idPersonal int,
	@FechaActivo datetime = NULL,
	@Activo bit = NULL,
	@FechaBaja datetime = NULL
)
AS
BEGIN


	INSERT
	INTO [CuadrillaPersonal]
	(
		[idCuadrilla],
		[idPersonal],
		[FechaActivo],
		[Activo],
		[FechaBaja]
	)
	VALUES
	(
		@idCuadrilla,
		@idPersonal,
		@FechaActivo,
		@Activo,
		@FechaBaja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaGetById]
(
	@idCuadrilla int
)
AS
BEGIN

	SELECT
		[idCuadrilla],
		[Nombre],
		[Baja],
		[idUsuario],
		[FechaAlta],
		idSector,
		idEmpresa
	FROM [Cuadrilla]
	WHERE
		([idCuadrilla] = @idCuadrilla)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaGetByFilter]
(
	@Nombre varchar(150)
)
AS
BEGIN

	SELECT
		[idCuadrilla],
		[Nombre],
		[Baja],
		[idUsuario],
		[FechaAlta]
	FROM [Cuadrilla]
	WHERE
		([Nombre] like ''%''+ @Nombre + ''%'' OR @Nombre IS NULL) 
		
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetAllBaja]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaGetAllBaja]
AS
BEGIN

	SELECT
		[idCuadrilla],
		[Nombre],
		[Baja],
		[idUsuario],
		[FechaAlta],
		idSector,
		idEmpresa
	FROM [Cuadrilla]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaGetAll]
AS
BEGIN

	SELECT
		[idCuadrilla],
		[Nombre],
		[Baja],
		[idUsuario],
		[FechaAlta],
		idSector,
		idEmpresa
	FROM [Cuadrilla]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaGestionGetByFilter]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaGestionGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*PANTALLA GESTION CUADRILLA
- NO SE PUEDE MODIFICAR
- SOLO DE INFORMACION
- QUE SE PUEDA EXPORTAR A EXCEL
*/
CREATE PROCEDURE [dbo].[CuadrillaGestionGetByFilter]
(
		@desde datetime,
		@hasta datetime,
		@idPersona int,
		@idCuadrilla int,
		@idSector int,
		@idEmpresa int
		
	)
	AS
	BEGIN
		SELECT  c.idCuadrilla
				,c.Nombre as Cuadrilla
				,p.Apellido + '', '' + p.Nombres as Persona
				, s.Descripcion as Sector
				,convert(varchar(10),cp.FechaActivo,103) as FechaInicio
				,convert(varchar(10),cp.FechaBaja,103)  as FechaDesafectacion
		FROM Cuadrilla c
		inner join CuadrillaPersonal cp on cp.idCuadrilla = c.idCuadrilla 
		inner join Personal p on p.idPersonal= cp.idPersonal
		inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
		inner join Sector s on s.idSector = pl.idSector
		inner join Empresa e on e.idEmpresa = pl.idEmpresa
		WHERE
			(e.idEmpresa = @idEmpresa or @idEmpresa IS NULL)
		AND (s.idSector = @idSector OR @idSector IS NULL)
		AND (p.idPersonal = @idPersona OR @idPersona IS NULL)
		AND (cp.idCuadrilla = @idCuadrilla OR @idCuadrilla IS NULL) 

		order by Persona
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaDelete]
(
	@idCuadrilla int
)
AS
BEGIN

	DELETE
	FROM [Cuadrilla]
	WHERE
		[idCuadrilla] = @idCuadrilla
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CuadrillaAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuadrillaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CuadrillaAdd]
(
	@Nombre varchar(150) = NULL,
	@Baja bit = NULL,
	@idUsuario int = NULL,
	@FechaAlta datetime = NULL,
	@idSector int,
	@idEmpresa int
)
AS
BEGIN


	INSERT
	INTO [Cuadrilla]
	(
		[Nombre],
		[Baja],
		[idUsuario],
		[FechaAlta],
		idSector,
		idEmpresa
	)
	VALUES
	(
		@Nombre,
		@Baja,
		@idUsuario,
		@FechaAlta,
		@idSector,
		@idEmpresa
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CondicionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CondicionUpdate]
(
	@idCondicion int,
	@Descripcion nvarchar(150)
)
AS
BEGIN

	UPDATE [Condicion]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idCondicion] = @idCondicion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CondicionGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CondicionGetById]
(
	@idCondicion int
)
AS
BEGIN

	SELECT
		[idCondicion],
		[Descripcion]
	FROM [Condicion]
	WHERE
		([idCondicion] = @idCondicion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CondicionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CondicionGetAll]
AS
BEGIN

	SELECT
		[idCondicion],
		[Descripcion]
	FROM [Condicion]
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CondicionDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CondicionDelete]
(
	@idCondicion int
)
AS
BEGIN

	DELETE
	FROM [Condicion]
	WHERE
		[idCondicion] = @idCondicion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CondicionAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CondicionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CondicionAdd]
(
	@Descripcion nvarchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Condicion]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionUpdate]
(
	@idCodificacion int,
	@Descripcion varchar(200),
	@Codigo varchar(25),
	@Equipo bit
)
AS
BEGIN

	UPDATE [Codificacion]
	SET
		[Descripcion] = @Descripcion,
		[Codigo] = @Codigo,
		[Equipo] = @Equipo
	WHERE
		[idCodificacion] = @idCodificacion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetVehiculo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionGetVehiculo]
AS
BEGIN

	SELECT
		[idCodificacion],
		[Descripcion],
		[Codigo],
		[Equipo]
	FROM [Codificacion]
	WHERE
		[Equipo] = 0;
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetEquipo]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetEquipo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionGetEquipo]
AS
BEGIN

	SELECT
		[idCodificacion],
		[Descripcion],
		[Codigo],
		[Equipo]
	FROM [Codificacion]
	WHERE
		[Equipo] = 1;
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionGetById]
(
	@idCodificacion int
)
AS
BEGIN

	SELECT
		[idCodificacion],
		[Descripcion],
		[Codigo],
		[Equipo]
	FROM [Codificacion]
	WHERE
		([idCodificacion] = @idCodificacion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionGetAll]
AS
BEGIN

	SELECT
		[idCodificacion],
		[Descripcion],
		[Codigo],
		[Equipo]
	FROM [Codificacion]
	Order by Codigo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionDelete]
(
	@idCodificacion int
)
AS
BEGIN

	DELETE
	FROM [Codificacion]
	WHERE
		[idCodificacion] = @idCodificacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CodificacionAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodificacionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CodificacionAdd]
(
	@Descripcion varchar(200),
	@Codigo varchar(25),
	@Equipo bit
)
AS
BEGIN


	INSERT
	INTO [Codificacion]
	(
		[Descripcion],
		[Codigo],
		[Equipo]
	)
	VALUES
	(
		@Descripcion,
		@Codigo,
		@Equipo
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BancoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[BancoUpdate]
(
	@idBanco int,
	@Descripcion varchar(250)
)
AS
BEGIN

	UPDATE [Banco]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idBanco] = @idBanco




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BancoGetById]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[BancoGetById]
(
	@idBanco int
)
AS
BEGIN

	SELECT
		[idBanco],
		[Descripcion]
	FROM [Banco]
	WHERE
		([idBanco] = @idBanco)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BancoGetAll]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[BancoGetAll]
AS
BEGIN

	SELECT
		[idBanco],
		[Descripcion]
	FROM [Banco]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BancoDelete]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[BancoDelete]
(
	@idBanco int
)
AS
BEGIN

	DELETE
	FROM [Banco]
	WHERE
		[idBanco] = @idBanco
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BancoAdd]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BancoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[BancoAdd]
(
	@Descripcion varchar(250)
)
AS
BEGIN


	INSERT
	INTO [Banco]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosAnuladosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosAnuladosGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosAnuladosGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer int = null,
	@idSector int,
	@idTipoServicio int,
	@idCondicion int
		
)
AS
BEGIN

SELECT	
		SC.idSolicitud,
		str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud) as NroSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		ISNULL(PE.NroParte,0) as NroParte,
		ISNULL(R.nroRemito,0) as nroRemito,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		ISNULL(OT.idOrden,0) as idOrden,
		ISNULL(OT.idTrailer,0) as idTrailer, 
		e.Descripcion AS EMPRESA,
		sec.Descripcion as Sector,
		ts.Descripcion as TipoServicio,
		c.Codigo + '' '' + vi.NroCodificacion as Trailer,
		isnull(R.idRemito,0) as idRemito,
		isnull(PE.idParteEntrega,0) as idParte,
		isnull(impreso,0) as impreso,
		ISNULL(anulado,0) as anulado
		
FROM  OrdenTrabajo OT
INNER JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
INNER JOIN REMITOS R ON R.idParte = PE.idParteEntrega 
left join Vehiculo v on v.idVehiculo = OT.idTrailer
left join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
left join Codificacion c on c.idCodificacion = vi.idCodificacion

left join Sector sec on (sec.idSector = SC.idSector)
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0 AND R.anulado = 1
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)		
		AND (PE.NroParte = @nroParte or @nroParte is null)
		AND (v.idVehiculo = @idTRailer or @idTRailer is null)
		AND (SC.idSector = @idSector OR @idSector IS NULL)
		AND (SC.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
AND SC.Baja = 0
ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoNumeroUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoNumeroUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoNumeroUpdate]
	@nroRemito int
AS

BEGIN

UPDATE [SIG].[dbo].[RemitoNumero]
   SET [nroRemito] = @nroRemito

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoGetNroRemito]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoGetNroRemito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoGetNroRemito]

AS

BEGIN

	SELECT nroRemito
	FROM RemitoNumero

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleUpdate]
(
	@idRemitoDetalle int,
	@idRemito int,
	@Cantidad int,
	@Detalle varchar(500),
	@PrecioUnit decimal(18,2),
	@PrecioTotal decimal(18,2)
)
AS
BEGIN

	UPDATE [RemitoDetalle]
	SET
		[idRemito] = @idRemito,
		[Cantidad] = @Cantidad,
		[Detalle] = @Detalle,
		[PrecioUnit] = @PrecioUnit,
		[PrecioTotal] = @PrecioTotal
	WHERE
		[idRemitoDetalle] = @idRemitoDetalle




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleGetById]
(
	@idRemitoDetalle int
)
AS
BEGIN

	SELECT
		[idRemitoDetalle],
		[idRemito],
		[Cantidad],
		[Detalle],
		[PrecioUnit],
		[PrecioTotal]
	FROM [RemitoDetalle]
	WHERE
		([idRemitoDetalle] = @idRemitoDetalle)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleGetAll]
AS
BEGIN

	SELECT
		[idRemitoDetalle],
		[idRemito],
		[Cantidad],
		[Detalle],
		[PrecioUnit],
		[PrecioTotal]
	FROM [RemitoDetalle]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleDelete]
(
	@idRemitoDetalle int
)
AS
BEGIN

	DELETE
	FROM [RemitoDetalle]
	WHERE
		[idRemitoDetalle] = @idRemitoDetalle
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleAddGetByRemito]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleAddGetByRemito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleAddGetByRemito]
	@IDREMITO INT
AS
BEGIN

	SELECT
		[idRemitoDetalle],
		[idRemito],
		[Cantidad],
		[Detalle],
		[PrecioUnit],
		[PrecioTotal],
		[idparte]
	FROM [RemitoDetalle]
	WHERE idRemito = @IDREMITO
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoDetalleAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoDetalleAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoDetalleAdd]
(
	@idRemito int,
	@Cantidad int,
	@Detalle varchar(500) = NULL,
	@PrecioUnit decimal(18,2),
	@PrecioTotal decimal(18,2),
	@idParte int
)
AS
BEGIN


	INSERT
	INTO [RemitoDetalle]
	(
		[idRemito],
		[Cantidad],
		[Detalle],
		[PrecioUnit],
		[PrecioTotal],
		[idParte]
	)
	VALUES
	(
		@idRemito,
		@Cantidad,
		@Detalle,
		@PrecioUnit,
		@PrecioTotal,
		@idParte
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionUpdate]
(
	@idRelacion int,
	@Descripcion varchar(200),
	@Baja bit
)
AS
BEGIN

	UPDATE [Relacion]
	SET
		[Descripcion] = @Descripcion,
		[Baja] = @Baja
	WHERE
		[idRelacion] = @idRelacion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionGetById]
(
	@idRelacion int
)
AS
BEGIN

	SELECT
		[idRelacion],
		[Descripcion],
		[Baja]
	FROM [Relacion]
	WHERE
		([idRelacion] = @idRelacion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetAllBaja]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionGetAllBaja]
AS
BEGIN

	SELECT
		[idRelacion],
		[Descripcion],
		[Baja]
	FROM [Relacion]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionGetAll]
AS
BEGIN

	SELECT
		[idRelacion],
		[Descripcion],
		[Baja]
	FROM [Relacion]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionDelete]
(
	@idRelacion int
)
AS
BEGIN

	DELETE
	FROM [Relacion]
	WHERE
		[idRelacion] = @idRelacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RelacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RelacionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RelacionAdd]
(
	@Descripcion varchar(200),
	@Baja bit
)
AS
BEGIN


	INSERT
	INTO [Relacion]
	(
		[Descripcion],
		[Baja]
	)
	VALUES
	(
		@Descripcion,
		@Baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ProvinciaUpdate]
(
	@idProvincia int,
	@Nombre nvarchar(250),
	@idPais int
)
AS
BEGIN

	UPDATE [Provincia]
	SET
		[Nombre] = @Nombre,
		[idPais] = @idPais
	WHERE
		[idProvincia] = @idProvincia




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ProvinciaGetById]
(
	@idProvincia int
)
AS
BEGIN

	SELECT
		[idProvincia],
		[Nombre],
		[idPais]
	FROM [Provincia]
	WHERE
		([idProvincia] = @idProvincia)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ProvinciaGetAll]
AS
BEGIN

	SELECT
		[idProvincia],
		[Nombre],
		[idPais]
	FROM [Provincia]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ProvinciaDelete]
(
	@idProvincia int
)
AS
BEGIN

	DELETE
	FROM [Provincia]
	WHERE
		[idProvincia] = @idProvincia
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinciaAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProvinciaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ProvinciaAdd]
(
	@Nombre nvarchar(250),
	@idPais int
)
AS
BEGIN


	INSERT
	INTO [Provincia]
	(
		[Nombre],
		[idPais]
	)
	VALUES
	(
		@Nombre,
		@idPais
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalUpdate]
(
	@idPersonal int,
	@NroLegajo varchar(50),
	@Apellido varchar(100),
	@Nombres varchar(150),
	@FechaNacimiento smalldatetime,
	@idSexo int,
	@DNI int,
	@CUIL varchar(30),
	@idEstadoCivil int,
	@idEstudios int,
	@idNacionalidad int,
	@idLocalidad int,
	@Domicilio varchar(150),
	@Telefono varchar(50),
	@Mail varchar(50),
	@idUsuarioAcceso int,
	@idPersonalContacto int,
	@idPersonalDatosLaborales int,
	@idPersonalInformacionMedica int,
	@idPersonalConduccion int
)
AS
BEGIN

	UPDATE [Personal]
	SET
		[NroLegajo] = @NroLegajo,
		[Apellido] = @Apellido,
		[Nombres] = @Nombres,
		[FechaNacimiento] = @FechaNacimiento,
		[idSexo] = @idSexo,
		[DNI] = @DNI,
		[CUIL] = @CUIL,
		[idEstadoCivil] = @idEstadoCivil,
		[idEstudios] = @idEstudios,
		[idNacionalidad] = @idNacionalidad,
		[idLocalidad] = @idLocalidad,
		[Domicilio] = @Domicilio,
		[Telefono] = @Telefono,
		[Mail] = @Mail,
		[idUsuarioAcceso] = @idUsuarioAcceso,
		[idPersonalContacto] = @idPersonalContacto,
		[idPersonalDatosLaborales] = @idPersonalDatosLaborales,
		[idPersonalInformacionMedica] = @idPersonalInformacionMedica,
		[idPersonalConduccion] = @idPersonalConduccion,
		[baja] = 0
	WHERE
		[idPersonal] = @idPersonal




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalToXLS]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalToXLS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalToXLS]
	@idPersonal int
AS
BEGIN
SELECT
	NroLegajo AS [Legajo],
	p.Apellido AS [Apellido],
	p.Nombres AS [Nombres],
	convert(varchar,p.FechaNacimiento,103) AS [FechaNacimiento],
	SE.Descripcion AS [Sexo],
	p.DNI AS [DNI],
	p.CUIL AS [CUIL],
	EC.Descripcion AS [Estado Civil],
	ES.Descripcion AS [Estudios],
	NA.Descripcion AS [Nacionalidad],
	LO.Nombre AS [Localidad],
	p.Domicilio AS [Domicilio],
	p.Telefono AS [Telefono],
	p.Mail AS [Mail],
	isnull(EMP.Descripcion,'' '') AS [Empresa],
	isnull(convert(varchar,PDL.FechaAlta,103),'' '') AS [FechaAlta],
	isnull(RE.Descripcion,'' '') AS [Regimen],
	isnull(MO.Descripcion,'' '') AS [Modalidad],
	isnull(FU.Descripcion,'' '') AS [Funcion],
	isnull(SEC.Descripcion,'' '') AS [Sector],
	isnull(DIA.Descripcion,'' '') AS [Diagrama],
	isnull(BA.Descripcion,'' '') AS [Banco],
	isnull(PDL.cbu,0) AS [CBU],
	PC.Apellido AS [Apellido Emergencia],
	PC.Nombre AS [Nombre Emergencia],
	PC.Relacion AS [Relacion del Contacto],
	PC.Telefono AS [Telefono de Emergencia],
	CASE WHEN PCon.ManejaEmpresa = 1 THEN ''SI'' ELSE ''NO'' END AS [Maneja en Empresa],
	isnull(convert(varchar,PCon.VtoCarnet,103),'' '') AS [Vencimiento Carnet],
	CASE WHEN Pcon.CargarGenerales = 1 THEN ''SI'' ELSE ''NO'' END AS [Cargas Generales],
	isnull(convert(varchar,PCon.VtoCargasGenerales,103),'' '') AS [Vto Cargas Generales],
	CASE WHEN Pcon.CargarPeligrosas = 1 THEN ''SI'' ELSE ''NO'' END AS [Cargas Peligrosas],
	isnull(convert(varchar,PCon.VtoCargasPeligrosas,103),'' '') AS [Vto. Cargas Peligrosas],
	CASE WHEN PCon.ManejoDefensivo = 1 THEN ''SI'' ELSE ''NO'' END AS [Manejo Defensivo],
	isnull(convert(varchar,PCon.VtoManejoDefensivo,103),'' '') AS [Vto Manejo Defensivo],
	CASE WHEN PCon.Psicofisico = 1 THEN ''SI'' ELSE ''NO'' END AS [Psicofisico],
	isnull(convert(varchar,PCon.VtoPsicofisico,103),'' '') AS [Vto Psicofisico],
	PCon.CertCNRT AS [Certificado CNRT],
	PCon.Empresa AS [Empresa CNRT],
	PM.GrupoSanguineo AS [Grupo Sanguineo],
	PM.Factor AS [Factor Sanguineo],
	PM.Alergico AS [Alergias],
	PM.Observacion AS [Observacion Alergias],
	CASE WHEN PM.HapatitisB = 1 THEN ''SI'' ELSE ''NO'' END AS [Hepatitis B],
	CASE WHEN PM.Antitetanica = 1 THEN ''SI'' ELSE ''NO'' END AS [Antitetanica],
	CASE WHEN PM.ExPreocupacional = 1 THEN ''SI'' ELSE ''NO'' END AS [Examen Preocupacional],
	isnull(convert(varchar,PM.FechaPreocupacional,103),'' '') AS [Fecha Preocupacional],
	CASE WHEN PM.AprobacionPreocupacional = 1 THEN ''SI'' ELSE ''NO'' END AS [Aprobo Preocupacional],
	CASE WHEN PM.ExPeriodico = 1 THEN ''SI'' ELSE ''NO'' END AS [Examen Periodico],
	isnull(convert(varchar,PM.FechaPeriodico,103),'' '') AS [Fecha Ex Periodico],
	CASE WHEN PM.AprobacionPeriodico = ''True'' THEN ''SI'' ELSE ''NO'' END AS [Aprobacion Periodico]
	
FROM
	Personal p
	LEFT JOIN PersonalDatosLaborales PDL ON p.idPersonalDatosLaborales = PDL.idPersonalDatosLaborales
	LEFT JOIN PersonalContacto PC ON p.idPersonalContacto = PC.idPersonalContacto
	LEFT JOIN PersonalConduccion PCon ON p.idPersonalConduccion = PCon.idPersonalConduccion
	LEFT JOIN PersonalInformacionMedica PM ON p.idPersonalInformacionMedica = PM.idPersonalInformacionMedica
	LEFT JOIN EstadoCivil EC ON p.idEstadoCivil = EC.idEstadoCivil
	LEFT JOIN Estudios ES ON p.idEstudios = ES.idEstudio
	LEFT JOIN Sexo SE ON p.idSexo = SE.idSexo
	LEFT JOIN Localidad LO ON p.idLocalidad = LO.idLocalidad
	LEFT JOIN Nacionalidad NA ON p.idNacionalidad = NA.idNacionalidad
	LEFT JOIN Empresa EMP ON PDL.idEmpresa = EMP.idEmpresa
	LEFT JOIN Relacion RE ON PDL.idRelacion = RE.idRelacion
	LEFT JOIN Modalidad MO ON PDL.idModalidad = MO.idModalidad
	LEFT JOIN Funcion FU ON PDL.idFuncion = FU.idFuncion
	LEFT JOIN Sector SEC ON PDL.idSector = SEC.idSector
	LEFT JOIN Diagrama DIA ON PDL.idDiagrama = DIA.idDiagrama
	LEFT JOIN Banco BA ON PDL.idBanco = BA.idBanco
WHERE
	(p.idPersonal = @idPersonal)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalInformacionMedicaUpdate]
(
	@idPersonalInformacionMedica int,
	@GrupoSanguineo varchar(50),
	@Factor varchar(50),
	@Alergico varchar(50),
	@Observacion varchar(250),
	@HapatitisB bit,
	@Antitetanica bit,
	@ExPreocupacional bit,
	@FechaPreocupacional smalldatetime,
	@AprobacionPreocupacional bit,
	@ExPeriodico bit,
	@FechaPeriodico smalldatetime,
	@AprobacionPeriodico nchar(10)
)
AS
BEGIN

	UPDATE [PersonalInformacionMedica]
	SET
		[GrupoSanguineo] = @GrupoSanguineo,
		[Factor] = @Factor,
		[Alergico] = @Alergico,
		[Observacion] = @Observacion,
		[HapatitisB] = @HapatitisB,
		[Antitetanica] = @Antitetanica,
		[ExPreocupacional] = @ExPreocupacional,
		[FechaPreocupacional] = @FechaPreocupacional,
		[AprobacionPreocupacional] = @AprobacionPreocupacional,
		[ExPeriodico] = @ExPeriodico,
		[FechaPeriodico] = @FechaPeriodico,
		[AprobacionPeriodico] = @AprobacionPeriodico
	WHERE
		[idPersonalInformacionMedica] = @idPersonalInformacionMedica




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalInformacionMedicaGetById]
(
	@idPersonalInformacionMedica int
)
AS
BEGIN

	SELECT
		[idPersonalInformacionMedica],
		[GrupoSanguineo],
		[Factor],
		[Alergico],
		[Observacion],
		[HapatitisB],
		[Antitetanica],
		[ExPreocupacional],
		[FechaPreocupacional],
		[AprobacionPreocupacional],
		[ExPeriodico],
		[FechaPeriodico],
		[AprobacionPeriodico]
	FROM [PersonalInformacionMedica]
	WHERE
		([idPersonalInformacionMedica] = @idPersonalInformacionMedica)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalInformacionMedicaGetAll]
AS
BEGIN

	SELECT
		[idPersonalInformacionMedica],
		[GrupoSanguineo],
		[Factor],
		[Alergico],
		[Observacion],
		[HapatitisB],
		[Antitetanica],
		[ExPreocupacional],
		[FechaPreocupacional],
		[AprobacionPreocupacional],
		[ExPeriodico],
		[FechaPeriodico],
		[AprobacionPeriodico]
	FROM [PersonalInformacionMedica]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalInformacionMedicaDelete]
(
	@idPersonalInformacionMedica int
)
AS
BEGIN

	DELETE
	FROM [PersonalInformacionMedica]
	WHERE
		[idPersonalInformacionMedica] = @idPersonalInformacionMedica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalInformacionMedicaAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalInformacionMedicaAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalInformacionMedicaAdd]
(
	@GrupoSanguineo varchar(50),
	@Factor varchar(50),
	@Alergico varchar(50) = NULL,
	@Observacion varchar(250) = NULL,
	@HapatitisB bit = NULL,
	@Antitetanica bit = NULL,
	@ExPreocupacional bit = NULL,
	@FechaPreocupacional smalldatetime = NULL,
	@AprobacionPreocupacional bit = NULL,
	@ExPeriodico bit = NULL,
	@FechaPeriodico smalldatetime = NULL,
	@AprobacionPeriodico nchar(10) = NULL
)
AS
BEGIN


	INSERT
	INTO [PersonalInformacionMedica]
	(
		[GrupoSanguineo],
		[Factor],
		[Alergico],
		[Observacion],
		[HapatitisB],
		[Antitetanica],
		[ExPreocupacional],
		[FechaPreocupacional],
		[AprobacionPreocupacional],
		[ExPeriodico],
		[FechaPeriodico],
		[AprobacionPeriodico]
	)
	VALUES
	(
		@GrupoSanguineo,
		@Factor,
		@Alergico,
		@Observacion,
		@HapatitisB,
		@Antitetanica,
		@ExPreocupacional,
		@FechaPreocupacional,
		@AprobacionPreocupacional,
		@ExPeriodico,
		@FechaPeriodico,
		@AprobacionPeriodico
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDelete]
(
	@idPersonal int
)
AS
BEGIN

	UPDATE [Personal]
	SET BAJA = 1
	WHERE
		[idPersonal] = @idPersonal
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDatosLaboralesUpdate]
(
	@idPersonalDatosLaborales int,
	@idEmpresa int,
	@FechaAlta smalldatetime,
	@idRelacion int,
	@idModalidad int,
	@idFuncion int,
	@idSector int,
	@idDiagrama int,
	@idBanco int,
	@cbu varchar(50)
)
AS
BEGIN

	UPDATE [PersonalDatosLaborales]
	SET
		[idEmpresa] = @idEmpresa,
		[FechaAlta] = @FechaAlta,
		[idRelacion] = @idRelacion,
		[idModalidad] = @idModalidad,
		[idFuncion] = @idFuncion,
		[idSector] = @idSector,
		[idDiagrama] = @idDiagrama,
		[idBanco] = @idBanco,
		[cbu] = @cbu
	WHERE
		[idPersonalDatosLaborales] = @idPersonalDatosLaborales




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDatosLaboralesGetById]
(
	@idPersonalDatosLaborales int
)
AS
BEGIN

	SELECT
		[idPersonalDatosLaborales],
		[idEmpresa],
		[FechaAlta],
		[idRelacion],
		[idModalidad],
		[idFuncion],
		[idSector],
		[idDiagrama],
		[idBanco],
		[cbu]
	FROM [PersonalDatosLaborales]
	WHERE
		([idPersonalDatosLaborales] = @idPersonalDatosLaborales)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDatosLaboralesGetAll]
AS
BEGIN

	SELECT
		[idPersonalDatosLaborales],
		[idEmpresa],
		[FechaAlta],
		[idRelacion],
		[idModalidad],
		[idFuncion],
		[idSector],
		[idDiagrama],
		[idBanco]
	FROM [PersonalDatosLaborales]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDatosLaboralesDelete]
(
	@idPersonalDatosLaborales int
)
AS
BEGIN

	DELETE
	FROM [PersonalDatosLaborales]
	WHERE
		[idPersonalDatosLaborales] = @idPersonalDatosLaborales
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalDatosLaboralesAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalDatosLaboralesAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalDatosLaboralesAdd]
(
	@idEmpresa int,
	@FechaAlta smalldatetime,
	@idRelacion int = NULL,
	@idModalidad int = NULL,
	@idFuncion int = NULL,
	@idSector int = NULL,
	@idDiagrama int = NULL,
	@idBanco int = NULL,
	@CBU varchar(50) = NULL
)
AS
BEGIN


	INSERT
	INTO [PersonalDatosLaborales]
	(
		[idEmpresa],
		[FechaAlta],
		[idRelacion],
		[idModalidad],
		[idFuncion],
		[idSector],
		[idDiagrama],
		[idBanco],
		[cbu]
	)
	VALUES
	(
		@idEmpresa,
		@FechaAlta,
		@idRelacion,
		@idModalidad,
		@idFuncion,
		@idSector,
		@idDiagrama,
		@idBanco,
		@CBU
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetLogistica]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetLogistica]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetLogistica]

AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[NroLegajo],
		p.[Apellido] + '', '' + p.[Nombres] as apellido,
		p.[Nombres],
		p.[FechaNacimiento],
		p.[idSexo],
		p.[DNI],
		p.[CUIL],
		p.[idEstadoCivil],
		p.[idEstudios],
		p.[idNacionalidad],
		p.[idLocalidad],
		p.[Domicilio],
		p.[Telefono],
		p.[Mail],
		p.[idUsuarioAcceso],
		p.[idPersonalContacto],
		p.[idPersonalDatosLaborales],
		p.[idPersonalInformacionMedica],
		p.[idPersonalConduccion]
	FROM [Personal] p
	inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	inner join PersonalInformacionMedica pim on pim.idPersonalInformacionMedica = p.idPersonalInformacionMedica
	Inner join Sector s on s.idSector = pl.idSector
	WHERE p.BAJA = 0 
		and pc.VtoCarnet >= GETDATE()
		and upper(s.Descripcion) like ''%LOG%''
		and s.idEmpresa in(select idEmpresa from Empresa where interna=1)
	ORDER BY P.Apellido, P.Nombres

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdSectorEmpresa]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdSectorEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetByIdSectorEmpresa]
(
	@idSector int,
	@idEmpresa int
)
AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[NroLegajo],
		p.[Apellido] + '', '' + p.[Nombres] as apellido,
		p.[Nombres],
		p.[FechaNacimiento],
		p.[idSexo],
		p.[DNI],
		p.[CUIL],
		p.[idEstadoCivil],
		p.[idEstudios],
		p.[idNacionalidad],
		p.[idLocalidad],
		p.[Domicilio],
		p.[Telefono],
		p.[Mail],
		p.[idUsuarioAcceso],
		p.[idPersonalContacto],
		p.[idPersonalDatosLaborales],
		p.[idPersonalInformacionMedica],
		p.[idPersonalConduccion]
	FROM [Personal] p
	inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	inner join PersonalInformacionMedica pim on pim.idPersonalInformacionMedica = p.idPersonalInformacionMedica
	WHERE isnull(p.baja,0)=0
		and (pl.[idSector] = @idSector or @idSector is null)
		and (pl.[idEmpresa] = @idEmpresa or @idEmpresa is null)
		and pc.VtoCarnet >= GETDATE()
		--and pim.FechaPreocupacional >= GETDATE()
	ORDER BY P.Apellido, p.Nombres		

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdSector]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdSector]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetByIdSector]
(
	@idSector int
)
AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[NroLegajo],
		p.[Apellido] + '', '' + p.[Nombres] as apellido,
		p.[Nombres],
		p.[FechaNacimiento],
		p.[idSexo],
		p.[DNI],
		p.[CUIL],
		p.[idEstadoCivil],
		p.[idEstudios],
		p.[idNacionalidad],
		p.[idLocalidad],
		p.[Domicilio],
		p.[Telefono],
		p.[Mail],
		p.[idUsuarioAcceso],
		p.[idPersonalContacto],
		p.[idPersonalDatosLaborales],
		p.[idPersonalInformacionMedica],
		p.[idPersonalConduccion]
	FROM [Personal] p
	inner join PersonalDatosLaborales pl on pl.idPersonalDatosLaborales = p.idPersonalDatosLaborales
	inner join PersonalConduccion pc on pc.idPersonalConduccion = p.idPersonalConduccion
	inner join PersonalInformacionMedica pim on pim.idPersonalInformacionMedica = p.idPersonalInformacionMedica
	WHERE
		(pl.[idSector] = @idSector or @idSector is null)
		and pc.VtoCarnet >= GETDATE()
		

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetByIdOrden]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetByIdOrden]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetByIdOrden]
(
	@idOrden int
)
AS
BEGIN

	SELECT
		p.[idPersonal],
		p.[Apellido] +'', '' + p.[Nombres] as apellido
	FROM [Personal] p
	INNER JOIN OrdenTrabajoDetalle otd on otd.idPersonal = p.idPersonal
	WHERE
		(otd.idOrden = @idOrden)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetById]
(
	@idPersonal int
)
AS
BEGIN

	SELECT
		[idPersonal],
		[NroLegajo],
		[Apellido],
		[Nombres],
		[FechaNacimiento],
		[idSexo],
		[DNI],
		[CUIL],
		[idEstadoCivil],
		[idEstudios],
		[idNacionalidad],
		[idLocalidad],
		[Domicilio],
		[Telefono],
		[Mail],
		[idUsuarioAcceso],
		[idPersonalContacto],
		[idPersonalDatosLaborales],
		[idPersonalInformacionMedica],
		[idPersonalConduccion]
	FROM [Personal]
	WHERE
		([idPersonal] = @idPersonal)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGetAll]
AS
BEGIN

	SELECT
		[idPersonal],
		[NroLegajo],
		[Apellido],
		[Nombres],
		[FechaNacimiento],
		[idSexo],
		[DNI],
		[CUIL],
		[idEstadoCivil],
		[idEstudios],
		[idNacionalidad],
		[idLocalidad],
		[Domicilio],
		[Telefono],
		[Mail],
		[idUsuarioAcceso],
		[idPersonalContacto],
		[idPersonalDatosLaborales],
		[idPersonalInformacionMedica],
		[idPersonalConduccion]
	FROM [Personal]
	ORDER BY Apellido, Nombres
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalGestionGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalGestionGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalGestionGetByFilter]
	(
		@ApNom varchar(250),
		@DNI int,
		@nroLegajo varchar(50),
		@idEmpresa int,
		@idSector int,
		@vtoCarnet datetime,
		@vtoPsico datetime,
		@vtoGrales datetime,
		@vtoPeligrosa datetime,
		@vtoMDef datetime
	)
	
AS

BEGIN

	SELECT P.Apellido,
		P.Nombres,		
		CONVERT(VARCHAR(10),P.FechaNacimiento, 103) AS FECHANAC,
		E.RazonSocial AS EMPRESA,
		P.DNI,
		S.Descripcion AS Sexo,
		SE.Descripcion AS Sector,
		P.NroLegajo,
		P.idPersonal,
		CONVERT(VARCHAR(10),PC.VtoCargasGenerales, 103) AS VtoCargasGenerales,
		CONVERT(VARCHAR(10),PC.VtoCargasPeligrosas, 103) AS VtoCargasPeligrosas,
		CONVERT(VARCHAR(10),PC.VtoCarnet, 103) AS VtoCarnet,
		CONVERT(VARCHAR(10),PC.VtoPsicofisico, 103) AS VtoPsicofisico,
		CONVERT(VARCHAR(10),PC.VtoManejoDefensivo, 103) AS VtoManejoDefensivo
		
		
		
		
	FROM Personal P
	INNER JOIN Sexo S ON P.idSexo = S.idSexo
	left JOIN PersonalDatosLaborales L ON P.idPersonalDatosLaborales = L.idPersonalDatosLaborales
	left JOIN EMPRESA E ON E.IDEMPRESA = L.idEmpresa AND (E.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
	left JOIN PersonalConduccion PC on P.idPersonalConduccion = PC.idPersonalConduccion 
													AND ( (PC.VtoCarnet >= @vtoCarnet OR @vtoCarnet IS NULL)
													AND (PC.VtoPsicofisico >= @vtoPsico OR @vtoPsico IS NULL)
													AND (PC.VtoCargasGenerales >= @vtoGrales OR @vtoGrales IS NULL)
													AND (PC.VtoCargasPeligrosas >= @vtoPeligrosa OR @vtoPeligrosa IS NULL)
													AND (PC.VtoManejoDefensivo >= @vtoMDef OR @vtoMDef IS NULL) )
	left JOIN PersonalInformacionMedica PM on P.idPersonalInformacionMedica = PM.idPersonalInformacionMedica
	left JOIN Sector SE on SE.idSector = L.idSector	AND (SE.idSector = @idSector OR @idSector IS NULL)
				
				
	WHERE
	(P.BAJA = 0)
	AND ( P.Apellido + P.Nombres LIKE ''%'' + @ApNom + ''%'' OR (@ApNom IS NULL) )
	AND (P.DNI = @DNI OR @DNI IS NULL)
	AND (P.NroLegajo =@nroLegajo OR @nroLegajo IS NULL)
	AND ((SE.idSector = @idSector) OR (@idSector IS NULL) )
	AND ((L.idEmpresa = @idEmpresa) OR (@idEmpresa IS NULL))
	
	
	
	
	ORDER BY P.Apellido , P.Nombres desc

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesUpdate]
(
	@idPersonalFiles int,
	@idPersonal int,
	@Nombre varchar(150),
	@Ruta varchar(150)
)
AS
BEGIN

	UPDATE [PersonalFiles]
	SET
		[idPersonal] = @idPersonal,
		[Nombre] = @Nombre,
		[Ruta] = @Ruta
	WHERE
		[idPersonalFiles] = @idPersonalFiles




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetByIdPersonal]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetByIdPersonal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesGetByIdPersonal]
(
	@idPersonal int
)
AS
BEGIN

	SELECT
		[idPersonalFiles],
		[idPersonal],
		[Nombre],
		[Ruta]
	FROM [PersonalFiles]PF
	WHERE
		(PF.idPersonal = @idPersonal)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesGetById]
(
	@idPersonalFiles int
)
AS
BEGIN

	SELECT
		[idPersonalFiles],
		[idPersonal],
		[Nombre],
		[Ruta]
	FROM [PersonalFiles]
	WHERE
		([idPersonalFiles] = @idPersonalFiles)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesGetAll]
AS
BEGIN

	SELECT
		[idPersonalFiles],
		[idPersonal],
		[Nombre],
		[Ruta]
	FROM [PersonalFiles]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesDelete]
(
	@idPersonalFiles int
)
AS
BEGIN

	DELETE
	FROM [PersonalFiles]
	WHERE
		[idPersonalFiles] = @idPersonalFiles
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalFilesAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalFilesAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalFilesAdd]
(
	@idPersonal int,
	@Nombre varchar(150),
	@Ruta varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [PersonalFiles]
	(
		[idPersonal],
		[Nombre],
		[Ruta]
	)
	VALUES
	(
		@idPersonal,
		@Nombre,
		@Ruta
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalContactoUpdate]
(
	@idPersonalContacto int,
	@Apellido varchar(100),
	@Nombre varchar(150),
	@Relacion varchar(50),
	@Telefono varchar(50)
)
AS
BEGIN

	UPDATE [PersonalContacto]
	SET
		[Apellido] = @Apellido,
		[Nombre] = @Nombre,
		[Relacion] = @Relacion,
		[Telefono] = @Telefono
	WHERE
		[idPersonalContacto] = @idPersonalContacto




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalContactoGetById]
(
	@idPersonalContacto int
)
AS
BEGIN

	SELECT
		[idPersonalContacto],
		[Apellido],
		[Nombre],
		[Relacion],
		[Telefono]
	FROM [PersonalContacto]
	WHERE
		([idPersonalContacto] = @idPersonalContacto)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalContactoGetAll]
AS
BEGIN

	SELECT
		[idPersonalContacto],
		[Apellido],
		[Nombre],
		[Relacion],
		[Telefono]
	FROM [PersonalContacto]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalContactoDelete]
(
	@idPersonalContacto int
)
AS
BEGIN

	DELETE
	FROM [PersonalContacto]
	WHERE
		[idPersonalContacto] = @idPersonalContacto
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalContactoAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalContactoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalContactoAdd]
(
	@Apellido varchar(100),
	@Nombre varchar(150),
	@Relacion varchar(50) = NULL,
	@Telefono varchar(50) = NULL
)
AS
BEGIN


	INSERT
	INTO [PersonalContacto]
	(
		[Apellido],
		[Nombre],
		[Relacion],
		[Telefono]
	)
	VALUES
	(
		@Apellido,
		@Nombre,
		@Relacion,
		@Telefono
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalConduccionUpdate]
(
	@idPersonalConduccion int,
	@ManejaEmpresa bit,
	@VtoCarnet smalldatetime,
	@Psicofisico bit,
	@VtoPsicofisico smalldatetime,
	@CargarGenerales bit,
	@VtoCargasGenerales smalldatetime,
	@CargarPeligrosas bit,
	@VtoCargasPeligrosas smalldatetime,
	@ManejoDefensivo bit,
	@VtoManejoDefensivo smalldatetime,
	@Empresa varchar(150),
	@CertCNRT varchar(150),
	@IdCategoria int,
	@CarnetPsicofisico bit,
	@VencimientoCarnet smalldatetime
)
AS
BEGIN

	UPDATE [PersonalConduccion]
	SET
		[ManejaEmpresa] = @ManejaEmpresa,
		[VtoCarnet] = @VtoCarnet,
		[Psicofisico] = @Psicofisico,
		[VtoPsicofisico] = @VtoPsicofisico,
		[CargarGenerales] = @CargarGenerales,
		[VtoCargasGenerales] = @VtoCargasGenerales,
		[CargarPeligrosas] = @CargarPeligrosas,
		[VtoCargasPeligrosas] = @VtoCargasPeligrosas,
		[ManejoDefensivo] = @ManejoDefensivo,
		[VtoManejoDefensivo] = @VtoManejoDefensivo,
		[Empresa] = @Empresa,
		[CertCNRT] = @CertCNRT,
		[idCategoria] = @IdCategoria,
		[CarnetPsicofisico] = @CarnetPsicofisico,
		[VencimientoCarnet] = @VencimientoCarnet
	WHERE
		[idPersonalConduccion] = @idPersonalConduccion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalConduccionGetById]
(
	@idPersonalConduccion int
)
AS
BEGIN

	SELECT
		[idPersonalConduccion],
		[ManejaEmpresa],
		[VtoCarnet],
		[Psicofisico],
		[VtoPsicofisico],
		[CargarGenerales],
		[VtoCargasGenerales],
		[CargarPeligrosas],
		[VtoCargasPeligrosas],
		[ManejoDefensivo],
		[VtoManejoDefensivo],
		[Empresa],
		[CertCNRT],
		[IdCategoria],
		[CarnetPsicofisico],
		[VencimientoCarnet]
	FROM [PersonalConduccion]
	WHERE
		([idPersonalConduccion] = @idPersonalConduccion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalConduccionGetAll]
AS
BEGIN

	SELECT
		[idPersonalConduccion],
		[ManejaEmpresa],
		[VtoCarnet],
		[Psicofisico],
		[VtoPsicofisico],
		[CargarGenerales],
		[VtoCargasGenerales],
		[CargarPeligrosas],
		[VtoCargasPeligrosas],
		[ManejoDefensivo],
		[VtoManejoDefensivo],
		[Empresa],
		[CertCNRT],
		[IdCategoria],
		[CarnetPsicofisico],
		[VencimientoCarnet]
	FROM [PersonalConduccion]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalConduccionDelete]
(
	@idPersonalConduccion int
)
AS
BEGIN

	DELETE
	FROM [PersonalConduccion]
	WHERE
		[idPersonalConduccion] = @idPersonalConduccion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalConduccionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalConduccionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalConduccionAdd]
(
	@ManejaEmpresa bit = NULL,
	@VtoCarnet smalldatetime = NULL,
	@Psicofisico bit = NULL,
	@VtoPsicofisico smalldatetime = NULL,
	@CargarGenerales bit = NULL,
	@VtoCargasGenerales smalldatetime = NULL,
	@CargarPeligrosas bit = NULL,
	@VtoCargasPeligrosas smalldatetime = NULL,
	@ManejoDefensivo bit = NULL,
	@VtoManejoDefensivo smalldatetime = NULL,
	@Empresa varchar(150) = NULL,
	@CertCNRT varchar(150) = NULL,
	@idCategoria int = NULL,
	@CarnetPsicofisico bit = NULL,
	@VencimientoCarnet smalldatetime = NULL
)
AS
BEGIN


	INSERT
	INTO [PersonalConduccion]
	(
		[ManejaEmpresa],
		[VtoCarnet],
		[Psicofisico],
		[VtoPsicofisico],
		[CargarGenerales],
		[VtoCargasGenerales],
		[CargarPeligrosas],
		[VtoCargasPeligrosas],
		[ManejoDefensivo],
		[VtoManejoDefensivo],
		[Empresa],
		[CertCNRT],
		[IdCategoria],
		[CarnetPsicofisico],
		[VencimientoCarnet]
	)
	VALUES
	(
		@ManejaEmpresa,
		@VtoCarnet,
		@Psicofisico,
		@VtoPsicofisico,
		@CargarGenerales,
		@VtoCargasGenerales,
		@CargarPeligrosas,
		@VtoCargasPeligrosas,
		@ManejoDefensivo,
		@VtoManejoDefensivo,
		@Empresa,
		@CertCNRT,
		@idCategoria,
		@CarnetPsicofisico,
		@VencimientoCarnet
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PersonalAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonalAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PersonalAdd]
(
	@NroLegajo varchar(50),
	@Apellido varchar(100),
	@Nombres varchar(150) = NULL,
	@FechaNacimiento smalldatetime = NULL,
	@idSexo int = NULL,
	@DNI int,
	@CUIL varchar(30) = NULL,
	@idEstadoCivil int = NULL,
	@idEstudios int = NULL,
	@idNacionalidad int = NULL,
	@idLocalidad int = NULL,
	@Domicilio varchar(150) = NULL,
	@Telefono varchar(50) = NULL,
	@Mail varchar(50) = NULL,
	@idUsuarioAcceso int = NULL,
	@idPersonalContacto int,
	@idPersonalDatosLaborales int,
	@idPersonalInformacionMedica int,
	@idPersonalConduccion int = NULL,
	@baja bit = 0
)
AS
BEGIN


	INSERT
	INTO [Personal]
	(
		[NroLegajo],
		[Apellido],
		[Nombres],
		[FechaNacimiento],
		[idSexo],
		[DNI],
		[CUIL],
		[idEstadoCivil],
		[idEstudios],
		[idNacionalidad],
		[idLocalidad],
		[Domicilio],
		[Telefono],
		[Mail],
		[idUsuarioAcceso],
		[idPersonalContacto],
		[idPersonalDatosLaborales],
		[idPersonalInformacionMedica],
		[idPersonalConduccion],
		[baja]
	)
	VALUES
	(
		@NroLegajo,
		@Apellido,
		@Nombres,
		@FechaNacimiento,
		@idSexo,
		@DNI,
		@CUIL,
		@idEstadoCivil,
		@idEstudios,
		@idNacionalidad,
		@idLocalidad,
		@Domicilio,
		@Telefono,
		@Mail,
		@idUsuarioAcceso,
		@idPersonalContacto,
		@idPersonalDatosLaborales,
		@idPersonalInformacionMedica,
		@idPersonalConduccion,
		@baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudRegresoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudRegresoGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudRegresoGetByFilter]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN
	
	SELECT distinct top 200 SC.idSolicitud,
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' - ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NroSolicitud],	
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,
			c.Codigo + '' '' + vi.NroCodificacion as Trailer,
			v.idVehiculo as IDTrailer,
			isnull(ot.idOrden,0) as idOrden,
			SC.Entrega,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.Fecha AS FECHAORD
	FROM SolicitudCliente SC
	left join Solicitante s on (s.idSolicitante = SC.idSolicitante or SC.idSolicitante is null)
								and (s.idSector = SC.idSector)	
	left join Sector sec on (sec.idSector = s.idSector or s.idSector is null)
							and (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud	and ot.Baja=0 
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	WHERE (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
			AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
			AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
			AND SC.Baja = 0
			and SC.Entrega = 0
			/*AND ScIda.Entrega = 1
			AND ScIda.Baja = 0*/
			AND (v.idVehiculo = @idTRailer or @idTRailer is null)
			
	ORDER BY FECHAORD DESC	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudHistorialGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudHistorialGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudHistorialGetByFilter]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN
	
	SELECT  SC.idSolicitud,
			--SC.NroSolicitud,	
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NroSolicitud],
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,
			ISNULL(c.Codigo + '' '' + vi.NroCodificacion, ''S/T'') as Trailer,
			isnull(ot.idOrden,0) as idOrden,
			SC.Entrega,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.NuevoMovimiento,
			ISNULL(pe.idParteEntrega,0) as idParteEntrega
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante --or SC.idSolicitante is null)
								--and (s.idSector = SC.idSector)	
	left join Sector sec on (sec.idSector = SC.idSector)
							--and (sec.idSector = s.idSector or s.idSector is null)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio	
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud	and ot.Baja=0 
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	WHERE (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
			AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
			AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
			AND (v.idVehiculo = @idTRailer or @idTRailer is null)			
			AND SC.Baja = 0
	ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
			
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE  PROCEDURE [dbo].[SolicitudClienteGetByFilter]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN

	SELECT distinct top 100 SC.idSolicitud,
			SC.NroSolicitud,
			sc.NroPedidoCliente,	
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,			
			isnull(c.Codigo + '' '' + vi.NroCodificacion, c2.Codigo + '' '' + vi2.NroCodificacion) as Trailer,
			isnull(ot.idOrden,0) as idOrden,
			isnull(SC.Entrega,0) as Entrega,
			SC.Fecha AS FECHSC,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.NUEVOMOVIMIENTO,
			DBO.ABC(SC.NroSolicitud, SC.idSolicitud) as Letra,
			dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud) as FechaRegreso,
			t2.idVehiculo
	FROM SolicitudCliente SC
	left join Solicitante s on (s.idSolicitante = SC.idSolicitante or SC.idSolicitante is null)
								and (s.idSector = SC.idSector)	
	left join Sector sec on (sec.idSector = s.idSector or s.idSector is null)
							and (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join SolicitudTrailerPosible st on sc.NroSolicitud = st.idSolicitud and sc.idTipoServicio = st.idTipoServicio
	left join Vehiculo t2 on t2.idVehiculo = st.idTrailer
	LEFT join VehiculoIdentificacion vi2 on vi2.idVehiculo = t2.idVehiculo
	LEFT join Codificacion c2 on c2.idCodificacion = vi2.idCodificacion 
	WHERE (NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
	AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
	AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
	AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
	AND (vi.idVehiculo = @idTRailer or @idTRailer is null)
	AND SC.Baja = 0
	AND ((SC.NroSolicitud IN (	SELECT NroSolicitud 
								FROM SOLICITUDCLIENTE 
								WHERE BAJA = 0 AND NUEVOMOVIMIENTO = 1 
								and NroSolicitud = SC.NroSolicitud
								--and idTipoServicio <> SC.idTipoServicio
								) 
			AND NUEVOMOVIMIENTO = 1)
		 OR (NUEVOMOVIMIENTO =  0 
			AND NOT exists (SELECT *
							FROM SOLICITUDCLIENTE 
							WHERE BAJA = 0 AND NUEVOMOVIMIENTO = 1
							and NroSolicitud = SC.NroSolicitud
							and idTipoServicio = SC.idTipoServicio)))
	AND (SC.idSolicitud = (select MAX(idsolicitud) 
						   from SolicitudCliente
						   where Baja = 0
						   and NuevoMovimiento = 1
						   and NroSolicitud = SC.NroSolicitud
						   and idTipoServicio = SC.idTipoServicio)
		or NuevoMovimiento = 0)
	AND (SC.Entrega = 1 AND SC.NroPedidoCliente NOT IN(SELECT NroPedidoCliente
													FROM SolicitudCliente 															
													WHERE Entrega = 0
													AND Baja = 0))
	AND sc.NroSolicitud not in (select sc2.NroSolicitud
								from SolicitudCliente sc2
								where sc2.NroSolicitud = SC.NroSolicitud
								and sc2.Entrega = 0 and sc2.NuevoMovimiento = 0
								and sc2.NroPedidoCliente like ''%Z'')
	
								
	
	ORDER BY FECHSC DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudClienteEliminadas]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudClienteEliminadas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudClienteEliminadas]
--DECLARE
	@NroSolicitud int=NULL,
	@idEmpresa int=NULL,
	@fechaDesde datetime=NULL,
	@fechaHasta datetime=NULL,
	@idTRailer int = null
AS
BEGIN
	
	SELECT  SC.idSolicitud,
			--SC.NroSolicitud,	
			SC.NroSolicitud,
			convert(varchar,SC.Fecha,103) as Fecha,
			e.idEmpresa,
			e.Descripcion as Empresa,
			sec.Descripcion as Sector,
			ts.Descripcion as TipoServicio,
			ISNULL(c.Codigo + '' '' + vi.NroCodificacion, ''S/T'') as Trailer,
			isnull(ot.idOrden,0) as idOrden,
			SC.Entrega,
			m.pozoDestino,
			lDe.Descripcion as Destino,
			SC.NuevoMovimiento,
			ISNULL(pe.idParteEntrega,0) as idParteEntrega
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante							
	left join Sector sec on (sec.idSector = SC.idSector)
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio	
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud	and ot.Baja=0 
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	WHERE (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
			AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
			AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
			AND (v.idVehiculo = @idTRailer or @idTRailer is null)			
			AND SC.Baja = 1
	ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
			
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RPTOTI]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPTOTI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RPTOTI]
(
--DECLARE
	@idOTI AS VARCHAR(250)
)
AS

BEGIN

SELECT DISTINCT 
         OrdenTrabajo.idOrden, 
         OrdenTrabajo.idSolicitudCliente, 
         SolicitudCliente.idMovimiento, 
         SolicitudCliente.NroPedidoCliente, 
         SolicitudCliente.NroSolicitud, 
         v.trailer AS Transporte, 
         Cod2.Codigo + '' '' + VI2.NroCodificacion AS Trailer, 
         TipoServicio.Descripcion AS TipoServicio, 
         LugarOrigen.Descripcion AS LugarOrigen, 
         Solicitante.Apellido AS Solicitante, 
         Sector.Descripcion AS Sector, 
         LugarDestino.Descripcion AS LugarDestino, 
         Empresa.RazonSocial AS Empresa, 
         SolicitudCliente.Fecha, 
         Chofer.Apellido + '', '' + Chofer.Nombres AS Chofer, 
         Movimiento.pozoOrigen, Movimiento.pozoDestino, 
         Movimiento.KmsEstimados, 
         Acompanante.Apellido + '', '' + Acompanante.Nombres AS Acompanante,          
         SolicitudCliente.idTipoServicio, 
         sce.idEspecificacion, 
         et.Descripcion AS Especificacion, 
         OrdenTrabajoConcepto.Concepto, 
         OrdenTrabajoConcepto.Valor,
         c.Descripcion AS Condicion
FROM         OrdenTrabajo INNER JOIN
             SolicitudCliente ON OrdenTrabajo.idSolicitudCliente = SolicitudCliente.idSolicitud INNER JOIN
             Sector ON SolicitudCliente.idSector = Sector.idSector INNER JOIN              
             Empresa ON Empresa.idEmpresa = Sector.idEmpresa INNER JOIN
             TipoServicio ON SolicitudCliente.idTipoServicio = TipoServicio.idTipoServicio LEFT OUTER JOIN
             OrdenTrabajoConcepto ON OrdenTrabajo.idOrden = OrdenTrabajoConcepto.idOti LEFT OUTER JOIN
             Condicion AS c ON SolicitudCliente.idCondicion = c.idCondicion LEFT OUTER JOIN
             (--VEHICULO
			(select v.idVehiculo,
			c.Codigo + '' '' + vi.NroCodificacion as trailer, ed.idEspecificacion
			from Vehiculo v
			inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
			inner join Codificacion c on c.idCodificacion = vi.idCodificacion
			left join EspecificacionDetalle ed on ed.idEquipoPropio = v.idVehiculo) 
			union 
			(--CUADRILLA
			select c.idCuadrilla, c.Nombre, ed.idEspecificacion
			from Cuadrilla c
			left join EspecificacionDetalle ed on ed.idCuadrilla = c.idCuadrilla)
			)v on v.idVehiculo = OrdenTrabajo.idTrailer  join
             --EQUIPO
             Vehiculo AS equipo ON OrdenTrabajo.idTrailer = equipo.idVehiculo LEFT OUTER JOIN
             VehiculoIdentificacion AS VI2 ON VI2.idVehiculo = equipo.idVehiculo LEFT OUTER JOIN
             Codificacion AS Cod2 ON Cod2.idCodificacion = VI2.idCodificacion LEFT OUTER JOIN
             
             Personal as chofer ON OrdenTrabajo.idChofer = Chofer.idPersonal LEFT OUTER JOIN
             Personal AS Acompanante ON OrdenTrabajo.idAcompanante = Acompanante.idPersonal LEFT OUTER JOIN
             
             Movimiento ON SolicitudCliente.idMovimiento = Movimiento.idMovimiento LEFT OUTER JOIN
             Lugar AS LugarOrigen ON Movimiento.idLugarOrigen = LugarOrigen.idLugar LEFT OUTER JOIN
             Lugar AS LugarDestino ON Movimiento.idLugarDestino = LugarDestino.idLugar LEFT OUTER JOIN
             Solicitante ON Solicitante.idSolicitante = SolicitudCliente.idSolicitante INNER JOIN
             
             Acuerdo A ON A.idCliente = Empresa.idEmpresa AND A.idSector = SolicitudCliente.idSector INNER JOIN
             AcuerdoServicio AS ase ON A.idAcuerdo = ase.idAcuerdo AND ase.idTipoServicio = SolicitudCliente.idTipoServicio LEFT OUTER JOIN
             AcuerdoConceptoCosto AS acc ON ase.idAcuerdoServicio = acc.idAcuerdoServicio LEFT OUTER JOIN
             
             SolicitudClienteEspecificacion sce on sce.idSolicitudCliente = SolicitudCliente.idSolicitud left join
             --EspecificacionDetalle AS ed ON ed.idEquipoPropio = v.idVehiculo and sce.idEspecificacion = ed.idEspecificacion LEFT OUTER JOIN 
             EspecificacionTecnica AS et ON et.idEspecificaciones = v.idEspecificacion and sce.idEspecificacion = v.idEspecificacion 
             
WHERE        (SolicitudCliente.Baja = 0) 
AND (OrdenTrabajo.BAJA = 0)
AND (OrdenTrabajo.idOrden IN
     (SELECT Fld
       FROM  dbo.ArrayToTable(@idOTI, '','') AS ArrayToTable_1)) 
 
                               
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ReportePosicionActual]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportePosicionActual]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ReportePosicionActual]
--declare	
	@idEmpresa int,
	@idSector int,
	@idCondicion int,
	@idTrailer int,
	@idLugarDestino int,
	@idTipoServicio int

AS

BEGIN

	SELECT  s.Empresa,
			s.Sector,
			s.[Condición],
			s.[Tipo de Servicio],
			c.Codigo + '' '' + vi.NroCodificacion as [N° Equipo],
			isnull(s.Origen,''En Base'') as Origen,
			s.[Pozo Origen],
			s.Destino,
			s.[Pozo Destino],
			convert(varchar, s.fecha, 103) as Fecha,
			s.[N° Solicitud],
			s.[N° Parte],
			s.[N° Remito]
	FROM Vehiculo v
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	LEFT JOIN (
		SELECT 	e.Descripcion as [Empresa],
				sec.Descripcion as [Sector],
				c.Descripcion as [Condición],
				ts.Descripcion as [Tipo de Servicio],
				isnull(cod.Codigo + '' '' + vi.NroCodificacion,sc.servicioaux) as [N° Equipo],
				lor.Descripcion as [Origen],
				m.pozoOrigen as [Pozo Origen],			
				lde.Descripcion as [Destino],
				m.pozoDestino as[Pozo Destino],
				isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [N° Solicitud],
				pe.NroParte as [N° Parte],
				pe.nroRemito as [N° Remito], 
				OT.IDTRAILER,		
				SC.NROSOLICITUD,
				SC.IDSOLICITUD,
				SC.FECHA,
				e.idEmpresa,
				sec.idSector,
				c.idCondicion,
				ts.idTipoServicio,
				m.idLugarDestino
		FROM SolicitudCliente SC
		inner join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 	
		INNER JOIN (select * from dbo.UltimaOtiTrailer()) OTI ON OTI.IDsolicitud = sc.IDsolicitud
																--and OTI.fecha = SC.fecha	
		left join Solicitante s on s.idSolicitante = SC.idSolicitante
		left join Sector sec on (sec.idSector = SC.idSector)						
		left join Empresa e on e.idEmpresa = sec.idEmpresa
		left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
		left join Condicion c on c.idCondicion = SC.idCondicion
		LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
		LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
		LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
		left join Movimiento m on m.idMovimiento = SC.idMovimiento
		left join Lugar lDe on lDe.idLugar = m.idLugarDestino
		left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
		left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
		WHERE SC.Baja = 0 AND OT.BAJA = 0			
		) S
		ON S.IDTRAILER = v.idVehiculo
	WHERE  (S.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
			AND (S.idSector = @idSector or @idSector IS NULL)
			AND (S.idCondicion = @idCondicion OR @idCondicion IS NULL)
			AND (v.idVehiculo = @idTrailer OR @idTrailer IS NULL)
			AND (S.idLugarDestino = @idLugarDestino OR @idLugarDestino IS NULL)				
			AND (S.idTipoServicio = @idTipoServicio or @idTipoServicio is null)
	ORDER BY S.Fecha DESC,DBO.ABC(S.NroSolicitud, S.idSolicitud) DESC
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ReporteMovimiento]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReporteMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ReporteMovimiento]	
--declare	
	@fechaDesde datetime = null,
	@fechaHasta datetime = null,
	@idEmpresa int = null,
	@idSector int = null,
	@idCondicion int = null,
	@idTrailer int = null,
	@idLugarOrigen int = null,
	@idLugarDestino int = null,
	@idChofer int = null,
	@idVehiculo int = null,
	@NroSolicitud int = null,
	@idTipoServicio int = null,
	@remito bit
AS

BEGIN

	SELECT convert(varchar,SC.Fecha,103) as Fecha,
			--SC.NroSolicitud as [N° Solicitud],
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NSolicitud],
			ts.Descripcion as [Tipo de Servicio],	
			case when isnull(pe.Conexion,0) = 0 then ''N'' else ''S'' end as [Conexión],
			case when isnull(pe.Desconexion ,0) = 0 then ''N'' else ''S'' end as [Desconexión],
			e.Descripcion as [Empresa],
			sec.Descripcion as [Sector],
			s.Nombre as [Solicitante],
			isnull(cod.Codigo + '' '' + vi.NroCodificacion,sc.servicioaux) as [N° Equipo],
			c.Descripcion as [Condición],
			convert(varchar,ScRegreso.Fecha,103) as [Fecha Regreso],
			CASE WHEN (SELECT COUNT(*) FROM SolicitudCliente
						WHERE NroSolicitud = SC.NroSolicitud AND BAJA = 0) > 2 THEN					   
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = ''B'' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			ELSE
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = ''Z'' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			END AS [Cant. Días],
			lor.Descripcion as [Origen],
			m.pozoOrigen as [Pozo Origen],			
			lde.Descripcion as [Destino],
			m.pozoDestino as[Pozo Destino],
                        tr.itemverificacion as [Vehículo],
			convert(int,m.KmsEstimados) as [Kms],
			chofer.Apellido + '', '' + chofer.Nombre  as [Chofer],
			acomp.Apellido + '', '' +	acomp.Nombre  as [Acompañante],
			pe.NroParte as [N° Parte],
			pe.nroRemito as [N° Remito],
			u.nombre as Usuario
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante
	left join Sector sec on (sec.idSector = SC.idSector)						
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join Condicion c on c.idCondicion = SC.idCondicion
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 		
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	left join Chofer chofer on chofer.idChofer = ot.idChofer
	left join Chofer acomp on acomp.idChofer = ot.idAcompanante
    left join Transporte tr on tr.idTransporte = ot.idTransporte
	left join SolicitudCliente ScRegreso on ScRegreso.NroSolicitud = SC.NroSolicitud 
											and ScRegreso.Entrega = 0 and ScRegreso.Baja = 0 
											and SC.idSolicitud = ScRegreso.idSolicitudOrig
	left join usuario u on u.idUsuario = sc.idUsuario
	WHERE SC.Baja = 0 AND ot.Baja=0
		AND (sc.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (sc.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.idEmpresa = @idEmpresa OR @idEmpresa IS NULL)
		AND (sec.idSector = @idSector or @idSector IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
        AND (ot.idTrailer = @idTrailer OR @idTrailer IS NULL)
		AND (M.idLugarOrigen = @idLugarOrigen OR @idLugarOrigen IS NULL)
		AND (M.idLugarDestino = @idLugarDestino OR @idLugarDestino IS NULL)
		AND (SC.idUsuario = @idChofer OR @idChofer IS NULL)
		AND (ot.idTransporte = @idVehiculo OR @idVehiculo IS NULL)
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL)
		AND (ts.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (((@remito = 1 and pe.nroremito is not null) or (@remito = 0 and pe.nroremito is null))
			or @remito is null)
	ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ReporteAlertaMovimiento]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReporteAlertaMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ReporteAlertaMovimiento]	

AS

BEGIN
	SELECT * FROM(
	SELECT convert(varchar,SC.Fecha,103) as Fecha,
			SC.NroSolicitud,
			isnull(sc.NroPedidoCliente, str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as [NSolicitud],
			ts.Descripcion as [TipoServicio],	
			e.Descripcion as [Empresa],
			sec.Descripcion as [Sector],
			s.Nombre as [Solicitante],
			isnull(cod.Codigo + '' '' + vi.NroCodificacion,sc.servicioaux) as [NroEquipo],
			c.Descripcion as [Condición],			
			CASE WHEN (SELECT COUNT(*) FROM SolicitudCliente
						WHERE NroSolicitud = SC.NroSolicitud AND BAJA = 0) >= 2 THEN					   
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = ''B'' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE 0 END
				END
			ELSE
				CASE WHEN DBO.ABC(SC.NroSolicitud, SC.idSolicitud) = ''Z'' THEN
				(datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) + 1)
				ELSE 
					CASE WHEN datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha) > 0 THEN
							  datediff(D, dbo.FechaRegreso(SC.NroSolicitud, SC.idSolicitud),SC.Fecha)  
					ELSE datediff(D, SC.Fecha, getDate() )   END
				END
			END AS [CantDias],
			lor.Descripcion as [Origen],
			m.pozoOrigen as [PozoOrigen],			
			lde.Descripcion as [Destino],
			m.pozoDestino as[PozoDestino],			
			SC.idSolicitud,
			ot.idOrden, 
			sc.fecha as fechaSC
	FROM SolicitudCliente SC
	left join Solicitante s on s.idSolicitante = SC.idSolicitante
	left join Sector sec on (sec.idSector = SC.idSector)						
	left join Empresa e on e.idEmpresa = sec.idEmpresa
	left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
	left join Condicion c on c.idCondicion = SC.idCondicion
	left join OrdenTrabajo ot on ot.idSolicitudCliente = SC.idSolicitud 		
	LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
	LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	LEFT join Codificacion cod on cod.idCodificacion = vi.idCodificacion 
	left join Movimiento m on m.idMovimiento = SC.idMovimiento
	left join Lugar lDe on lDe.idLugar = m.idLugarDestino
	left join Lugar lOr on lOr.idLugar = m.idLugarOrigen
	left join ParteEntrega pe on pe.idOrdenTrabajo = ot.idOrden
	left join Chofer chofer on chofer.idChofer = ot.idChofer
	left join Chofer acomp on acomp.idChofer = ot.idAcompanante
    left join Transporte tr on tr.idTransporte = ot.idTransporte
	WHERE SC.Baja = 0 
	AND ot.Baja=0
	AND (SC.Entrega = 1 AND SC.NroSolicitud NOT IN(SELECT NroSolicitud 
													FROM SolicitudCliente 															
													WHERE Entrega = 0
													AND Baja = 0))
	AND sc.idcondicion in (SELECT IDCONDICION FROM CONDICION WHERE DESCRIPCION LIKE ''DIARIO'')
	)SC
	WHERE SC.CantDias >= 15
	ORDER BY SC.fechaSC DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer int = null,
	@idSector int,
	@idTipoServicio int,
	@idCondicion int,
	@nroRemito int = null
		
)
AS
BEGIN

SELECT	
		SC.idSolicitud,		
		isnull(sc.NroPedidoCliente,str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as NroSolicitud,
		sc.NroPedidoCliente,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		PE.NroParte as NroParte,
		R.nroRemito as nroRemito,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		ISNULL(OT.idOrden,0) as idOrden,
		ISNULL(OT.idTrailer,0) as idTrailer, 
		e.Descripcion AS EMPRESA,
		sec.Descripcion as Sector,
		ts.Descripcion as TipoServicio,
		c.Codigo + '' '' + vi.NroCodificacion as Trailer,
		isnull(R.idRemito,0) as idRemito,
		isnull(PE.idParteEntrega,0) as idParte,
		isnull(impreso,0) as impreso,
		ISNULL(anulado,0) as anulado
		
FROM  OrdenTrabajo OT
INNER JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
LEFT JOIN RemitoDetalle RD ON RD.IDPARTE = PE.idParteEntrega
LEFT JOIN REMITOS R ON R.idRemito = RD.idRemito AND R.anulado = 0
LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
left join Solicitante s on (s.idSolicitante = SC.idSolicitante or SC.idSolicitante is null)
							and (s.idSector = SC.idSector)	
left join Sector sec on (sec.idSector = s.idSector or s.idSector is null)
						and (sec.idSector = SC.idSector)
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0 
AND SC.Baja = 0
AND (R.nroRemito is null or R.nroRemito <> 0)
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)		
		AND (PE.NroParte = @nroParte or @nroParte is null)
		AND (v.idVehiculo = @idTRailer or @idTRailer is null)
		AND (SC.idSector = @idSector OR @idSector IS NULL)
		AND (SC.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
		AND (R.NROREMITO = @NROREMITO OR @NROREMITO IS NULL)

ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosBonificadosGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosBonificadosGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosBonificadosGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer int = null,
	@idSector int,
	@idTipoServicio int,
	@idCondicion int,
	@nroRemito int = null
		
)
AS
BEGIN

SELECT	
		SC.idSolicitud,		
		isnull(sc.NroPedidoCliente,str(SC.NroSolicitud) +'' ''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud)) as NroSolicitud,
		sc.NroPedidoCliente,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		PE.NroParte as NroParte,
		R.nroRemito as nroRemito,
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		ISNULL(OT.idOrden,0) as idOrden,
		ISNULL(OT.idTrailer,0) as idTrailer, 
		e.Descripcion AS EMPRESA,
		sec.Descripcion as Sector,
		ts.Descripcion as TipoServicio,
		c.Codigo + '' '' + vi.NroCodificacion as Trailer,
		isnull(R.idRemito,0) as idRemito,
		isnull(PE.idParteEntrega,0) as idParte,
		isnull(impreso,0) as impreso,
		ISNULL(anulado,0) as anulado
		
FROM  OrdenTrabajo OT
INNER JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
LEFT JOIN RemitoDetalle RD ON RD.IDPARTE = PE.idParteEntrega
LEFT JOIN REMITOS R ON R.idRemito = RD.idRemito AND R.anulado = 0
LEFT join Vehiculo v on v.idVehiculo = OT.idTrailer
LEFT join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
LEFT join Codificacion c on c.idCodificacion = vi.idCodificacion 
left join Solicitante s on (s.idSolicitante = SC.idSolicitante and s.idSector = SC.idSector)	
left join Sector sec on (sec.idSector = SC.idSector)
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0 
AND SC.Baja = 0
AND R.nroRemito = 0
		AND (SC.NroSolicitud = @NroSolicitud OR @NroSolicitud IS NULL )
		AND (SC.Fecha >= @fechaDesde OR @fechaDesde IS NULL)
		AND (SC.Fecha <= @fechaHasta OR @fechaHasta IS NULL)
		AND (e.IDEMPRESA = @idEmpresa OR @idEmpresa IS NULL)		
		AND (PE.NroParte = @nroParte or @nroParte is null)
		AND (v.idVehiculo = @idTRailer or @idTRailer is null)
		AND (SC.idSector = @idSector OR @idSector IS NULL)
		AND (SC.idTipoServicio = @idTipoServicio OR @idTipoServicio IS NULL)
		AND (SC.idCondicion = @idCondicion OR @idCondicion IS NULL)
		AND (R.NROREMITO = @NROREMITO OR @NROREMITO IS NULL)

ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitoGetRPT]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitoGetRPT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitoGetRPT]
(
	@IDREMITO INT
)

AS

BEGIN

SELECT R.nroRemito,
		Convert(varchar,R.fecha,106) as fecha,
		R.observacion,
		RD.Cantidad,
		RD.Detalle,
		RD.PrecioUnit,
		RD.PrecioTotal,
		PE.NroParte,
		SC.NroSolicitud,
		E.RazonSocial,
		E.Domicilio,
		E.CUIT,
		E.CondicionIVA,
		S.Descripcion as Sector,
		SOL.Apellido + '', '' + SOL.Nombre AS SOLICITANTE,
		R.CCosto,
		R.NroOC
FROM Remitos R
INNER JOIN RemitoDetalle RD ON RD.idRemito = R.idRemito
INNER JOIN ParteEntrega PE ON PE.idParteEntrega = Rd.idParte
INNER JOIN OrdenTrabajo OT ON OT.idOrden = PE.idOrdenTrabajo
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
INNER JOIN Sector S ON S.idSector = SC.idSector
INNER JOIN Empresa E ON E.idEmpresa = S.idEmpresa
LEFT JOIN Solicitante SOL ON SOL.idSolicitante = SC.idSolicitante
WHERE R.idRemito = @IDREMITO

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[OrdenTrabajoGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrdenTrabajoGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[OrdenTrabajoGetByFilter] 
(
	@NroSolicitud int,
	@idEmpresa int,
	@fechaDesde datetime,
	@fechaHasta datetime,
	@nroParte int,
	@idTRailer inT = null	
)
AS
BEGIN
DECLARE 
	
	@NroSolicitudPARAM int,
	@idEmpresaPARAM int,
	@fechaDesdePARAM datetime,
	@fechaHastaPARAM datetime,
	@nroPartePARAM int,
	@idTRailerPARAM int 
	
SET @NroSolicitudPARAM = @NroSolicitud
SET @idEmpresaPARAM = @idEmpresa
SET @fechaDesdePARAM  = @fechaDesde
SET @fechaHastaPARAM = @fechaHasta 
SET @nroPartePARAM = @nroParte 
SET @idTRailerPARAM = @idTRailer
	
SELECT	OT.idOrden,
		OT.idTrailer, 
		ISNULL(SC.NroPedidoCliente, str(SC.NroSolicitud) +''-''+ DBO.ABC(SC.NroSolicitud, SC.idSolicitud))as NroSolicitud,		
		CONVERT(VARchar(10), SC.Fecha, 103) AS FECHA,		
		e.Descripcion AS EMPRESA,
		sec.Descripcion as SOLICITANTE,
		ts.Descripcion as TipoServicio,
		c.Codigo +'' '' + vi.NroCodificacion as Trailer,
		SC.idSolicitud,
		ISNULL(PE.idParteEntrega,0) as idParteEntrega,
		isnull(SC.Entrega, 1) as entrega,
		PE.NroParte
FROM  OrdenTrabajo OT
LEFT JOIN ParteEntrega PE ON PE.idOrdenTrabajo = OT.idOrden
left join Vehiculo v on v.idVehiculo = OT.idTrailer
left join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
left join Codificacion c on c.idCodificacion = vi.idCodificacion
	
INNER JOIN SolicitudCliente SC ON SC.idSolicitud = OT.idSolicitudCliente
left join Solicitante s on (s.idSolicitante = SC.idSolicitante and s.idSector = SC.idSector)	
left join Sector sec on sec.idSector = SC.idSector
left join Empresa e on e.idEmpresa = sec.idEmpresa
left join TipoServicio ts on ts.idTipoServicio = SC.idTipoServicio
WHERE OT.Baja = 0
		AND (SC.NroSolicitud = @NroSolicitudPARAM OR @NroSolicitudPARAM IS NULL )
		AND (SC.Fecha >= @fechaDesdePARAM OR @fechaDesdePARAM IS NULL)
		AND (SC.Fecha <= @fechaHastaPARAM OR @fechaHastaPARAM IS NULL)
		AND (e.IDEMPRESA = @idEmpresaPARAM OR @idEmpresaPARAM IS NULL)		
		AND (PE.NroParte = @nroPartePARAM or @nroPartePARAM is null)
		AND (v.idVehiculo = @idTRailerPARAM or @idTRailerPARAM is null)
AND SC.Baja = 0
-- and isnull(pe.nroremito,0)=0
ORDER BY SC.Fecha DESC,DBO.ABC(SC.NroSolicitud, SC.idSolicitud) DESC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SexoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SexoUpdate]
(
	@idSexo int,
	@Descripcion varchar(150)
)
AS
BEGIN

	UPDATE [Sexo]
	SET
		[Descripcion] = @Descripcion
	WHERE
		[idSexo] = @idSexo




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SexoGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SexoGetById]
(
	@idSexo int
)
AS
BEGIN

	SELECT
		[idSexo],
		[Descripcion]
	FROM [Sexo]
	WHERE
		([idSexo] = @idSexo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SexoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SexoGetAll]
AS
BEGIN

	SELECT
		[idSexo],
		[Descripcion]
	FROM [Sexo]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SexoDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SexoDelete]
(
	@idSexo int
)
AS
BEGIN

	DELETE
	FROM [Sexo]
	WHERE
		[idSexo] = @idSexo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SexoAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SexoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SexoAdd]
(
	@Descripcion varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [Sexo]
	(
		[Descripcion]
	)
	VALUES
	(
		@Descripcion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorUpdate]
(
	@idSector int,
	@Descripcion nvarchar(150),
	@idEmpresa int
)
AS
BEGIN

	UPDATE [Sector]
	SET
		[Descripcion] = @Descripcion,
		[idEmpresa]	= @idEmpresa
	WHERE
		[idSector] = @idSector




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorGetById]
(
	@idSector int
)
AS
BEGIN

	SELECT
		[idSector],
		[Descripcion],
		isnull([idEmpresa],0) as idEmpresa
	FROM [Sector]
	WHERE
		([idSector] = @idSector)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAllByIdEmpresaInterna]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAllByIdEmpresaInterna]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorGetAllByIdEmpresaInterna]
AS
BEGIN

	SELECT
		S.[idSector],
		S.[Descripcion],
		isnull(S.[idEmpresa],0) as idEmpresa
	FROM [Sector] S
	INNER JOIN Empresa E ON E.idEmpresa = S.idEmpresa
	WHERE ISNULL(E.interna,0) = 1
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAllByIdEmpresa]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAllByIdEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorGetAllByIdEmpresa]
(
	@IDEMPRESA AS INT
)
AS
BEGIN

	SELECT
		[idSector],
		[Descripcion],
		isnull([idEmpresa],0) as idEmpresa
	FROM [Sector]
	WHERE idEmpresa = @IDEMPRESA OR @IDEMPRESA IS NULL
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorGetAll]
AS
BEGIN

	SELECT
		[idSector],
		[Descripcion],
		isnull([idEmpresa],0) as idEmpresa
	FROM [Sector]
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorDelete]
(
	@idSector int
)
AS
BEGIN

	DELETE
	FROM [Sector]
	WHERE
		[idSector] = @idSector
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SectorAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SectorAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SectorAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@idEmpresa int = null
	
)
AS
BEGIN


	INSERT
	INTO [Sector]
	(
		[Descripcion],
		[idEmpresa]
	)
	VALUES
	(
		@Descripcion,
		@idEmpresa
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleUpdate]
(
	@idPosible int,
	@idSolicitud int,
	@idTipoServicio int,
	@idTrailer int

)
AS
BEGIN

	UPDATE [SolicitudTrailerPosible]
	SET
		[idSolicitud] = @idSolicitud,
		[idTrailer] = @idTrailer,
		[idTipoServicio] = @idTipoServicio
	WHERE
		[idPosible] = @idPosible




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleGetById]
(
	@idPosible int
)
AS
BEGIN

	SELECT
		[idPosible],
		[idSolicitud],
		[idTrailer],
		[idTipoServicio]
	FROM [SolicitudTrailerPosible]
	WHERE
		([idPosible] = @idPosible)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleGetAll]
AS
BEGIN

	SELECT
		[idPosible],
		[idSolicitud],
		[idTrailer],
		[idTipoServicio]
	FROM [SolicitudTrailerPosible]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleGet]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleGet]
(
	@nroSolicitud int,
	@idTipoServicio int
)

AS

BEGIN

	SELECT distinct v.idVehiculo,
			c.Codigo+ '' '' + vi.NroCodificacion as Descripcion,
			v.idMarca,
			v.Modelo,
			v.Anio,
			v.Patente,
			v.idEmpresa,
			v.idSector,
			v.fechaAltaEmpresa,
			v.fechaBajaEmpresa,
			v.idUsuario,
			v.fechaAlta,
			v.Titular,
			v.PotenciaMotor,
			v.Color,
			v.NroChasis,
			v.esEquipo,
			v.NroMotor
	FROM Vehiculo v 
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion 
	INNER JOIN SolicitudTrailerPosible STP ON v.idVehiculo = STP.idTrailer
	WHERE idSolicitud = @nroSolicitud
	AND idTipoServicio = @idTipoServicio

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleDelete]
(
	@idPosible int
)
AS
BEGIN

	DELETE
	FROM [SolicitudTrailerPosible]
	WHERE
		[idPosible] = @idPosible
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudTrailerPosibleAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudTrailerPosibleAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudTrailerPosibleAdd]
(
	@idSolicitud int,
	@idTrailer int,
	@idTipoServicio int
)
AS
BEGIN


	INSERT
	INTO [SolicitudTrailerPosible]
	(
		[idSolicitud],
		[idTrailer],
		[idTipoServicio]
	)
	VALUES
	(
		@idSolicitud,
		@idTrailer,
		@idTipoServicio
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioDelete]
(
	@idTipoServicio int
)
AS
BEGIN

	UPDATE [TipoServicio]
	SET BAJA = 1
	WHERE
		[idTipoServicio] = @idTipoServicio
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@codTango varchar(50) = null,
	@Abreviacion varchar(10),
	@RequierePersonal bit,
	@idSector int,
	@RequiereKms bit,
	@RequiereMaterial bit,
	@RequiereCuadrilla bit,
	@MasEquipo bit,
    @RequiereTraslado bit,
    @RequiereEquipo bit,
    @RequiereKmsTaco bit,
    @RequiereChofer bit
)
AS
BEGIN


	INSERT
	INTO [TipoServicio]
	(	[Descripcion],
		[CodTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		[RequiereKms],
		RequiereMaterial,
		RequiereCuadrilla,		
		MasEquipo,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	)
	VALUES
	(	@Descripcion,
		@codTango,
		@Abreviacion,
		@RequierePersonal,
		@idSector,
		@RequiereKms,
		@RequiereMaterial,
		@RequiereCuadrilla,
		@MasEquipo,
		@RequiereTraslado,
		@RequiereEquipo ,
		@RequiereKmsTaco,
		@RequiereChofer 
		
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

/****** Object:  StoredProcedure [dbo].[TipoServicioGetAll]    Script Date: 10/06/2015 09:12:16 ******/
SET ANSI_NULLS ON
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudEspecificacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudEspecificacionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudEspecificacionDelete]
	@idSolicitud INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DELETE FROM [SolicitudClienteEspecificacion]
   WHERE IDSOLICITUDCLIENTE = @idSolicitud
   
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudEspecificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudEspecificacionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudEspecificacionAdd]
	@idEspecificacion int,
	@idSolicitud int
AS
BEGIN

    INSERT INTO SolicitudClienteEspecificacion
			([idEspecificacion]
           ,[idSolicitudCliente])
     VALUES
           (@idEspecificacion
           ,@idSolicitud)
           
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCuadrillaPosibleGet]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitudCuadrillaPosibleGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitudCuadrillaPosibleGet]
(
	@nroSolicitud int,
	@idTipoServicio int
)

AS

BEGIN

	SELECT distinct c.idCuadrilla,
			c.Nombre, 
			c.FechaAlta			
	FROM Cuadrilla c
	INNER JOIN SolicitudTrailerPosible STP ON c.idCuadrilla = STP.idTrailer
	WHERE idSolicitud = @nroSolicitud
	AND idTipoServicio = @idTipoServicio

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VerificaParteUpdate]
	@idVerifica int,
	@idParte int,
	@Cantidad decimal(18,2)
AS
BEGIN
	
	UPDATE VerificaParte				
	SET Cantidad = @Cantidad
	WHERE
		idVerifica = @idVerifica
		AND idParte = @idParte
		
	

	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteGetByIdParte]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteGetByIdParte]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VerificaParteGetByIdParte]
	@idParte int
AS
BEGIN

	SELECT	VP.idParte,
			VP.idVerifica,
			VP.Cantidad,
			V.descripcion
	FROM VerificaParte VP
	INNER JOIN Verifica V ON V.idVerifica = VP.idVerifica
	WHERE idParte = @idParte

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VerificaParteGetById]
(
	@IdVerifica int,
	@IdParte INT
)
AS
BEGIN

	SELECT	idVerifica,
		idParte,
		Cantidad
	FROM	VerificaParte
	WHERE
		idVerifica = @IdVerifica
		AND idParte = @IdParte

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VerificaParteAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaParteAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VerificaParteAdd]
	@idVerifica int,
	@idParte int,
	@Cantidad decimal(18,2)
AS
BEGIN
	
	INSERT
	INTO VerificaParte
	(
		idVerifica,
		idParte,
		Cantidad
	)
	VALUES
	(
		@idVerifica,
		@idParte,
		@Cantidad
	)

	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VerificaGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerificaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VerificaGetAll]
AS
BEGIN

	SELECT
		idVerifica,
		descripcion
		
	FROM Verifica
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoUsuariosGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoUsuariosGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoUsuariosGetById]
(
	@IDTipoUsuario INT
)
AS
BEGIN
	SELECT idTipoUsuario,
			Descripcion
	FROM TipoUsuario
	WHERE idTipoUsuario = @IDTipoUsuario
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoUsuarioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoUsuarioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoUsuarioGetAll]
AS
BEGIN
	SELECT idTipoUsuario,
			Descripcion
	FROM TipoUsuario
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoUpdate]
(
	@idVencimiento int,
	@idVehiculo int,
	@vencimientoCedula datetime,
	@vencimientoPatente datetime,
	@vencimientoLeasing datetime,
	@NroVTV varchar(50),
	@vencimientoVTV datetime,
	@RUTA bit,
	@NroExpediente varchar(50),
	@emision datetime,
	@vencimiento datetime
)
AS
BEGIN

	UPDATE [VehiculoVencimiento]
	SET
		[idVehiculo] = @idVehiculo,
		[vencimientoCedula] = @vencimientoCedula,
		[vencimientoPatente] = @vencimientoPatente,
		[vencimientoLeasing] = @vencimientoLeasing,
		[NroVTV] = @NroVTV,
		[vencimientoVTV] = @vencimientoVTV,
		[RUTA] = @RUTA,
		[NroExpediente] = @NroExpediente,
		[emision] = @emision,
		[vencimiento] = @vencimiento
	WHERE
		[idVencimiento] = @idVencimiento




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetByIdVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoGetByIdVehiculo]
(
	@idVehiculo int
)
AS
BEGIN

	SELECT
		[idVehiculo],
		[idVencimiento],
		[vencimiento],
		[vencimientoCedula],
		[vencimientoLeasing],
		[vencimientoPatente],
		[NroVTV],
		[vencimientoVTV],
		[RUTA],
		[NroExpediente],
		[emision]
	FROM [VehiculoVencimiento] VV
	WHERE
		([idVehiculo] = @idVehiculo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoGetById]
(
	@idVencimiento int
)
AS
BEGIN

	SELECT
		[idVencimiento],
		[idVehiculo],
		[vencimientoCedula],
		[vencimientoPatente],
		[vencimientoLeasing],
		[NroVTV],
		[vencimientoVTV],
		[RUTA],
		[NroExpediente],
		[emision],
		[vencimiento]
	FROM [VehiculoVencimiento]
	WHERE
		([idVencimiento] = @idVencimiento)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoGetAll]
AS
BEGIN

	SELECT
		[idVencimiento],
		[idVehiculo],
		[vencimientoCedula],
		[vencimientoPatente],
		[vencimientoLeasing],
		[NroVTV],
		[vencimientoVTV],
		[RUTA],
		[NroExpediente],
		[emision],
		[vencimiento]
	FROM [VehiculoVencimiento]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoDelete]
(
	@idVencimiento int
)
AS
BEGIN

	DELETE
	FROM [VehiculoVencimiento]
	WHERE
		[idVencimiento] = @idVencimiento
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoVencimientoAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoVencimientoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoVencimientoAdd]
(
	@idVehiculo int,
	@vencimientoCedula datetime = NULL,
	@vencimientoPatente datetime = NULL,
	@vencimientoLeasing datetime = NULL,
	@NroVTV varchar(50) = NULL,
	@vencimientoVTV datetime = NULL,
	@RUTA bit = NULL,
	@NroExpediente varchar(50) = NULL,
	@emision datetime = NULL,
	@vencimiento datetime = NULL
)
AS
BEGIN


	INSERT
	INTO [VehiculoVencimiento]
	(
		[idVehiculo],
		[vencimientoCedula],
		[vencimientoPatente],
		[vencimientoLeasing],
		[NroVTV],
		[vencimientoVTV],
		[RUTA],
		[NroExpediente],
		[emision],
		[vencimiento]
	)
	VALUES
	(
		@idVehiculo,
		@vencimientoCedula,
		@vencimientoPatente,
		@vencimientoLeasing,
		@NroVTV,
		@vencimientoVTV,
		@RUTA,
		@NroExpediente,
		@emision,
		@vencimiento
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoDelete]
(
	@idVehiculo int
)
AS
BEGIN

	DELETE
	FROM [Vehiculo]
	WHERE
		[idVehiculo] = @idVehiculo
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoAdd]
(
	@idMarca int,
	@Modelo varchar(50) = NULL,
	@Anio int = NULL,
	@Patente varchar(50) = NULL,
	@idEmpresa int = NULL,
	@idSector int = NULL,
	@fechaAltaEmpresa datetime = NULL,
	@fechaBajaEmpresa datetime = NULL,
	@idUsuario int = NULL,
	@fechaAlta datetime = NULL,
	@Titular varchar(150) = NULL,
	@PotenciaMotor varchar(50) = NULL,
	@Color varchar(50) = NULL,
	@NroMotor varchar(50) = NULL,
	@NroChasis varchar(50) = NULL,
	@Alta bit = null,
	@Baja bit = null,
	@esEquipo bit = null
)
AS
BEGIN


	INSERT
	INTO [Vehiculo]
	(
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		[alta],
		[baja],
		[esEquipo]
	)
	VALUES
	(
		@idMarca,
		@Modelo,
		@Anio,
		@Patente,
		@idEmpresa,
		@idSector,
		@fechaAltaEmpresa,
		@fechaBajaEmpresa,
		@idUsuario,
		@fechaAlta,
		@Titular,
		@PotenciaMotor,
		@Color,
		@NroMotor,
		@NroChasis,
		@Alta,
		@Baja,
		@esEquipo
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoUpdate]
(
	@idVehiculo int,
	@idMarca int,
	@Modelo varchar(50),
	@Anio int,
	@Patente varchar(50),
	@idEmpresa int,
	@idSector int,
	@fechaAltaEmpresa datetime,
	@fechaBajaEmpresa datetime,
	@idUsuario int,
	@fechaAlta datetime,
	@Titular varchar(150),
	@PotenciaMotor varchar(50),
	@Color varchar(50),
	@NroMotor varchar(50),
	@NroChasis varchar(50),
	@Alta bit,
	@Baja bit,
	@esEquipo bit
)
AS
BEGIN

	UPDATE [Vehiculo]
	SET
		[idMarca] = @idMarca,
		[Modelo] = @Modelo,
		[Anio] = @Anio,
		[Patente] = @Patente,
		[idEmpresa] = @idEmpresa,
		[idSector] = @idSector,
		[fechaAltaEmpresa] = @fechaAltaEmpresa,
		[fechaBajaEmpresa] = @fechaBajaEmpresa,
		[idUsuario] = @idUsuario,
		[fechaAlta] = @fechaAlta,
		[Titular] = @Titular,
		[PotenciaMotor] = @PotenciaMotor,
		[Color] = @Color,
		[NroMotor] = @NroMotor,
		[NroChasis] = @NroChasis,
		[alta] = @Alta,
		[baja] = @Baja,
		[esEquipo] = @esEquipo
	WHERE
		[idVehiculo] = @idVehiculo




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroUpdate]
(
	@idSeguro int,
	@idVehiculo int,
	@CompaniaAseg bit,
	@NroPoliza varchar(50),
	@vigenciaDesde datetime,
	@vigenciaHasta datetime
)
AS
BEGIN

	UPDATE [VehiculoSeguro]
	SET
		[idVehiculo] = @idVehiculo,
		[CompaniaAseg] = @CompaniaAseg,
		[NroPoliza] = @NroPoliza,
		[vigenciaDesde] = @vigenciaDesde,
		[vigenciaHasta] = @vigenciaHasta
	WHERE
		[idSeguro] = @idSeguro




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetByIdVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroGetByIdVehiculo]
(
	@idVehiculo int
)
AS
BEGIN

	SELECT
		[idVehiculo],
		[idSeguro],
		[CompaniaAseg],
		[NroPoliza],
		[vigenciaDesde],
		[vigenciaHasta]
	FROM [VehiculoSeguro] VS
	WHERE
		([idVehiculo] = @idVehiculo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroGetById]
(
	@idSeguro int
)
AS
BEGIN

	SELECT
		[idSeguro],
		[idVehiculo],
		[CompaniaAseg],
		[NroPoliza],
		[vigenciaDesde],
		[vigenciaHasta]
	FROM [VehiculoSeguro]
	WHERE
		([idSeguro] = @idSeguro)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroGetAll]
AS
BEGIN

	SELECT
		[idSeguro],
		[idVehiculo],
		[CompaniaAseg],
		[NroPoliza],
		[vigenciaDesde],
		[vigenciaHasta]
	FROM [VehiculoSeguro]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroDelete]
(
	@idSeguro int
)
AS
BEGIN

	DELETE
	FROM [VehiculoSeguro]
	WHERE
		[idSeguro] = @idSeguro
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoSeguroAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoSeguroAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoSeguroAdd]
(
	@idVehiculo int,
	@CompaniaAseg bit = NULL,
	@NroPoliza varchar(50) = NULL,
	@vigenciaDesde datetime = NULL,
	@vigenciaHasta datetime = NULL
)
AS
BEGIN


	INSERT
	INTO [VehiculoSeguro]
	(
		[idVehiculo],
		[CompaniaAseg],
		[NroPoliza],
		[vigenciaDesde],
		[vigenciaHasta]
	)
	VALUES
	(
		@idVehiculo,
		@CompaniaAseg,
		@NroPoliza,
		@vigenciaDesde,
		@vigenciaHasta
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionUpdate]
(
	@idIdentificacion int,
	@idVehiculo int,
	@idCodificacion int,
	@NroTacografo varchar(50),
	@Alta bit,
	@Baja bit,
	@NroCodif varchar(50)
)
AS
BEGIN

	UPDATE [VehiculoIdentificacion]
	SET
		[idVehiculo] = @idVehiculo,
		[idCodificacion] = @idCodificacion,
		[NroTacografo] = @NroTacografo,
		[Alta] = @Alta,
		[Baja] = @Baja,
		[NroCodificacion] = @NroCodif
	WHERE
		[idIdentificacion] = @idIdentificacion




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetByIdVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionGetByIdVehiculo]
(
	@idVehiculo int
)
AS
BEGIN

	SELECT
		[idVehiculo],
		[idIdentificacion],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion] 
	FROM [VehiculoIdentificacion] VI
	WHERE
		([idVehiculo] = @idVehiculo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionGetById]
(
	@idIdentificacion int
)
AS
BEGIN

	SELECT
		[idIdentificacion],
		[idVehiculo],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion]
	FROM [VehiculoIdentificacion]
	WHERE
		([idIdentificacion] = @idIdentificacion)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetAllBaja]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetAllBaja]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionGetAllBaja]
AS
BEGIN

	SELECT
		[idIdentificacion],
		[idVehiculo],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion] 
	FROM [VehiculoIdentificacion]
	WHERE ([Baja] = 0)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionGetAll]
AS
BEGIN

	SELECT
		[idIdentificacion],
		[idVehiculo],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion] 
	FROM [VehiculoIdentificacion]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionDelete]
(
	@idIdentificacion int
)
AS
BEGIN

	DELETE
	FROM [VehiculoIdentificacion]
	WHERE
		[idIdentificacion] = @idIdentificacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoIdentificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoIdentificacionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoIdentificacionAdd]
(
	@idVehiculo int,
	@idCodificacion int,
	@NroTacografo varchar(50) = NULL,
	@Alta bit = NULL,
	@Baja bit = NULL,
	@NroCodif varchar(50)
)
AS
BEGIN


	INSERT
	INTO [VehiculoIdentificacion]
	(
		[idVehiculo],
		[idCodificacion],
		[NroTacografo],
		[Alta],
		[Baja],
		[NroCodificacion]
	)
	VALUES
	(
		@idVehiculo,
		@idCodificacion,
		@NroTacografo,
		@Alta,
		@Baja,
		@NroCodif
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoHistorialGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoHistorialGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoHistorialGetByFilter]
	(
		@patente varchar(250),
		@ruta varchar(250),
		@vCedula int,
		@vPatente int,
		@vtv varchar(50),
		@idCodificacion int,
		@idEmpresa int
		
	)
	
AS

BEGIN

	SELECT V.idVehiculo,
		V.Patente,		
		E.RazonSocial AS EMPRESA,
		M.Descripcion AS MARCA,
		SE.Descripcion AS Sector,
		CONVERT(VARCHAR(10),V.fechaBajaEmpresa, 103) AS fechaBaja
		
		
	FROM Vehiculo V
	INNER JOIN VehiculoIdentificacion vi on vi.idVehiculo  = v.idVehiculo
	INNER JOIN VehiculoVencimiento VV ON V.idVehiculo = VV.idVehiculo 
	INNER JOIN Codificacion C ON C.idCodificacion = vi.idCodificacion
	INNER JOIN EMPRESA E ON E.idEmpresa = V.idEmpresa
	INNER JOIN Sector SE ON SE.idSector = V.idSector
													
	left JOIN Marca M ON M.idMarca = V.idMarca
	left JOIN VehiculoSeguro VS ON VS.idVehiculo = V.idVehiculo
				
	WHERE
	V.baja = 1
	AND V.esEquipo = 0
	AND (C.idCodificacion = @idCodificacion OR @idCodificacion IS NULL)
	AND (V.Patente LIKE ''%'' + @patente + ''%'' OR (@patente IS NULL) )
	AND (V.idEmpresa = @idEmpresa or @idEmpresa is NULL)	
	AND (VV.vencimientoCedula >= @vCedula OR @vCedula IS NULL)
	AND (VV.vencimientoPatente >= @vPatente OR @vPatente IS NULL)
	AND (VV.NroVTV = @vtv OR @vtv IS NULL)
	AND (VV.RUTA = @ruta OR @ruta IS NULL)
	
		
	ORDER BY V.Patente desc

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetVehiculos]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetVehiculos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGetVehiculos]
AS
BEGIN

	SELECT
		v.[idVehiculo],
		v.[idMarca],
		v.[Modelo],
		v.[Anio],
		v.[Patente],
		v.[idEmpresa],
		v.[idSector],
		v.[fechaAltaEmpresa],
		v.[fechaBajaEmpresa],
		v.[idUsuario],
		v.[fechaAlta],
		v.[Titular],
		v.[PotenciaMotor],
		v.[Color],
		v.[NroMotor],
		v.[NroChasis],
		v.[alta],
		v.[baja] ,
		v.esEquipo,
		c.Codigo + '' '' + vi.NroCodificacion as Codificacion
	FROM [Vehiculo] v
	INNER JOIN VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	WHERE
		v.[esEquipo] = 0
		and v.baja = 0
	ORDER BY c.Codigo, vi.NroCodificacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetEquipos]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetEquipos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGetEquipos]
AS
BEGIN

	SELECT
		v.[idVehiculo],
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		v.[alta],
		v.[baja] ,
		esEquipo,
		c.Codigo + '' '' + convert(varchar,vi.NroCodificacion) as codificacion
	FROM [Vehiculo] v
	inner join VehiculoVencimiento vv on vv.idVehiculo = v.idVehiculo
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	WHERE
		[esEquipo] = 1
		and v.baja = 0
		and (vv.vencimiento IS NULL OR vv.vencimiento > GETDATE())
		and (vv.vencimientoPatente  IS NULL OR vv.vencimientoPatente > GETDATE())
	ORDER BY C.Descripcion, vi.NroCodificacion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetByIdSector]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetByIdSector]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGetByIdSector]
(
	@idSector int,
	@idEmpresa int
)
AS
BEGIN

	SELECT distinct
		v.[idVehiculo],
		v.[idMarca],
		v.[Modelo],
		v.[Anio],
		v.[Patente] + '' - '' + c.Descripcion as descripcion,
		v.[Patente],
		v.[idEmpresa],
		v.[idSector],
		v.[fechaAltaEmpresa],
		v.[fechaBajaEmpresa],
		v.[idUsuario],
		v.[fechaAlta],
		v.[Titular],
		v.[PotenciaMotor],
		v.[Color],
		v.[NroMotor],
		v.[NroChasis]
	FROM [Vehiculo] v
	inner join VehiculoVencimiento vv on vv.idVehiculo = v.idVehiculo
	inner join VehiculoIdentificacion vi on vi.idVehiculo = v.idVehiculo
	inner join Codificacion c on c.idCodificacion = vi.idCodificacion
	WHERE
		v.esEquipo = 0
		and (v.[idSector] = @idSector)
		and v.idEmpresa = @idEmpresa
		and vv.vencimiento > GETDATE()
		and vv.vencimientoCedula > GETDATE()

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGetById]
(
	@idVehiculo int
)
AS
BEGIN

	SELECT
		[idVehiculo],
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		[alta],
		[baja] ,
		[esEquipo]
	FROM [Vehiculo]
	WHERE
		[idVehiculo] = @idVehiculo

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGetAll]
AS
BEGIN

	SELECT
		[idVehiculo],
		[idMarca],
		[Modelo],
		[Anio],
		[Patente],
		[idEmpresa],
		[idSector],
		[fechaAltaEmpresa],
		[fechaBajaEmpresa],
		[idUsuario],
		[fechaAlta],
		[Titular],
		[PotenciaMotor],
		[Color],
		[NroMotor],
		[NroChasis],
		[alta],
		[baja] ,
		esEquipo
	FROM [Vehiculo]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoGestionGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoGestionGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoGestionGetByFilter]
	(
		@patente varchar(250),
		@ruta varchar(250),
		@vCedula int,
		@vPatente int,
		@vtv varchar(50),
		@idCodificacion int,
		@idEmpresa int
		
	)
	
AS

BEGIN

	SELECT V.idVehiculo,
		V.Patente,		
		E.RazonSocial AS EMPRESA,
		M.Descripcion AS MARCA,
		SE.Descripcion AS Sector,
		(C.Codigo) + '' ''+ CONVERT(VARCHAR(10),vi.NroCodificacion) AS Codificacion ,
		CONVERT(VARCHAR(10),VV.vencimientoCedula, 103) AS vencimientoCedula,
		CONVERT(VARCHAR(10),VV.vencimientoPatente, 103) AS vencimientoPatente,
		CONVERT(VARCHAR(10),VV.vencimientoLeasing, 103) AS vencimientoLeasing,
		CONVERT(VARCHAR(10),VV.vencimientoVTV, 103) AS vencimientoVTV,
		CONVERT(VARCHAR(10),VV.vencimiento, 103) AS vencimiento,
		CONVERT(VARCHAR(10),VS.vigenciaHasta, 103) AS VencimientoSeguro
		
	FROM Vehiculo V
	left JOIN VehiculoIdentificacion vi on vi.idVehiculo  = v.idVehiculo
	INNER JOIN VehiculoVencimiento VV ON V.idVehiculo = VV.idVehiculo 
	left JOIN Codificacion C ON C.idCodificacion = vi.idCodificacion
	INNER JOIN EMPRESA E ON E.idEmpresa = V.idEmpresa
	INNER JOIN Sector SE ON SE.idSector = V.idSector
													
	left JOIN Marca M ON M.idMarca = V.idMarca
	left JOIN VehiculoSeguro VS ON VS.idVehiculo = V.idVehiculo
				
	WHERE
	(V.esEquipo = 0)
	AND (V.baja = 0)
	AND (C.idCodificacion = @idCodificacion OR @idCodificacion IS NULL)
	AND (V.Patente LIKE ''%'' + @patente + ''%'' OR @patente IS NULL)
	AND (V.idEmpresa = @idEmpresa or @idEmpresa is NULL)	
	AND (VV.vencimientoCedula >= @vCedula OR @vCedula IS NULL or VV.vencimientoCedula is null)
	AND (VV.vencimientoPatente >= @vPatente OR @vPatente IS NULL or VV.vencimientoPatente is null)
	AND (VV.NroVTV = @vtv OR @vtv IS NULL)
	AND (VV.RUTA = @ruta OR @ruta IS NULL)
	
		
	ORDER BY V.Patente desc

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesUpdate]
(
	@idVehiculoFiles int,
	@idVehiculo int,
	@Nombre varchar(150),
	@Ruta varchar(150)
)
AS
BEGIN

	UPDATE [VehiculoFiles]
	SET
		[idVehiculo] = @idVehiculo,
		[Nombre] = @Nombre,
		[Ruta] = @Ruta
	WHERE
		[idVehiculoFiles] = @idVehiculoFiles




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetByIdVehiculo]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetByIdVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesGetByIdVehiculo]
(
	@idVehiculo int
)
AS
BEGIN

	SELECT
		[idVehiculoFiles],
		[idVehiculo],
		[Nombre],
		[Ruta]
	FROM [VehiculoFiles]VF
	WHERE
		(VF.idVehiculo = @idVehiculo)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesGetById]
(
	@idVehiculoFiles int
)
AS
BEGIN

	SELECT
		[idVehiculoFiles],
		[idVehiculo],
		[Nombre],
		[Ruta]
	FROM [VehiculoFiles]
	WHERE
		([idVehiculoFiles] = @idVehiculoFiles)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesGetAll]
AS
BEGIN

	SELECT
		[idVehiculoFiles],
		[idVehiculo],
		[Nombre],
		[Ruta]
	FROM [VehiculoFiles]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesDelete]
(
	@idVehiculoFiles int
)
AS
BEGIN

	DELETE
	FROM [VehiculoFiles]
	WHERE
		[idVehiculoFiles] = @idVehiculoFiles
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[VehiculoFilesAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehiculoFilesAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[VehiculoFilesAdd]
(
	@idVehiculo int,
	@Nombre varchar(150),
	@Ruta varchar(150) = NULL
)
AS
BEGIN


	INSERT
	INTO [VehiculoFiles]
	(
		[idVehiculo],
		[Nombre],
		[Ruta]
	)
	VALUES
	(
		@idVehiculo,
		@Nombre,
		@Ruta
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  View [dbo].[v_Acuerdo]    Script Date: 12/03/2015 17:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_Acuerdo]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[v_Acuerdo]
AS
SELECT     dbo.Acuerdo.idAcuerdo, dbo.Acuerdo.titulo, dbo.Acuerdo.nroAcuerdo, dbo.Acuerdo.idCliente, dbo.Acuerdo.nroRevision, dbo.AcuerdoConceptoCosto.idAcuerdoConcepto, 
                      dbo.AcuerdoConceptoCosto.Concepto, dbo.AcuerdoConceptoCosto.Costo, dbo.AcuerdoConceptoCosto.idEspecificacion, dbo.AcuerdoConceptoCosto.cantidad, dbo.AcuerdoServicio.idTipoServicio, 
                      dbo.AcuerdoServicio.idAcuerdoServicio, dbo.Acuerdo.idSector
FROM         dbo.Acuerdo INNER JOIN
                      dbo.AcuerdoServicio ON dbo.Acuerdo.idAcuerdo = dbo.AcuerdoServicio.idAcuerdo INNER JOIN
                      dbo.AcuerdoConceptoCosto ON dbo.AcuerdoConceptoCosto.idAcuerdoServicio = dbo.AcuerdoServicio.idAcuerdoServicio LEFT OUTER JOIN
                          (SELECT     MAX(nroRevision) AS NroRevision, nroAcuerdo
                            FROM          dbo.Acuerdo AS Acuerdo_1
                            GROUP BY nroAcuerdo) AS maxA ON maxA.nroAcuerdo = dbo.Acuerdo.nroAcuerdo AND maxA.NroRevision = dbo.Acuerdo.nroRevision
WHERE     (dbo.Acuerdo.baja = 0)
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'v_Acuerdo', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AcuerdoConceptoCosto"
            Begin Extent = 
               Top = 9
               Left = 276
               Bottom = 178
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Acuerdo"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 195
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "maxA"
            Begin Extent = 
               Top = 6
               Left = 746
               Bottom = 96
               Right = 944
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AcuerdoServicio"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 129
               Right = 708
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane2' , N'SCHEMA',N'dbo', N'VIEW',N'v_Acuerdo', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'v_Acuerdo', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO
/****** Object:  StoredProcedure [dbo].[UsuarioUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsuarioUpdate]
(
	@idUsuario int,
	@Nombre nvarchar(150),
	@Apellido nvarchar(150),
	@Telefono nvarchar(50),
	@Direccion nvarchar(150),
	@Clave varchar(150) = NULL,
	@UserName varchar(150) = NULL,
	@IdTipoUsuario int,
	@mail varchar(150)
)
AS
BEGIN

	UPDATE [Usuario]
	SET
		[Nombre] = @Nombre,
		[Apellido] = @Apellido,
		[Telefono] = @Telefono,
		[Direccion] = @Direccion,
		[Clave] = @Clave,
		[UserName] = @UserName,
		[idTipoUsuario] = @idTipoUsuario,
		[mail] = @mail
	WHERE
		[idUsuario] = @idUsuario




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UsuarioGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsuarioGetById]
(
	@idUsuario int
)
AS
BEGIN

	SELECT
		[idUsuario],
		[Nombre],
		[Apellido],
		[Telefono],
		[Direccion],
		[Clave],
		[UserName],
		isnull([idTipoUsuario],0) as idTipoUsuario,
		mail
	FROM [Usuario]
	WHERE
		([idUsuario] = @idUsuario)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UsuarioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsuarioGetAll]
AS
BEGIN

	SELECT
		[idUsuario],
		[Nombre],
		[Apellido],
		[Telefono],
		[Direccion],
		[Clave],
		[UserName],
		isnull([idTipoUsuario],0) as idTipoUsuario,
		mail
	FROM [Usuario]
	WHERE isnull(Baja,0) = 0
	ORDER BY Apellido, Nombre
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UsuarioDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsuarioDelete]
(
	@idUsuario int
)
AS
BEGIN

	UPDATE [Usuario]
	SET Baja = 1
	WHERE
		[idUsuario] = @idUsuario
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[UsuarioAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsuarioAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsuarioAdd]
(
	@Nombre nvarchar(150),
	@Apellido nvarchar(150),
	@Telefono nvarchar(50) = NULL,
	@Direccion nvarchar(150) = NULL,
	@Clave varchar(150) = NULL,
	@UserName varchar(150) = NULL,
	@idTipoUsuario int,
	@mail varchar(150)
)
AS
BEGIN


	INSERT
	INTO [Usuario]
	(
		[Nombre],
		[Apellido],
		[Telefono],
		[Direccion],
		[Clave],
		[UserName],
		[idTipoUsuario],
		[mail]
	)
	VALUES
	(
		@Nombre,
		@Apellido,
		@Telefono,
		@Direccion,
		@Clave,
		@UserName,
		@idTipoUsuario,
		@mail
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[TipoServicioUpdate]
(
	@idTipoServicio int,
	@Descripcion nvarchar(150),
	@codTango varchar(50),
	@Abreviacion varchar(10),
	@RequierePersonal bit,
	@idSector int,
	@RequiereKms bit,
	@RequiereMaterial bit,
	@RequiereCuadrilla bit,
	@MasEquipo bit,
    @RequiereTraslado bit,
    @RequiereEquipo bit,
    @RequiereKmsTaco bit,
    @RequiereChofer bit
)
AS
BEGIN

	UPDATE [TipoServicio]
	SET
		[Descripcion] = @Descripcion,
		codTango = @codTango,
		[Abreviacion] = @Abreviacion,
		[RequierePersonal] = @RequierePersonal,
		[idSector] = @idSector,
		[RequiereKms] = @RequiereKms,
		RequiereMaterial = @RequiereMaterial ,
		RequiereCuadrilla = @RequiereCuadrilla ,
		MasEquipo = @MasEquipo ,
		RequiereTraslado = @RequiereTraslado ,
		RequiereEquipo = @RequiereEquipo ,
		RequiereKmsTaco = @RequiereKmsTaco,
		RequiereChofer =	@RequiereChofer 
		
	WHERE
		[idTipoServicio] = @idTipoServicio


END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[TipoServicioGetById]
(
	@idTipoServicio int
)
AS
BEGIN
	SELECT
		[idTipoServicio],
		[Descripcion],
		[codTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		RequiereKms,
		RequiereMaterial ,
		RequiereCuadrilla ,
		MasEquipo ,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	FROM [TipoServicio]
	WHERE
		([idTipoServicio] = @idTipoServicio)

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioGetByFilter]
	@Descripcion varchar(150)
AS
BEGIN

	SELECT
		[idTipoServicio],
		[idEspecificaciones],
		[Descripcion],
		[codTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		RequiereKms,
		RequiereMaterial,
		RequiereCuadrilla ,
		MasEquipo,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer  
	FROM [TipoServicio]
	WHERE (Descripcion like ''%''+ @Descripcion +''%'' or @Descripcion IS NULL)
	AND ISNULL(BAJA,0) = 0
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByAcuerdoCliente]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetByAcuerdoCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioGetByAcuerdoCliente]
(
	@idCliente int,
	@idSector int
)

AS

BEGIN

	SELECT	distinct acs.idTipoServicio, 
			ts.Descripcion as TipoServicio,
			ts.Abreviacion,
			ts.RequierePersonal,
			ts.idSector,
			ts.RequiereTraslado,
			ts.RequiereCuadrilla,
			ts.RequiereEquipo,
			ts.RequiereKms,
			ts.RequiereKmsTaco
	FROM  Acuerdo a
	inner join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	inner join TipoServicio ts on ts.idTipoServicio= acs.idTipoServicio
	WHERE isnull(a.baja,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0);

	SELECT	distinct acs.idTipoServicio, 
			et.idEspecificaciones as idEspecificacion,
			et.Descripcion as EspecificacionTecnica,
			et.RequiereTraslado
			
	FROM    Acuerdo a
	--inner join (select MAX(isnull(nroRevision,0))as nroRev from acuerdo where idcliente=@idCliente) aa on aa.nroRev = a.nroRevision
	left join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	left join TipoServicioEspecificacion tse on tse.idTipoServicio = acs.idTipoServicio 
	left join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = acs.idAcuerdoServicio
	left join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.RequiereTraslado = 0 --and (et.RequiereCuadrilla = 1 and et.RequiereEquipo = 0)
	WHERE isnull(a.BAJA,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)
	and a.fechaInicio = (select MAX(fechaInicio)as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)		
	
		


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[TipoServicioGetAll]
AS
BEGIN

	SELECT
		[idTipoServicio],
		[Descripcion],
		[codTango],
		[Abreviacion],
		[RequierePersonal],
		[idSector],
		RequiereKms,
		RequiereMaterial,
		RequiereCuadrilla,
		MasEquipo ,
		RequiereTraslado ,
		RequiereEquipo ,
		RequiereKmsTaco,
		RequiereChofer 
	FROM [TipoServicio]
	WHERE ISNULL(BAJA,0) = 0
	ORDER BY Descripcion
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioEspecificacionUpdate]
(
	@idTipoServicio int,
	@idEspecificacion int,
	@idTipoEspecificacion INT
	
)
AS

BEGIN

UPDATE [SIG].[dbo].[TipoServicioEspecificacion]
   SET [idTipoServicio] = @idTipoServicio
      ,[idEspecificacion] = @idEspecificacion
 WHERE [idTipoEspecificacion] = @idTipoEspecificacion

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionGetByIdTipoServicio]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionGetByIdTipoServicio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioEspecificacionGetByIdTipoServicio]
(
	@IdTipoServicio INT
)

AS

BEGIN

	SELECT [idTipoEspecificacion]
		  ,[idTipoServicio]
		  ,[idEspecificacion]
	FROM [SIG].[dbo].[TipoServicioEspecificacion]
	WHERE idTipoServicio = @IdTipoServicio

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioEspecificacionDeleteByIdTipoServicio]
(
	@idTipoServicio int
)
AS

BEGIN

DELETE FROM [TipoServicioEspecificacion]
      WHERE idTipoServicio = @idTipoServicio 

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioEspecificacionAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoServicioEspecificacionAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TipoServicioEspecificacionAdd]
(
	@idEspecificacion INT,
	@idTipoServicio INT
)

AS

BEGIN

INSERT INTO TipoServicioEspecificacion
           ([idTipoServicio]
           ,[idEspecificacion])
     VALUES
           (@idTipoServicio,
            @idEspecificacion)
            

	SELECT CAST(SCOPE_IDENTITY() as INT)


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TransporteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TransporteUpdate]
(
	@idTransporte int,
	@Descripcion nvarchar(150),
	@Marca nvarchar(50),
	@Modelo nvarchar(50),
	@Anio int,
	@ItemVerificacion nvarchar(50),
	@Patente nvarchar(50)
)
AS
BEGIN

	UPDATE [Transporte]
	SET
		[Descripcion] = @Descripcion,
		[Marca] = @Marca,
		[Modelo] = @Modelo,
		[Anio] = @Anio,
		[ItemVerificacion] = @ItemVerificacion,
		[Patente] = @Patente
	WHERE
		[idTransporte] = @idTransporte


END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TransporteGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TransporteGetById]
(
	@idTransporte int
)
AS
BEGIN

	SELECT
		[idTransporte],
		[ItemVerificacion] as Descripcion,
		[Marca],
		[Modelo],
		[Anio],
		[ItemVerificacion],
		[Patente]
	FROM [Transporte]
	WHERE
		([idTransporte] = @idTransporte)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TransporteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TransporteGetAll]
AS
BEGIN

	SELECT
		[idTransporte],
		[ItemVerificacion] as Descripcion,
		[Marca],
		[Modelo],
		[Anio],
		[ItemVerificacion],
		[Patente]
	FROM [Transporte]
	ORDER BY Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TransporteDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TransporteDelete]
(
	@idTransporte int
)
AS
BEGIN

	DELETE
	FROM [Transporte]
	WHERE
		[idTransporte] = @idTransporte
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TransporteAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransporteAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TransporteAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@Marca nvarchar(50) = NULL,
	@Modelo nvarchar(50) = NULL,
	@Anio int = NULL,
	@ItemVerificacion nvarchar(50) = NULL,
	@Patente nvarchar(50) = NULL
)
AS
BEGIN


	INSERT
	INTO [Transporte]
	(
		[Descripcion],
		[Marca],
		[Modelo],
		[Anio],
		[ItemVerificacion],
		[Patente]
	)
	VALUES
	(
		@Descripcion,
		@Marca,
		@Modelo,
		@Anio,
		@ItemVerificacion,
		@Patente
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerUpdate]
(
	@idTrailer int,
	@Descripcion nvarchar(150),
	@idEspecificacion int
)
AS
BEGIN

	UPDATE [Trailer]
	SET
		[Descripcion] = @Descripcion,
		[idEspecificacion] = @idEspecificacion
	WHERE
		[idTrailer] = @idTrailer




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerGetById]
(
	@idTrailer int
)
AS
BEGIN

	SELECT
		[idTrailer],
		[Descripcion],
		[idEspecificacion]
	FROM [Trailer]
	WHERE
		([idTrailer] = @idTrailer)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerGetAll]
AS
BEGIN

	SELECT
		[idTrailer],
		[Descripcion],
		[idEspecificacion]
	FROM [Trailer]
	ORDER BY Len(Descripcion),Descripcion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerDelete]
(
	@idTrailer int
)
AS
BEGIN

	DELETE
	FROM [Trailer]
	WHERE
		[idTrailer] = @idTrailer
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrailerAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrailerAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TrailerAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@idEspecificacion int = NULL
)
AS
BEGIN


	INSERT
	INTO [Trailer]
	(
		[Descripcion],
		[idEspecificacion]
	)
	VALUES
	(
		@Descripcion,
		@idEspecificacion
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosGetAll]
AS
BEGIN

	SELECT
		[idRemito],
		[nroRemito],
		[fecha],
		[idParte],
		[cantidad],
		[observacion],
		[anulado],
		[impreso],
		[idUsuario],
		[fechaAlta],
		NroOC,
		CCosto
	FROM [Remitos]
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosDelete]
(
	@idRemito int
)
AS
BEGIN

	DELETE
	FROM [Remitos]
	WHERE
		[idRemito] = @idRemito
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteUpdate]
(
	@idSolicitante int,
	@Nombre nvarchar(100),
	@Apellido nvarchar(100),
	@idSector int,
	@idEmpresa int,
	@Telefono nvarchar(50)
)
AS
BEGIN

	UPDATE [Solicitante]
	SET
		[Nombre] = @Nombre,
		[Apellido] = @Apellido,
		[idSector] = @idSector,
		[idEmpresa] = @idEmpresa,
		[Telefono] = @Telefono
	WHERE
		[idSolicitante] = @idSolicitante




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteGetById]
(
	@idSolicitante int
)
AS
BEGIN

	SELECT
		[idSolicitante],
		[Nombre],
		[Apellido],
		isnull([idSector],0) as idSector,
		isnull([idEmpresa],0) as idEmpresa,
		[Telefono]
	FROM [Solicitante]
	WHERE
		([idSolicitante] = @idSolicitante)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetByFilter]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetByFilter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteGetByFilter]
( 
	 @IDSECTOR AS INT,
	 @IDEMPRESA AS INT
)
AS
BEGIN

	SELECT
		[idSolicitante],
		[Nombre],
		[Apellido],
		isnull([idSector],0) as idSector,
		isnull([idEmpresa],0) as idEmpresa,
		[Telefono]
	FROM [Solicitante]
	WHERE (idSector = @IDSECTOR OR @IDSECTOR IS NULL)
			AND (idEmpresa = @IDEMPRESA OR @IDEMPRESA IS NULL)			
	ORDER BY apellido
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteGetAll]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteGetAll]
AS
BEGIN

	SELECT
		[idSolicitante],
		[Nombre],
		[Apellido],
		isnull([idSector],0) as idSector,
		isnull([idEmpresa],0) as idEmpresa,
		[Telefono]
	FROM [Solicitante]
order by apellido
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteDelete]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteDelete]
(
	@idSolicitante int
)
AS
BEGIN

	DELETE
	FROM [Solicitante]
	WHERE
		[idSolicitante] = @idSolicitante
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SolicitanteAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SolicitanteAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SolicitanteAdd]
(
	@Nombre nvarchar(100) = NULL,
	@Apellido nvarchar(100) = NULL,
	@idSector int = NULL,
	@idEmpresa int = NULL,
	@Telefono nvarchar(50) = NULL
)
AS
BEGIN


	INSERT
	INTO [Solicitante]
	(
		[Nombre],
		[Apellido],
		[idSector],
		[idEmpresa],
		[Telefono]
	)
	VALUES
	(
		@Nombre,
		@Apellido,
		@idSector,
		@idEmpresa,
		@Telefono
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosUpdate]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosUpdate]
(
	@idRemito int,
	@nroRemito int,
	@fecha datetime,
	@idParte int,
	@cantidad int,
	@observacion varchar(250),
	@anulado bit,
	@impreso bit,
	@idUsuario int,	
	@CCosto varchar(150),
	@NroOC varchar(150)
)
AS
BEGIN

	UPDATE [Remitos]
	SET
		[nroRemito] = @nroRemito,
		[fecha] = @fecha,
		[idParte] = @idParte,
		[cantidad] = @cantidad,
		[observacion] = @observacion,
		[anulado] = @anulado,
		[impreso] = @impreso,
		[idUsuario] = @idUsuario,
		CCosto = @CCosto,
		NroOC = @NroOC
	WHERE
		[idRemito] = @idRemito




END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetUltimaFecha]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetUltimaFecha]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosGetUltimaFecha]
AS
BEGIN

	SELECT ISNULL(MAX(FECHA),''19000101'') AS FECHA
	FROM Remitos
	WHERE anulado = 0
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosGetById]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosGetById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosGetById]
(
	@idRemito int
)
AS
BEGIN

	SELECT
		[idRemito],
		[nroRemito],
		[fecha],
		[idParte],
		[cantidad],
		[observacion],
		[anulado],
		[impreso],
		[idUsuario],
		[fechaAlta],
		NroOC,
		CCosto
	FROM [Remitos]
	WHERE
		([idRemito] = @idRemito)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[RemitosAdd]    Script Date: 12/03/2015 17:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RemitosAdd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[RemitosAdd]
(
	@nroRemito int,
	@fecha datetime,
	@idParte int,
	@cantidad int,
	@observacion varchar(250) = NULL,
	@anulado bit,
	@impreso bit,
	@idUsuario int,	
	@CCosto varchar(150),
	@NroOC varchar(150)
)
AS
BEGIN


	INSERT
	INTO [Remitos]
	(
		[nroRemito],
		[fecha],
		[idParte],
		[cantidad],
		[observacion],
		[anulado],
		[impreso],
		[idUsuario],
		NroOC,
		CCosto
	)
	VALUES
	(
		@nroRemito,
		@fecha,
		@idParte,
		@cantidad,
		@observacion,
		@anulado,
		@impreso,
		@idUsuario,
		@NroOC,
		@CCosto
	)

	UPDATE PARTEENTREGA
	SET NROREMITO = @NROREMITO
	WHERE IDPARTEENTREGA = @idParte

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoServicioUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoServicioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoServicioUpdate]
(
	@idAcuerdoServicio int,
	@idAcuerdo int,
	@idTipoServicio int,
	@requiere varchar(1)
)
AS
BEGIN

	/*UPDATE [AcuerdoServicio]
	SET
		[idAcuerdo] = @idAcuerdo,
		[idTipoServicio] = @idTipoServicio
	WHERE
		[idAcuerdoServicio] = @idAcuerdoServicio*/
		
DELETE FROM [AcuerdoServicio]
WHERE [idAcuerdo] = @idAcuerdo
	AND [idTipoServicio] = @idTipoServicio

EXEC [AcuerdoServicioAdd] @idAcuerdo, @idTipoServicio, @requiere

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByCliente]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoGetByCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoGetByCliente]
(
	@IDSECTOR INT,
	@IDCLIENTE INT	
)
AS
BEGIN

SELECT *
FROM v_Acuerdo
WHERE idCliente = @IDCLIENTE
and idSector = @IDSECTOR

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoConceptoCostoUpdate]    Script Date: 12/03/2015 17:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AcuerdoConceptoCostoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AcuerdoConceptoCostoUpdate]
(
	@idAcuerdoConcepto int,
	@Concepto varchar(150),
	@Costo decimal(18,2),
	@idEspecificacion int,
	@idAcuerdoServicio int
)
AS
BEGIN

	/*UPDATE [AcuerdoConceptoCosto]
	SET
		[Concepto] = @Concepto,
		[Costo] = @Costo,
		[idEspecificacion] = @idEspecificacion,
		[idAcuerdoServicio] = @idAcuerdoServicio
	WHERE
		[idAcuerdoConcepto] = @idAcuerdoConcepto*/

DELETE FROM [AcuerdoConceptoCosto]
WHERE [Concepto] = @Concepto
AND [idEspecificacion] = @idEspecificacion
AND [idAcuerdoServicio] = @idAcuerdoServicio

EXEC [AcuerdoConceptoCostoAdd] @Concepto, @Costo, @idEspecificacion, @idAcuerdoServicio


END
' 
END
GO
