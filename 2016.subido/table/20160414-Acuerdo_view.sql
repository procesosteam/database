USE [SIG_Prod]
GO

/****** Object:  View [dbo].[v_Acuerdo]    Script Date: 04/14/2016 18:10:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[v_Acuerdo]
AS
SELECT     dbo.Acuerdo.idAcuerdo, dbo.Acuerdo.titulo, dbo.Acuerdo.nroAcuerdo, dbo.Acuerdo.idCliente, dbo.Acuerdo.nroRevision, dbo.AcuerdoConceptoCosto.idAcuerdoConcepto, 
                      dbo.AcuerdoConceptoCosto.Concepto, dbo.AcuerdoConceptoCosto.Costo, dbo.AcuerdoConceptoCosto.idEspecificacion, dbo.AcuerdoConceptoCosto.cantidad, dbo.AcuerdoServicio.idTipoServicio, 
                      dbo.AcuerdoServicio.idAcuerdoServicio, dbo.Acuerdo.idSector
FROM         dbo.Acuerdo INNER JOIN
                      dbo.AcuerdoServicio ON dbo.Acuerdo.idAcuerdo = dbo.AcuerdoServicio.idAcuerdo INNER JOIN
                      dbo.AcuerdoConceptoCosto ON dbo.AcuerdoConceptoCosto.idAcuerdoServicio = dbo.AcuerdoServicio.idAcuerdoServicio INNER JOIN
                          (SELECT     MAX(nroRevision) AS NroRevision, nroAcuerdo, idCliente, idSector
                            FROM          dbo.Acuerdo AS Acuerdo_1
                            GROUP BY nroAcuerdo, idCliente, idSector) AS maxA ON maxA.nroAcuerdo = dbo.Acuerdo.nroAcuerdo AND maxA.NroRevision = dbo.Acuerdo.nroRevision AND 
                      maxA.idCliente = dbo.Acuerdo.idCliente AND maxA.idSector = dbo.Acuerdo.idSector
WHERE     (dbo.Acuerdo.baja = 0)

GO

