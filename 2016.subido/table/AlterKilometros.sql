/*
   martes, 12 de abril de 201605:44:56 p.m.
   Usuario: 
   Servidor: localhost
   Base de datos: SIG_Prod
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Lugar SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Kilometros ADD CONSTRAINT
	FK_Kilometros_Lugar FOREIGN KEY
	(
	idLugarDesde
	) REFERENCES dbo.Lugar
	(
	idLugar
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Kilometros ADD CONSTRAINT
	FK_Kilometros_Lugar1 FOREIGN KEY
	(
	idLugarHasta
	) REFERENCES dbo.Lugar
	(
	idLugar
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Kilometros SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
