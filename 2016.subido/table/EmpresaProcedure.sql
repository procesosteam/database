USE [SIG]
GO

/****** Object:  StoredProcedure [dbo].[EmpresaAdd]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaAdd]
(
	@Descripcion nvarchar(150) = NULL,
	@RazonSocial varchar(250) = NULL,
	@Domicilio varchar(150) = NULL,
	@CondicionIVA varchar(50) = NULL,
	@CUIT varchar(50) = NULL,
	@codTango varchar(50) = NULL,
	@interna bit = NULL,
	@baja bit = NULL
)
AS
BEGIN


	INSERT
	INTO [Empresa]
	(
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna],
		[Baja]
	)
	VALUES
	(
		@Descripcion,
		@RazonSocial,
		@Domicilio,
		@CondicionIVA,
		@CUIT,
		@codTango,
		@interna,
		@baja
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaDelete]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaDelete]
(
	@idEmpresa int
)
AS
BEGIN

	UPDATE [Empresa]
	SET baja = 1	
	WHERE
		[idEmpresa] = @idEmpresa
END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaGetAll]    Script Date: 01/14/2016 16:33:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaGetAll]
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna],
		[baja]
	FROM [Empresa]
	WHERE ISNULL(BAJA,0) = 0
	ORDER BY Descripcion
END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaGetAllInternas]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaGetAllInternas]
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna],
		[baja]
	FROM [Empresa]	
	WHERE
		interna=1
		and ISNULL(baja,0)=0
END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaGetByFilter]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaGetByFilter]
(
	@nombre varchar(150)
)
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		isnull([interna],0) as interna
	FROM [Empresa]
	where (Descripcion like '%' + @nombre + '%' or @nombre is null)
	and ISNULL(baja,0)=0
	ORDER BY Descripcion
END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaGetById]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaGetById]
(
	@idEmpresa int
)
AS
BEGIN

	SELECT
		[idEmpresa],
		[Descripcion],
		[RazonSocial],
		[Domicilio],
		[CondicionIVA],
		[CUIT],
		[codTango],
		[interna],
		[baja]
	FROM [Empresa]
	WHERE
		([idEmpresa] = @idEmpresa)

END

GO

/****** Object:  StoredProcedure [dbo].[EmpresaUpdate]    Script Date: 01/14/2016 09:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[EmpresaUpdate]
(
	@idEmpresa int,
	@Descripcion nvarchar(150),
	@RazonSocial varchar(250),
	@Domicilio varchar(150),
	@CondicionIVA varchar(50),
	@CUIT varchar(50),
	@codTango varchar(50),
	@interna bit,
	@baja bit
)
AS
BEGIN

	UPDATE [Empresa]
	SET
		[Descripcion] = @Descripcion,
		[RazonSocial] = @RazonSocial,
		[Domicilio] = @Domicilio,
		[CondicionIVA] = @CondicionIVA,
		[CUIT] = @CUIT,
		[codTango] = @codTango,
		[interna] = @interna,
		baja = @baja
	WHERE
		[idEmpresa] = @idEmpresa




END

GO


