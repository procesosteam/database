USE [SIG]
GO

/****** Object:  Table [dbo].[OrdenTrabajoDetalle]    Script Date: 11/03/2015 11:50:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrdenTrabajoDetalle](
	[idOrdenDetalle] [int] IDENTITY(1,1) NOT NULL,
	[idOrden] [int] NOT NULL,
	[idPersonal] [int] NULL,
	[idEquipoCliente] [int] NULL,
	[idTipoServicio] [int] NULL,
	[idEquipoPropio] [int] NULL,
 CONSTRAINT [PK_OrdenTrabajoDetalle] PRIMARY KEY CLUSTERED 
(
	[idOrdenDetalle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

