USE [SIG_Prod]
GO
/****** Object:  Trigger [dbo].[tg_AditoriaSS]    Script Date: 04/12/2016 18:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[tg_AditoriaSS]
   ON  [dbo].[SolicitudCliente]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @IDUSER INT
    DECLARE @IDSS INT
    DECLARE @BAJA BIT
    DECLARE @BAJASTR VARCHAR(100)
    DECLARE @Action as char(1)

	SET  @BAJA = 0
	SET @BAJASTR= ''
    
    SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                         AND EXISTS(SELECT * FROM DELETED)
                        THEN 'U'  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN 'I'  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN 'D'  -- Set Action to Deleted.
                        ELSE NULL -- Skip. It may have been a "failed delete".   
                    END)
    
	SELECT @IDUSER = idUsuario, @IDSS = IDsolicitud , @BAJA = BAJA
	FROM INSERTED
	
	IF @Action = 'U'
	BEGIN
		IF @BAJA = 1
			BEGIN SET @BAJASTR = ' DELETE ' END
		ELSE
			BEGIN SET @BAJASTR = ' UPDATE ' END
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,'SOLICITUD CLIENTE'
			   ,@BAJASTR + ' IDSS: ' + STR(LTRIM(RTRIM(@IDSS))) 
			   ,GETDATE())
			   
	END
	IF @Action = 'I'
	BEGIN
		INSERT INTO [dbo].[Auditoria]
			   ([idUsuario]
			   ,[Tabla]
			   ,[Accion]
			   ,[Fecha])
		 VALUES
			   (@IDUSER
			   ,'SOLICITUD CLIENTE'
			   ,'INSERT IDSS: ' + STR(LTRIM(RTRIM(@IDSS)))
			   ,GETDATE())
     END
      
    UPDATE SolicitudCliente 
	SET nropedidocliente = CASE WHEN nropedidocliente IS NULL OR (SELECT COUNT(*) FROM dbo.ArrayToTable(NROPEDIDOCLIENTE,'-'))<3 THEN
							isnull(nropedidocliente, str(nrosolicitud)) + ' - ' + dbo.ABC(Nrosolicitud, idSolicitud)
							ELSE
							NROPEDIDOCLIENTE
							END
	WHERE idsolicitud = @IDSS
	
END

