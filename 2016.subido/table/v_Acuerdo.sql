USE [SIG]
GO

/****** Object:  View [dbo].[v_Acuerdo]    Script Date: 11/06/2015 14:39:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Acuerdo]
AS
SELECT     dbo.Acuerdo.idAcuerdo, dbo.Acuerdo.titulo, dbo.Acuerdo.nroAcuerdo, dbo.Acuerdo.idCliente, dbo.Acuerdo.nroRevision, dbo.AcuerdoConceptoCosto.idAcuerdoConcepto, 
                      dbo.AcuerdoConceptoCosto.Concepto, dbo.AcuerdoConceptoCosto.Costo, dbo.AcuerdoConceptoCosto.idEspecificacion, dbo.AcuerdoConceptoCosto.cantidad, dbo.AcuerdoServicio.idTipoServicio, 
                      dbo.AcuerdoServicio.idAcuerdoServicio, dbo.Acuerdo.idSector
FROM         dbo.Acuerdo INNER JOIN
                      dbo.AcuerdoServicio ON dbo.Acuerdo.idAcuerdo = dbo.AcuerdoServicio.idAcuerdo INNER JOIN
                      dbo.AcuerdoConceptoCosto ON dbo.AcuerdoConceptoCosto.idAcuerdoServicio = dbo.AcuerdoServicio.idAcuerdoServicio LEFT OUTER JOIN
                          (SELECT     MAX(nroRevision) AS NroRevision, nroAcuerdo
                            FROM          dbo.Acuerdo AS Acuerdo_1
                            GROUP BY nroAcuerdo) AS maxA ON maxA.nroAcuerdo = dbo.Acuerdo.nroAcuerdo AND maxA.NroRevision = dbo.Acuerdo.nroRevision
WHERE     (dbo.Acuerdo.baja = 0)

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AcuerdoConceptoCosto"
            Begin Extent = 
               Top = 9
               Left = 276
               Bottom = 178
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Acuerdo"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 195
               Right = 236
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "maxA"
            Begin Extent = 
               Top = 6
               Left = 746
               Bottom = 96
               Right = 944
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AcuerdoServicio"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 129
               Right = 708
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_Acuerdo'
GO

