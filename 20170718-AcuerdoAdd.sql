USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoAdd]    Script Date: 07/18/2017 18:14:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoAdd]
(
	@nroAcuerdo varchar(50),
	@idCliente int,
	@fechaInicio datetime = NULL,
	@fechaFin datetime = NULL,
	@fechaEmision datetime = NULL,
	@nroRevision int = NULL,
	@titulo varchar(250) = NULL,
	@tipoMoneda varchar(50) = NULL,
	@idSector int,
	@activo bit
)
AS
BEGIN


	INSERT
	INTO [Acuerdo]
	(
		[titulo],
		[nroAcuerdo],
		[idCliente],
		[fechaInicio],
		[fechaFin],
		[fechaEmision],
		[nroRevision],
		[tipoMoneda],
		[idSector],
		[activo]
	
	)
	VALUES
	(
		@titulo,
		@nroAcuerdo,
		@idCliente,
		@fechaInicio,
		@fechaFin,
		@fechaEmision,
		@nroRevision,
		@tipoMoneda,
		@idSector,
		@activo
	)

	SELECT CAST(SCOPE_IDENTITY() as INT)

END
