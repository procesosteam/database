USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetById]    Script Date: 07/18/2017 18:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoGetById]
(
	@idAcuerdo int
)
AS
BEGIN
--declare @idAcuerdo int = 129
	SELECT DISTINCT
		A.[idAcuerdo],
		A.[nroAcuerdo],
		A.[titulo],
		DBO.GETDIFF(A.[titulo],AANT.[titulo]) AS TITULODIF,
		A.[idCliente],
		E.RAZONSOCIAL,
		A.[fechaInicio],
		A.[fechaFin],
		A.[fechaEmision],
		A.[nroRevision],
		A.[tipoMoneda],
		AT.detalle as header,
		A.idSector,
		s.Descripcion as Sector,
		A.activo
	FROM [Acuerdo] A
	INNER JOIN EMPRESA E ON E.IDEMPRESA = A.IDCLIENTE
	LEFT JOIN ACUERDO AANT ON AANT.NROACUERDO = A.NROACUERDO AND (A.NROREVISION-1) = AANT.NROREVISION AND AANT.BAJA = 0 and AANT.idCliente = E.idEmpresa and AANT.idSector = A.idSector
	LEFT JOIN AcuerdoTitulo AT on AT.idAcuerdo = A.idAcuerdo and AT.titulo = 'header'
	left join Sector s on s.idSector = a.idSector
	WHERE
		(A.[idAcuerdo] = @idAcuerdo)
		--AND A.baja = 0

END
