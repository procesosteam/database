/*
   martes, 18 de julio de 201705:56:24 p.m.
   Usuario: 
   Servidor: (local)
   Base de datos: SIG_Prod
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acuerdo ADD
	activo bit NULL
GO
ALTER TABLE dbo.Acuerdo ADD CONSTRAINT
	DF_Acuerdo_activo DEFAULT 0 FOR activo
GO
ALTER TABLE dbo.Acuerdo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
