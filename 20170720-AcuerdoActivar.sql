USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoDelete]    Script Date: 07/20/2017 17:57:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AcuerdoActivar]
(
	@idAcuerdo int
)
AS
BEGIN

	UPDATE [Acuerdo]
	SET activo = 1
	WHERE
		[idAcuerdo] = @idAcuerdo
END
