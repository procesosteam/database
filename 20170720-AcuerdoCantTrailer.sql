USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoCantTrailer]    Script Date: 07/20/2017 20:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	ESTE PROCEDIMIENTO DEBE MOSTRAR LA CANTIDAD DE TRAILERS ASIGNADAS AL CLIENTE A TRAVEZ DEL ACUERDO
--				Y LA CANTIDAD INGRESADA EN EL ACUERDO, PARA LUEGO COMPARARLAS CON LOS TRAILERS DISPONIBLES
-- =============================================
ALTER PROCEDURE [dbo].[AcuerdoCantTrailer]
(
	@IDCLIENTE INT,
	@idSector int, 
	@idEspecificacion varchar(400)
)
AS

BEGIN

/*
declare @IDCLIENTE INT,
	@idSector int, 
	@idEspecificacion varchar(400)

select @IDCLIENTE = 8 ,
	@idSector = 31, 
	@idEspecificacion ='50'
*/

DECLARE @IDACUERDO INT

SELECT @IDACUERDO = max(IDACUERDO)
FROM Acuerdo A
WHERE isnull(a.baja,0) = 0
	AND ISNULL(a.activo,1) = 1
	AND a.idcliente = @idCliente
	and a.idSector = @idSector
	--and getdate() between a.fechainicio and a.fechafin
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0 AND ISNULL(a.activo,1) = 1 and nroacuerdo = a.nroacuerdo)

	SELECT DISTINCT
				A.IDACUERDO, 				
				ac.idTipoServicio,
				Acc.idAcuerdoConcepto,
				acc.idEspecificacion,			
				ISNULL(cast(acc2.Costo as int),0) as cantAcuerdo,
				ISNULL(at.cantAsignados,0) as cantAsignados,
				ISNULL(ut.disponible,'FALSE') AS disponible,
				isnull(cantTrUs.cantTrailerUso,0) as cantTrailerUso,
				a.nroRevision,
				a.nroAcuerdo
	FROM Acuerdo a	
	INNER JOIN AcuerdoServicio ac ON AC.idAcuerdo = A.idAcuerdo
	left join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = ac.idAcuerdoServicio and acc.Concepto not like '%Cant%'
	left join AcuerdoConceptoCosto acc2 on acc2.idAcuerdoServicio = ac.idAcuerdoServicio and acc2.Concepto like '%Cant%' and acc.idEspecificacion = acc2.idEspecificacion
	left JOIN (select count(*) as cantAsignados,idAcuerdoConcepto
				from AcuerdoConceptoTrailer
				group by idAcuerdoConcepto) at on (at.idAcuerdoConcepto = acc.idAcuerdoConcepto )
	left JOIN (SELECT CASE WHEN sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null) 
							THEN 'TRUE' ELSE 'FALSE' END AS DISPONIBLE, 
						ACT.idacuerdoconcepto		
				from AcuerdoConceptoTrailer act
				left join dbo.UltimaOtiTrailer()ut ON UT.idTrailer = Act.idTrailer
				left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
													AND SC.Baja = 0 
													and SC.Entrega = 0
													and UT.FECHA = SC.FechaAlta
													and sc.idTipoServicio = UT.idtipoServicio) UT on ut.idacuerdoconcepto = acc.idAcuerdoConcepto
													
	left JOIN (SELECT count(ut.idTrailer) as cantTrailerUso,sc.idSector, ut.idtipoServicio
				from dbo.UltimaOtiTrailer()ut 
				left JOIN SolicitudCliente sc on UT.nroSolicitud = sc.nroSolicitud 
													AND SC.Baja = 0 
													and  SC.Entrega = 1
													and UT.FECHA = SC.FechaAlta
													and sc.idTipoServicio = UT.idtipoServicio
				left join SolicitudClienteEspecificacion sce on sce.idSolicitudCliente = sc.idSolicitud
				where (sc.idsolicitud is not null or (ut.nrosolicitud is null and ut.fecha is null))						
				and sc.idCondicion = 2
						and (ut.nroSolicitud not in (select nroSolicitud from SolicitudCliente ss
										where ss.Baja = 0 and ss.Entrega = 0) 			
							or
							((select count(1) from SolicitudCliente ss2
								where ss2.Baja = 0 and ss2.Entrega = 0 
								and ss2.nroSolicitud = ut.nroSolicitud 
								and ss2.idtipoServicio = ut.idtipoServicio
								and ss2.idSector = sc.idSector) = 0))
						and (sce.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')))
				group by sc.idSector, ut.idtipoServicio
				) cantTrUs on cantTrUs.idSector = a.idSector and ac.idTipoServicio = cantTrUs.idtipoServicio
													
	
	WHERE a.idCliente = @IDCLIENTE
			and a.idSector = @idSector
			and a.baja = 0
			and a.idAcuerdo = @IDACUERDO
			/*and a.nroRevision = (select MAX(ax.nroRevision)
								from Acuerdo ax
								where ax.baja = 0 
								and ax.idCliente = @IDCLIENTE and ax.idSector = @idSector
								and ax.nroAcuerdo = a.nroAcuerdo)*/
			and (acc.idEspecificacion IN (SELECT * FROM dbo.ArrayToTable(@idEspecificacion,',')) )		
END

