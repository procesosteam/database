USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[AcuerdoGetByCliente]    Script Date: 07/20/2017 19:40:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcuerdoGetByCliente]
(
	@IDSECTOR INT,
	@IDCLIENTE INT,
	@IDSOLICITUD INT
)
AS
BEGIN


SELECT     dbo.Acuerdo.idAcuerdo, dbo.Acuerdo.titulo, dbo.Acuerdo.nroAcuerdo, dbo.Acuerdo.idCliente, dbo.Acuerdo.nroRevision, dbo.AcuerdoConceptoCosto.idAcuerdoConcepto, 
                      dbo.AcuerdoConceptoCosto.Concepto, dbo.AcuerdoConceptoCosto.Costo, dbo.AcuerdoConceptoCosto.idEspecificacion, dbo.AcuerdoConceptoCosto.cantidad, 
                      dbo.AcuerdoServicio.idTipoServicio, dbo.AcuerdoServicio.idAcuerdoServicio, dbo.Acuerdo.idSector, dbo.Acuerdo.fechaInicio, dbo.Acuerdo.fechaFin
FROM         dbo.Acuerdo 
INNER JOIN dbo.AcuerdoServicio ON dbo.Acuerdo.idAcuerdo = dbo.AcuerdoServicio.idAcuerdo 
INNER JOIN dbo.AcuerdoConceptoCosto ON dbo.AcuerdoConceptoCosto.idAcuerdoServicio = dbo.AcuerdoServicio.idAcuerdoServicio 
INNER JOIN (SELECT top 1 *
			FROM Acuerdo 
			WHERE idCliente = @IDCLIENTE
			and idSector = @IDSECTOR
			and baja = 0
			and ((select fecha from solicitudcliente where idsolicitud = @IDSOLICITUD)
				between fechainicio and fechafin)
			order by nroacuerdo desc, nrorevision desc) topA on topA.idacuerdo = dbo.acuerdo.idacuerdo
WHERE   (dbo.Acuerdo.baja = 0)
and isnull(dbo.Acuerdo.activo,1) = 1
and		 dbo.Acuerdo.idCliente = @IDCLIENTE
and		 dbo.Acuerdo.idSector = @IDSECTOR

END


