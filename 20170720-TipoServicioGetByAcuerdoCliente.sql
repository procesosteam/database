USE [SIG_Prod]
GO
/****** Object:  StoredProcedure [dbo].[TipoServicioGetByAcuerdoCliente]    Script Date: 07/20/2017 18:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TipoServicioGetByAcuerdoCliente]
(
	@idCliente int,
	@idSector int
)

AS

BEGIN

DECLARE @IDACUERDO INT

SELECT @IDACUERDO = max(IDACUERDO)
FROM Acuerdo A
WHERE isnull(a.baja,0) = 0
	AND ISNULL(a.activo,1) = 1
	AND a.idcliente = @idCliente
	and a.idSector = @idSector
	and getdate() between a.fechainicio and a.fechafin
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector AND ISNULL(activo,1) = 1 and baja = 0 and nroacuerdo = a.nroacuerdo)

	SELECT	distinct 
			a.nroAcuerdo, 
			a.nroRevision, 
			a.fechaInicio, 
			a.fechaFin,
			acs.idTipoServicio, 
			ts.Descripcion as TipoServicio,
			ts.Abreviacion,
			ts.RequierePersonal,
			ts.idSector,
			ts.RequiereTraslado,
			ts.RequiereCuadrilla,
			ts.RequiereEquipo,
			ts.RequiereKms,
			ts.RequiereKmsTaco
	FROM  Acuerdo a
	inner join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	inner join TipoServicio ts on ts.idTipoServicio= acs.idTipoServicio
	WHERE a.idAcuerdo = @IDACUERDO
	/* isnull(a.baja,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and getdate() between a.fechainicio and a.fechafin
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)*/
	ORDER BY ts.descripcion;
	
	SELECT	distinct acs.idTipoServicio, 
			et.idEspecificaciones as idEspecificacion,
			et.Descripcion as EspecificacionTecnica,
			et.RequiereTraslado,
			et.RequiereEquipo
						
	FROM    Acuerdo a
	--inner join (select MAX(isnull(nroRevision,0))as nroRev from acuerdo where idcliente=@idCliente) aa on aa.nroRev = a.nroRevision
	left join AcuerdoServicio acs on a.idAcuerdo = acs.idAcuerdo
	left join TipoServicioEspecificacion tse on tse.idTipoServicio = acs.idTipoServicio 
	left join AcuerdoConceptoCosto acc on acc.idAcuerdoServicio = acs.idAcuerdoServicio
	left join EspecificacionTecnica et on et.idEspecificaciones = acc.idEspecificacion and et.RequiereTraslado = 0 --and (et.RequiereCuadrilla = 1 and et.RequiereEquipo = 0)
	WHERE a.idAcuerdo = @IDACUERDO
	/*isnull(a.BAJA,0) = 0
	and a.idcliente = @idCliente
	and a.idSector = @idSector
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)
	and a.fechaInicio = (select MAX(fechaInicio)as nroRev 
						from acuerdo where idcliente=@idCliente and idSector = @idSector and baja = 0)		
	and getdate() between a.fechainicio and a.fechafin*/
	
		


END

