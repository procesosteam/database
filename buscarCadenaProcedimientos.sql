SELECT *
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_DEFINITION LIKE '%MAX(isnull(nroRevision,0))%'
AND ROUTINE_TYPE='PROCEDURE'
order by ROUTINE_NAME


SELECT * --max(IDACUERDO)
FROM Acuerdo A
WHERE isnull(a.baja,0) = 0
	AND ISNULL(a.activo,1) = 1
	AND a.idcliente = 8
	and a.idSector = 31
	and getdate() between a.fechainicio and a.fechafin
	and a.nroRevision = (select MAX(isnull(nroRevision,0))as nroRev 
						from acuerdo where idcliente=8 and idSector = 31 
						AND ISNULL(activo,1) = 1 
						and baja = 0 and nroacuerdo = a.nroacuerdo)
